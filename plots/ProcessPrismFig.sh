#!/bin/bash
echo "pre-cleanup"
rm warn.txt
rm info.txt
rm -r PDFs
rm -r PNGs
mkdir PDFs
mkdir PNGs

echo "making pdfs"
## Primary Figures
inkscape ModSvg/ProtoProfiles_mod.svg --export-pdf=PDFs/F1_ProtoProfiles.pdf 2>>warn.txt 1>>info.txt
inkscape ModSvg/contour.mod.svg --export-pdf=PDFs/F2_contours.pdf 2>>warn.txt 1>>info.txt
inkscape ModSvg/betareg.mod.svg --export-pdf=PDFs/F3A_betareg.pdf 2>>warn.txt 1>>info.txt
inkscape ModSvg/betamap.mod.svg --export-pdf=PDFs/F3B_betamap.pdf 2>>warn.txt  1>>info.txt
inkscape barleaveoneout.eps --export-pdf=PDFs/F4_barleaveoneout.pdf 2>>warn.txt 1>>info.txt
inkscape ModSvg/teffmap.mod.svg --export-pdf=PDFs/F5_teffmap.pdf 2>>warn.txt 1>>info.txt
inkscape ModSvg/tefflatscenarios.mod.svg --export-pdf=PDFs/F6_tefflatscenarios.pdf 2>>warn.txt 1>>info.txt

## Supplement
inkscape ModSvg/particle_model_validation.mod.svg --export-pdf=PDFs/S1_particle_model_validation.pdf 2>>warn.txt 1>>info.txt
inkscape ModSvg/ballastchl.mod.svg --export-pdf=PDFs/S2_ballastchl.pdf 2>>warn.txt 1>>info.txt
inkscape ModSvg/contour_ag.mod.svg --export-pdf=PDFs/S3_contour_ag.pdf 2>>warn.txt 1>>info.txt
inkscape ModSvg/exportregion.mod.svg --export-pdf=PDFs/S4_exportregion.pdf 2>>warn.txt 1>>info.txt
inkscape ModSvg/reminlengthmap.mod.svg --export-pdf=PDFs/S5A_reminlengthmap.pdf 2>>warn.txt 1>>info.txt
inkscape ModSvg/reminlengthscenarios0m.mod.svg --export-pdf=PDFs/S5B_reminlengthscenarios0m.pdf 2>>warn.txt 1>>info.txt
inkscape ModSvg/reminlengthscenarios.mod.svg --export-pdf=PDFs/S5C_reminlengthscenarios200m.pdf 2>>warn.txt 1>>info.txt
inkscape ModSvg/reminlengthscenarios1000m.mod.svg --export-pdf=PDFs/S5D_reminlengthscenarios1000m.pdf 2>>warn.txt 1>>info.txt
inkscape taylor.eps --export-pdf=PDFs/S6_taylor.pdf 2>>warn.txt 1>>info.txt
inkscape barleaveoneout_gamma.eps --export-pdf=PDFs/S7_barleaveoneout_gamma.pdf 2>>warn.txt 1>>info.txt
inkscape taylor_gamma.eps --export-pdf=PDFs/S8_taylor_gamma.pdf 2>>warn.txt 1>>info.txt

echo "making pngs"
## Primary Figures
inkscape ModSvg/ProtoProfiles_mod.svg --export-png=PNGs/F1_ProtoProfiles.png 2>>warn.txt 1>>info.txt
inkscape ModSvg/contour.mod.svg --export-png=PNGs/F2_contours.png 2>>warn.txt 1>>info.txt
inkscape ModSvg/betareg.mod.svg --export-png=PNGs/F3A_betareg.png 2>>warn.txt 1>>info.txt
inkscape ModSvg/betamap.mod.svg --export-png=PNGs/F3B_betamap.png 2>>warn.txt  1>>info.txt
inkscape barleaveoneout.eps --export-png=PNGs/F4_barleaveoneout.png 2>>warn.txt 1>>info.txt
inkscape ModSvg/teffmap.mod.svg --export-png=PNGs/F5_teffmap.png 2>>warn.txt 1>>info.txt
inkscape ModSvg/teffmap.mod.svg --export-png=PNGs/F5_teffmap.png 2>>warn.txt 1>>info.txt
inkscape ModSvg/tefflatscenarios.mod.svg --export-png=PNGs/F6_tefflatscenarios.png 2>>warn.txt 1>>info.txt

## Supplement
inkscape ModSvg/particle_model_validation.mod.svg --export-png=PNGs/S1_particle_model_validation.png 2>>warn.txt 1>>info.txt
inkscape ModSvg/ballastchl.mod.svg --export-png=PNGs/S2_ballastchl.png 2>>warn.txt 1>>info.txt
inkscape ModSvg/contour_ag.mod.svg --export-png=PNGs/S3_contour_ag.png 2>>warn.txt 1>>info.txt
inkscape ModSvg/exportregion.mod.svg --export-png=PNGs/S4_exportregion.png 2>>warn.txt 1>>info.txt
inkscape ModSvg/reminlengthmap.mod.svg --export-png=PNGs/S5A_reminlengthmap.png 2>>warn.txt 1>>info.txt
inkscape ModSvg/reminlengthscenarios0m.mod.svg --export-png=PNGs/S5B_reminlengthscenarios0m.png 2>>warn.txt 1>>info.txt
inkscape ModSvg/reminlengthscenarios.mod.svg --export-png=PNGs/S5C_reminlengthscenarios200m.png 2>>warn.txt 1>>info.txt
inkscape ModSvg/reminlengthscenarios1000m.mod.svg --export-png=PNGs/S5D_reminlengthscenarios1000m.png 2>>warn.txt 1>>info.txt
inkscape taylor.eps --export-png=PNGs/S6_taylor.png 2>>warn.txt 1>>info.txt
inkscape barleaveoneout_gamma.eps --export-png=PNGs/S7_barleaveoneout_gamma.png 2>>warn.txt 1>>info.txt
inkscape taylor_gamma.eps --export-png=PNGs/S8_taylor_gamma.png 2>>warn.txt 1>>info.txt

echo "concatinating pdfs"

pdftk PDFs/F1_ProtoProfiles.pdf PDFs/F2_contours.pdf  PDFs/F3A_betareg.pdf PDFs/F3B_betamap.pdf PDFs/F4_barleaveoneout.pdf PDFs/F5_teffmap.pdf PDFs/F6_tefflatscenarios.pdf   cat output pvstofigs.pdf

pdftk PDFs/S1_particle_model_validation.pdf PDFs/S2_ballastchl.pdf PDFs/S3_contour_ag.pdf PDFs/S4_exportregion.pdf PDFs/S5A_reminlengthmap.pdf PDFs/S5B_reminlengthscenarios0m.pdf PDFs/S5C_reminlengthscenarios200m.pdf PDFs/S5D_reminlengthscenarios1000m.pdf PDFs/S6_taylor.pdf PDFs/S7_barleaveoneout_gamma.pdf PDFs/S8_taylor_gamma.pdf cat output pvstosupfigs.pdf

pdftk PDFs/F3A_betareg.pdf PDFs/F3B_betamap.pdf cat output F3_beta_combined.pdf
