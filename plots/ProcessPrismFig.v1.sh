#!/bin/bash
echo "pre-cleanup"
rm warn.txt
rm info.txt
rm -r PDFs
rm -r PNGs
mkdir PDFs
mkdir PNGs

echo "making pdfs"
inkscape ModSvg/ProtoProfiles_mod.svg --export-pdf=PDFs/F1_ProtoProfiles.pdf 2>>warn.txt 1>>info.txt
inkscape ModSvg/contour.mod.svg --export-pdf=PDFs/F2_contours.pdf 2>>warn.txt 1>>info.txt
inkscape ModSvg/betareg.mod.svg --export-pdf=PDFs/F3A_betareg.pdf 2>>warn.txt 1>>info.txt
inkscape ModSvg/betamap.mod.svg --export-pdf=PDFs/F3B_betamap.pdf 2>>warn.txt  1>>info.txt
inkscape barleaveoneout.eps --export-pdf=PDFs/F4_barleaveoneout.pdf 2>>warn.txt 1>>info.txt
inkscape ModSvg/teffmap.mod.svg --export-pdf=PDFs/F5_teffmap.pdf 2>>warn.txt 1>>info.txt
inkscape ModSvg/reminlengthmap.mod.svg --export-pdf=PDFs/F6A_reminlengthmap.pdf 2>>warn.txt 1>>info.txt
inkscape ModSvg/reminlengthscenarios.mod.svg --export-pdf=PDFs/F6B_reminlengthscenarios.pdf 2>>warn.txt 1>>info.txt
inkscape ModSvg/particle_model_validation.mod.svg --export-pdf=PDFs/S1_particle_model_validation.pdf 2>>warn.txt 1>>info.txt
inkscape ModSvg/ballastchl.mod.svg --export-pdf=PDFs/S2_ballastchl.pdf 2>>warn.txt 1>>info.txt
inkscape ModSvg/exportregion.mod.svg --export-pdf=PDFs/S3_exportregion.pdf 2>>warn.txt 1>>info.txt

echo "making pngs"
inkscape ModSvg/ProtoProfiles_mod.svg --export-png=PNGs/F1_ProtoProfiles.png 2>>warn.txt 1>>info.txt
inkscape ModSvg/contour.mod.svg --export-png=PNGs/F2_contours.png 2>>warn.txt 1>>info.txt
inkscape ModSvg/betareg.mod.svg --export-png=PNGs/F3A_betareg.png 2>>warn.txt 1>>info.txt
inkscape ModSvg/betamap.mod.svg --export-png=PNGs/F3B_betamap.png 2>>warn.txt  1>>info.txt
inkscape barleaveoneout.eps --export-png=PNGs/F4_barleaveoneout.png 2>>warn.txt 1>>info.txt
inkscape ModSvg/teffmap.mod.svg --export-png=PNGs/F5_teffmap.png 2>>warn.txt 1>>info.txt
inkscape ModSvg/reminlengthmap.mod.svg --export-png=PNGs/F6A_reminlengthmap.png 2>>warn.txt 1>>info.txt
inkscape ModSvg/reminlengthscenarios.mod.svg --export-png=PNGs/F6B_reminlengthscenarios.png 2>>warn.txt 1>>info.txt
inkscape ModSvg/particle_model_validation.mod.svg --export-png=PNGs/S1_particle_model_validation.png 2>>warn.txt 1>>info.txt
inkscape ModSvg/ballastchl.mod.svg --export-png=PNGs/S2_ballastchl.png 2>>warn.txt 1>>info.txt
inkscape ModSvg/exportregion.mod.svg --export-png=PNGs/S3_exportregion.png 2>>warn.txt 1>>info.txt

echo "concatinating pdfs"

pdftk PDFs/F1_ProtoProfiles.pdf PDFs/F2_contours.pdf  PDFs/F3A_betareg.pdf PDFs/F3B_betamap.pdf PDFs/F4_barleaveoneout.pdf  PDFs/F5_teffmap.pdf PDFs/F6A_reminlengthmap.pdf PDFs/F6B_reminlengthscenarios.pdf cat output pvstofigs.pdf

pdftk PDFs/S1_particle_model_validation.pdf PDFs/S2_ballastchl.pdf PDFs/S3_exportregion.pdf cat output pvstosupfigs.pdf
