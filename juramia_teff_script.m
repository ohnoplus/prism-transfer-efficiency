% A Juramia is the prehistoric ancestor to all mammals. Earlier
% versions of this script were named after dinosaurs.
% This script recreates the figures in Cram et al. (in prep). They
% are not in the same order here as one would find them
% in the paper.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% SETUP, load in paths and data%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
addpath('data')
addpath(genpath('tools'))
addpath('maincode')
% Surface PSD data from p16 transect
load('McDonnellPSD.mat') % p16psd
% chlorophyl climatologyx data
chl = load('chl_wgrid.mat');
load('juramia_data.mat')
nx = length(grid.xt);
ny = length(grid.yt);
regabr = {'AAZ', 'SAZ', 'STA', 'STP', 'ETA', 'ETP', 'NA', 'NP'};

                                            
% weber et al. 2016 transfer efficiency estemates
frankteff = load('franken_teff_1k_2k.mat'); % method*region*depth
teff_obs = frankteff.output.TE(1, :, 1);
teff_err =  frankteff.output.TEerr(1,:,1);

%%
%% frankenexport (surface export according to Weber et al. 2016)

% frankex_clim = nan([size(grid.XT), 12]);

exportmaps = load('sat_Cexport_all_algorithms.mat');
franken_weight = load('franken_weight_map.mat');

%W * Cex is anual map

pre_franken_clim = permute(exportmaps.Cex_mon, [1 2 4 5 3]) .* repmat(franken_weight.W, 1, 1, ...
                                            1, 1, 12);

frankex_clim_woa = squeeze(sum(sum(pre_franken_clim, 3), 4));

for t = 1:12
    frankex_clim(:,:,t) = repaint_interp2(woachl.grid.XT, woachl.grid.YT, ...
                              frankex_clim_woa(:,:,t), grid.XT, ...
                                          grid.YT, M3d(:,:,1));
end

%% frankenexport but this time, missing values are treated as zeros
frankex_clim_woa_0 = frankex_clim_woa;
frankex_clim_woa_0(isnan(frankex_clim_woa_0)) = 0;

for t = 1:12
    frankex_clim_0(:,:,t) = repaint_interp2(woachl.grid.XT, woachl.grid.YT, ...
                              frankex_clim_woa_0(:,:,t), grid.XT, ...
                                          grid.YT, M3d(:,:,1));
end

%% Calculate particle size distribution map
% from chlorphyl concentraitons and the P16 UVP data.
% mcchlC is the chlophyl climatology measurment corresponding to each PSD measurment observed by
% McDonnell's group and their UVP
% The following block makes that list.
nL = length(p16psd.lat);
Ih = find(chl.grid.xt > 180);
Il = find(chl.grid.xt <= 180);
re_chl_clim = [chl.chl_clim(:,Ih,:),chl.chl_clim(:,Il,:)];
re_chl_lat = [chl.grid.xt(Ih)-360, chl.grid.xt(Il)];

for i = 1:nL
    [jnk, ccidx.lat(i,1)] = min(abs(chl.grid.yt - p16psd.lat(i)));
    [jnk, ccidx.lon(i,1)] = min(abs(re_chl_lat - ...
                                    p16psd.lon(i)));
    try
        mcchlC(i,1) = re_chl_clim(ccidx.lat(i,1), ccidx.lon(i,1), ...
                                  p16psd.mon(i));
    catch
        mcchlC(i,1) = NaN;
    end
end


% Linear model to estemate PSD from Chl productivity.
findx = find(~(isnan(log10(mcchlC)) | isnan(p16psd.surf_beta)));

[mod4 smod4] = polyfit(log10(mcchlC(findx)), p16psd.surf_beta(findx), ...
                       1);
[mod4b smod4b] = fit(log10(mcchlC(findx)), p16psd.surf_beta(findx), ...
                     'poly1'); 

mod4c = fitlm(log10(mcchlC(findx)), p16psd.surf_beta(findx), 'linear');


% extrapolate PSD from chlorophyl climatology
iocn = find(M3d(:)==1);
isurf = find(M3d(:,:,1)==1);
isurft = find(repmat(M3d(:,:,1), 1, 1, 12) == 1);

chl_clim_vec = pgrid_chl_clim(isurft);
chl_psd_vec = polyval(mod4, log10(chl_clim_vec));

mcchl_psd = nan(size(pgrid_chl_clim));
mcchl_psd(isurft) = chl_psd_vec;

PSD_map = -sum(mcchl_psd .* frankex_clim, 3)./...
    sum(frankex_clim, 3);

%% calculate and regrid sea water density and temperature
iocn = find(M3d(:)==1);
M4d = repmat(M3d, 1, 1, 1, 12);
iocn4 = find(M4d(:)==1);

grid4 = grid;
grid4.XT4d = repmat(grid.XT3d, 1, 1, 1, 12);
grid4.YT4d = repmat(grid.YT3d, 1, 1, 1, 12);
grid4.ZT4d = repmat(grid.ZT3d, 1, 1, 1, 12);


vecs4.lat = grid4.YT4d(iocn4);
vecs4.depth= grid4.ZT4d(iocn4);
vecs4.pres = sw_pres(vecs4.depth, vecs4.lat);
vecs4.salt = woaRe3.salt_mon(iocn4);
vecs4.temp = woaRe3.temp_mon(iocn4);
vecs4.pdens = sw_pden(vecs4.salt, vecs4.temp, vecs4.pres, 10.13);
pdens4d = nan(size(woaRe3.temp_mon));
pdens4d(iocn4) = vecs4.pdens/1000;

% and more bookkeeping
flux4D_0 = repmat(permute(frankex_clim,[ 1 2 4 3]), 1, 1, 24, 1);
% nan below sea floor
flux4D = nan(size(flux4D_0)); 
flux4D(iocn4) = flux4D_0(iocn4);

%% Ballast export
% these are austensibly molar ratios, according to CD
gfdl_file = 'gfdl_esm2m_hist_ballast.nc';
gfdl.info = ncinfo(gfdl_file);
gfdl.lon = ncread(gfdl_file, 'LON');
gfdl.lat = ncread(gfdl_file, 'LAT');

gfdl.C = ncread(gfdl_file, 'EXPC');
gfdl.si = ncread(gfdl_file, 'EXPSI');
gfdl.calc = ncread(gfdl_file, 'EXPCALC');
gfdl.arag = ncread(gfdl_file, 'EXPARAG');
gfdl.car = gfdl.calc + gfdl.arag;

gfdl.F.car = gfdl.car./gfdl.C;
gfdl.F.si = gfdl.si./gfdl.C;

[gfdl.grid.X2d, gfdl.grid.Y2d] = meshgrid(gfdl.lon, gfdl.lat);

gfdl_PF.car = repaint_interp2(gfdl.grid.X2d, gfdl.grid.Y2d, ...
                              gfdl.F.car', grid.XT, grid.YT, M3d(:,: ...
                                                  ,1));
gfdl_PF.si = repaint_interp2(gfdl.grid.X2d, gfdl.grid.Y2d, ...
                              gfdl.F.si', grid.XT, grid.YT, M3d(:,: ...
                                                  ,1));


%% Make the parJ object
% parJ is a multi field structure that is an input to fucntions
% and contains all of the data those functions need

% grid, same as in Devries
parJ.grid = grid;
% which grid cells are ocean and which are land
parJ.M3d = M3d;
% consider temperature effects, also don't break
parJ.mode = 4;
% the top three layers are not modeled because we focus on the mesopelegic
parJ.nplayers = 3;

%densities and similar
parJ.component.rho_opal = 2.09;
parJ.component.rho_CaCO3 = 2.83;
parJ.component.rho_POM = 1.033;

% fraction of POM that is actually carbon
% the rest is water (4/5) and elements that aren't carbon (1/2 of
% whats left)
parJ.component.c2pom = 0.1; 
                           
                           
% Non porous mass of a standard particle mg/ml
% Euphausid fecal pellet as used by Alldredge and Gotschalk 1988
parJ.component.rho_std = 1.23; 

%alternatively, global mean non-pourous particle density
% results in slightly higher estemate of Cr/Cw
%parJ.component.rho_std = 1.18; 

% Standard mass of water mg/ml
parJ.component.rho_water_std = 1.027;
% Since the top three grid cells are not used, we actually only go
% to 900m, which gets us close to 1000m actual
parJ.teff_depth = 900;
% Regions, as in Weber et al. 2016
parJ.R2d = R2d;

% Names of those regions
parJ.regnames = regnames;

% upper and lower bounds for particle size distributions
parJ.ds = 2;
parJ.dl = 4000;

% surface export estemates
parJ.EX = mean(frankex_clim_0, 3);

% fields of temperatue, potential density and oxygen
parJ.phys.temp = nansum(woaRe3.temp_mon .* flux4D, 4)./nansum(flux4D, ...
                                                  4);
parJ.phys.pdens = nansum(pdens4d .* flux4D, 4)./nansum(flux4D, ...
                                                  4);
parJ.phys.o2 = nansum(woaRe3.o2_mon .* flux4D, 4)./nansum(flux4D, ...
                                                  4);
% particle size distribution
parJ.PSD = PSD_map;

% export maps of F ratios of carbonate, and silicate
parJ.export.CaCO3 = gfdl_PF.car;
parJ.export.opal= gfdl_PF.si;

% in an earlier version carbonate and silicate were raw values, now
% that they are F ratios, we always treat carbon export as 1
parJ.export.C= ones(ny, nx);

% molar mass of carbonate, opal and POM
parJ.mass.CaCO3 = 100.1;
parJ.mass.opal = 96.1;
parJ.mass.C = 12;

% This needs to be deeper than the transfer efficiency depth.
% Its essentially how deep the algorithm actually calculates flux.
% There is some intermediate data product that can get exported
% that depends on this as well.
parJ.maxdeep = 1500;

%% parJ objects in which some parameters do not vary
% for isolating the effects of particular variables
% temperature, density, and oxygen are constant, size still varies
% flatXXX where XXX are the parameters that are set to not vary
% B - size, T - temperature, D - water density, O - oxygen
% Bal - Ballast by both carbonate and silicate

parJ_flatBTDO = parJ;
parJ_flatBTDO.PSD = repmat(nanmean(parJ.PSD(:)), ...
                             size(parJ.PSD));
parJ_flatBTDO.phys.temp = repmat(nanmean(parJ.phys.temp(:)), ...
                             size(parJ.phys.temp));
parJ_flatBTDO.phys.pdens = repmat(nanmean(parJ.phys.pdens(:)), ...
                             size(parJ.phys.pdens));
parJ_flatBTDO.phys.o2 = repmat(nanmean(parJ.phys.o2(:)), ...
                             size(parJ.phys.o2));
% as above, but now density varies
parJ_flatBTO = parJ_flatBTDO;
parJ_flatBTO.phys.pdens = parJ.phys.pdens;
parJ_flatPSD = parJ;

% particle size distribution is the same everywhere
mean_PSD = nanmean(parJ.PSD(:));
parJ_flatPSD.PSD = repmat(mean_PSD, size(parJ.PSD));
% temperature and density are the same everywhere
parJ_flatTD = parJ;
parJ_flatTD.phys.temp = repmat(nanmean(parJ.phys.temp(:)), ...
                             size(parJ.phys.temp));
parJ_flatTD.phys.pdens = repmat(nanmean(parJ.phys.pdens(:)), ...
                             size(parJ.phys.pdens));
% ballast is the same everywhere
parJ_flatBal = parJ;
parJ_flatBal.export.CaCO3 = repmat(nanmean(parJ.export.CaCO3(:)), size(parJ.export.CaCO3));
parJ_flatBal.export.opal = repmat(nanmean(parJ.export.opal(:)), size(parJ.export.opal));


%% Whole basin flux profile PreCalculations
% for calculating basin average flux proiles
% a vector of depths
z = 1:10:5100;
% basin wide particle size distribtutions

%basin_profiles.PSD = basin_avg(nanmean(parJ.PSD,3), R2d);
things.PSD = basin_sum(parJ.PSD .* parJ.EX, R2d)./...
    basin_sum(parJ.EX, R2d);
% temperature at 200m
things.T200 = basin_sum(parJ.phys.temp(:,:,5) .* parJ.EX, R2d)./...
    basin_sum(parJ.EX, R2d);
% temperature at the surface
things.TSurf = basin_sum(parJ.phys.temp(:,:,1) .* parJ.EX, R2d)./...
    basin_sum(parJ.EX, R2d);
% oxygen at 200m
things.ox200 = basin_sum(parJ.phys.o2(:,:,5) .* parJ.EX, R2d)./...
    basin_sum(parJ.EX, R2d);
% basin wide silicate and carbonate exports 
things.si = basin_sum(parJ.export.opal .* parJ.EX, R2d)./ ...
    basin_sum(parJ.EX, R2d);
things.car =  basin_sum(parJ.export.CaCO3 .* parJ.EX, R2d)./ ...
    basin_sum(parJ.EX, R2d);
% basin wide mass percentages
things.mpsi = (things.si .* parJ.mass.opal) ./...
    ((things.si .*parJ.mass.opal) + ...
    (things.car .*parJ.mass.CaCO3) + ...
    (ones(size(things.si)) .*parJ.mass.C));
things.mpcar = (things.car .* parJ.mass.CaCO3) ./...
    ((things.si .*parJ.mass.opal) + ...
    (things.car .*parJ.mass.CaCO3) + ...
    (ones(size(things.si)) .*parJ.mass.C));

% global averages of the above
mean_things.si = nansum(parJ.export.opal(:) .* parJ.EX(:)) ./ ...
    nansum(parJ.EX(:));
mean_things.car = nansum(parJ.export.CaCO3(:) .* parJ.EX(:)) ./ ...
    nansum(parJ.EX(:));
mean_things.mpsi = (mean_things.si .* parJ.mass.opal) ./...
    ((mean_things.si .*parJ.mass.opal) + ...
    (mean_things.car .*parJ.mass.CaCO3) + ...
    (ones(size(mean_things.si)) .*parJ.mass.C));
mean_things.mpcar = (mean_things.car .* parJ.mass.CaCO3) ./...
    ((mean_things.si .*parJ.mass.opal) + ...
    (mean_things.car .*parJ.mass.CaCO3) + ...
    (ones(size(mean_things.si)) .*parJ.mass.C));
%% make flux profiles accross a range of binned temperature and
%% oxygen values
% temperature profiles are the average of all grid cells in which
% surface temperature falls within a certain range.

% surface temperatures
surt_mat = parAm6.phys.temp(:,:,1);
surt_vec = surt_mat(isurf); 
[surt_b, surt_i] = sort(surt_vec);

% surface oxygens, unused
suro_mat = parAm6.phys.o2(:,:,1);
suro_vec = suro_mat(isurf);
[suro_b, suro_i] = sort(suro_vec);

% oxygen minima
lowo_mat = min(parAm6.phys.o2, [], 3);
lowo_vec = lowo_mat(isurf);
[lowo_b, lowo_i] = sort(lowo_vec);

[nx ny nz] = size(parAm6.phys.temp); 

% sort temperature and density by surface temperature, and oxygen
% by minimum oxygen
t_unsorted = nan(length(isurf), nz);
t_sorted =  nan(length(isurf), nz);
pd_unsorted = nan(length(isurf), nz);
pd_sorted =  nan(length(isurf), nz);
o_unsorted = nan(length(isurf), nz);
o_sorted =  nan(length(isurf), nz);

for zidx = 1:nz
    t_mat_loc = parAm6.phys.temp(:,:,zidx);
    t_unsorted(:,zidx) = t_mat_loc(isurf);
    t_sorted(:,zidx) = t_unsorted(surt_i,zidx);
    % note that pd indexes on the order of temperature
    pd_mat_loc = parAm6.phys.pdens(:,:,zidx);
    pd_unsorted(:,zidx) = pd_mat_loc(isurf);
    pd_sorted(:,zidx) = pd_unsorted(surt_i, zidx);
    % but oxygen does not
    o_mat_loc = parAm6.phys.o2(:,:,zidx);
    o_unsorted(:,zidx) = o_mat_loc(isurf);
    o_sorted(:,zidx) = o_unsorted(lowo_i, zidx);
    
end

% bin by degree or oxygen ppm 

lt =  floor(t_sorted(1,1));
ht = ceil(t_sorted(end,1));
lo = floor(min(o_sorted(1,:)));
ho = ceil(min(o_sorted(end,:)));

% temperature and density
tbvec = lt:1:ht-1;
tbin = nan(nz, length(tbvec));
pdbin = nan(nz, length(tbvec));
nT = length(tbvec);
for tidx = 1:nT;
    t = tbvec(tidx);
    loc_sidx = find(surt_b >= t & surt_b < t+1);
    tbin(:,tidx) = nanmean(t_sorted(loc_sidx,:),1);
    pdbin(:,tidx) = nanmean(pd_sorted(loc_sidx,:),1);
end

% oxygen
obvec = lo:1:ho;
nO = length(obvec);
obin = nan(nz, nO);
for oidx = 1:nO;
    o = obvec(oidx);
    loc_osidx = find(lowo_b >= o & lowo_b < o+1);
    obin(:,oidx) = nanmean(o_sorted(loc_osidx, :), 1);
end


% interpolate all profiles
% note: basin_profile_avg calls grid.zw, rather than grid.zt, I
% think the latter is more appropriate
binned_profiles.T = nan(length(z), nT);
binned_profiles.PD = nan(length(z), nT);
for tidx = 1:nT
    binned_profiles.T(:,tidx) = spline(grid.zt, tbin(:,tidx), z);
    binned_profiles.PD(:,tidx) = spline(grid.zt, pdbin(:,tidx), z);
end
binned_profiles.O = nan(length(z), nO);
for oidx = 1:nO-1
    try
    binned_profiles.O(:,oidx) = spline(grid.zt, obin(:,oidx), z);
    end
end


%%%%%%%%%%%%%%%%%%%%%%%%%%
%% setup for figure 1 %%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Shows variability in flux proviles when individual parameters
%% are varied

%%


% average tempError: Expression or statement is incorrect--possibly unbalanced (, {, or [.erature, density and oxygen over all regions
basin_profiles.T = basin_profile_avg(nanmean(parJ.phys.temp,4), grid, R3d, ...
                                     z);

basin_profiles.pdens = basin_profile_avg(nanmean(parJ.phys.pdens, 4), grid, R3d, ...
                                         z);
basin_profiles.O2 =  basin_profile_avg(nanmean(parJ.phys.o2, 4), grid, R3d, ...
                                         z);
% non-optimized estemates of model parameters
Cr4 = 0.5; % base remineralization rate
Q10 = 2; %  temperature dependence of reminerilization
alpha = 2.3; % particle fractal dimension
KmOxy = 10; % oxygen dependence of reminerilization

% for the base case, just average all of the eight regions together
null_dens = repmat(mean(basin_profiles.pdens(:)), length(z), 1);
null_temp = repmat(mean(basin_profiles.T(:)), length(z), 1);
null_oxy =  repmat(mean(basin_profiles.O2(:)), length(z), 1);
null_si = mean(things.mpsi);
null_car = mean(things.mpcar); 

% standard particle size (um)
xmm = 20;
% standard partticle sinking speed (m/d)
wxmm = 2; 
% standar particle mass (arbitrary
mxmm = 0.004;
% upper and lower bounds of particle size distribution
ds = 2;
dl = 4e3;

% mass of a standard particle
rho_std = parJ.component.rho_std;
% an alternative average particle mass, I think this is closer to
% the global average
rho_std1 = 1.092; %average particle mass
rho_water = 1.0250;
rho_POM = parJ.component.rho_POM;

% standard particle size distribution
stdPSD = 3.5;
%%
% a base flux profile, to which other profiles are compared
prof.base = pvsto_limw3(alpha, stdPSD, alpha-1, Cr4, ...
                       Q10, KmOxy, xmm, mxmm, wxmm, 4, ds, dl, ...
                       rho_std,  rho_std, rho_std, ...
                       null_dens/1, rho_water, ...
                       1, null_temp, null_oxy, z, 1000);


 
ocn_abr = {'AA', 'STP'};
ocn_n = [1 4];

% flux profiles where I vary different parameters
% Viscosity + Density
Q10_null = 1;
KmOxy_null = 0;
for i = 1:2
    basinN = ocn_n(i)
    prof.visden.(ocn_abr{i}) = pvsto_limw3(alpha, stdPSD, alpha-1, Cr4, ...
                                          Q10_null, KmOxy_null, xmm, mxmm, wxmm, 4,ds, dl, ...
                                          rho_std,  rho_std, rho_std, ...
                                          basin_profiles.pdens(:,basinN)/1, rho_water, ...
                                          1, basin_profiles.T(:,basinN), basin_profiles.O2(:,basinN), ...
                                          z, 1000); % 1.033
end

%  Q10 Only
Qos = {'Qo2', 'Qo3'};
Qvals = {2, 3};
for h = 1:2
    for i = 1:2
        basinN = ocn_n(i)
        prof.(Qos{h}).(ocn_abr{i}) = pvsto_limw3(alpha, stdPSD, alpha-1, Cr4, ...
                                                          Qvals{h}, KmOxy_null,xmm, mxmm, wxmm, 3,ds, dl, ...
                                                          rho_std,  rho_std, rho_std, ...
                                                          null_dens/1, rho_water, ...
                                                          1, basin_profiles.T(:,basinN), basin_profiles.O2(:,basinN),...
                                                          z, 1000); % 1.033
    end
end

% Viscosity + Density + Q10
Qs = {'Q2', 'Q3'};
Qvals = {2, 3};
for h = 1:2
    for i = 1:2
        basinN = ocn_n(i)
        prof.(Qs{h}).(ocn_abr{i}) = pvsto_limw3(alpha, stdPSD, alpha-1, Cr4, ...
                                                          Qvals{h}, KmOxy_null,xmm, mxmm, wxmm, 4,ds, dl, ...
                                                          rho_std,  rho_std, rho_std, ...
                                                          basin_profiles.pdens(:,basinN)/1, rho_water, ...
                                                          1, basin_profiles.T(:,basinN), basin_profiles.O2(:,basinN),...
                                                          z, 1000); % 1.033
    end
end

%PSD
bsn.PSD = basin_sum(parJ.PSD .* parJ.EX, R2d)./...
          basin_sum(parJ.EX, R2d);
psd_vec = [2.5 3.5 4.5 3 4];
psd_abr = {'x25', 'x35', 'x45', 'x30', 'x40'};

for i = 1:length(psd_vec)
    prof.PSD.(psd_abr{i}) = pvsto_limw3(alpha, psd_vec(i), alpha-1, Cr4, ...
                                        Q10, KmOxy, xmm, mxmm, wxmm, 4, ds, dl, ...
                                        rho_std, rho_std, rho_std, ...
                                        null_dens/1, rho_water, ...
                                        1, null_temp, null_oxy, z, 1000);
end

%% If I don't have this break here, the code fails for reasons that
%% I don't understand (when I runn cell by cell)
% Error: Expression or statement is incorrect--possibly unbalanced
% (, {, or [.
% 
% I'm just leaving it here for now, run these cells seperately or
% copy paste into the terminal

%  Opal + Car
bsn.carFco = things.mpcar;
bsn.siFco =  things.mpsi;
bsn.CFco = 1-bsn.carFco - bsn.siFco;
for i = 1:2
    basinN = ocn_n(i);
    prof.bal.(ocn_abr{i}) = pvsto_limw3(alpha, stdPSD, alpha-1, [0 Cr4 Cr4], ...
                                       [Q10 Q10 Q10], repmat(KmOxy_null, 1, 3), xmm, mxmm, wxmm, 4, ds, dl, ...
                                       rho_std, ...
                                       [parJ.component.rho_CaCO3,parJ.component.rho_opal,...
                        parJ.component.rho_POM],...
                                       [parJ.component.rho_CaCO3,parJ.component.rho_opal,...
                        parJ.component.rho_POM * parJ.component.c2pom],...
                                       null_dens/1, rho_water, ...
                                       [bsn.carFco(basinN), bsn.siFco(basinN),...
                        1-bsn.carFco(basinN)-bsn.siFco(basinN)],...
                                       null_temp, null_oxy, z, 1000);
end

prof.bal.Unbal = pvsto_limw3(alpha, stdPSD, alpha-1, [0 Cr4 Cr4], ...
                                       [Q10 Q10 Q10],repmat(KmOxy_null, 1, 3), xmm, mxmm, wxmm, 4, ds, dl, ...
                                       rho_std, ...
                                       [parJ.component.rho_CaCO3,parJ.component.rho_opal,...
                        parJ.component.rho_POM],...
                                       [parJ.component.rho_CaCO3,parJ.component.rho_opal,...
                        parJ.component.rho_POM * parJ.component.c2pom],...
                                       null_dens/1, rho_water, ...
                                       [0 0 1],...
                                       null_temp, null_oxy, z, 1000);

% Oxygen
% average all profiles with lowest value < 2ppm
omz_prof = nanmean(binned_profiles.O(:, 1:4), 2);
%all_prof
globalOxy_prof = spline(grid.zt, squeeze(nanmean(nanmean(parJ.phys.o2,1), 2)), z)';

ocn_n_oxy = [1 4 6];
ocn_abr_oxy = [regabr(ocn_n_oxy), 'OMZ', 'GLO'];
kms = {'oxy10', 'oxy20', 'oxy30'};
kmvals = {10, 20 ,30};
for k = 1:3
    for i = 1:3
        basinN = ocn_n_oxy(i)
        prof.(kms{k}).(ocn_abr_oxy{i}) = pvsto_limw3(alpha, stdPSD, alpha-1, Cr4, ...
                                                   Q10_null, kmvals{k}, xmm, mxmm, wxmm, 4,ds, dl, ...
                                                   rho_std,  rho_std, rho_std, ...
                                                   null_dens/1, rho_water, ...
                                                   1, null_temp, basin_profiles.O2(:,basinN), ...
                                                   z, 1000); % 1.033
    end
end
% for the OMZ
for k = 1:3
    prof.(kms{k}).OMZ = pvsto_limw3(alpha, stdPSD, alpha-1, Cr4, ...
                                                   Q10_null, kmvals{k}, xmm, mxmm, wxmm, 4,ds, dl, ...
                                                   rho_std,  rho_std, rho_std, ...
                                                   null_dens/1, rho_water, ...
                                                   1, null_temp, omz_prof, ...
                                                   z, 1000); % 1.033
end

% for global oxygen levels
for k = 1:3
    prof.(kms{k}).GLO = pvsto_limw3(alpha, stdPSD, alpha-1, Cr4, ...
                                                   Q10_null, kmvals{k}, xmm, mxmm, wxmm, 4,ds, dl, ...
                                                   rho_std,  rho_std, rho_std, ...
                                                   null_dens/1, rho_water, ...
                                                   1, null_temp, globalOxy_prof, ...
                                                   z, 1000); % 1.033
end


%% Save out basin data for demonstration files
basin_data.basin_profiles = basin_profiles;
basin_data.bsn.PSD = things.PSD;
basin_data.bsn.mpcar = things.mpcar;
basin_data.bsn.mpsi = things.mpsi;
basin_data.bsn.mpC = 1 - things.mpcar - things.mpsi;
basin_data.regnames = regnames;
basin_data.regabr = regabr;

save('ballasted_sinking_environment.mat', 'basin_data')

%%%%%%%%%%%%%%%%%%%%
%% Plot Figure 1%%
%% Flux profiles%%
%%%%%%%%%%%%%%%%%%%%%%%
f1 = figure(1);
clf;
set(gcf, 'units','normalized','outerposition',[0 0 1 1],...
        'Color', [1 1 1])
default_colors = get(groot,'DefaultAxesColorOrder');
los_linS = {'-', '--', ':', '-.', '-'};
legtext = ['base', ocn_abr(1:2)];
legtext2 = [ocn_abr(1:2), 'Unbal'];

% converting numeric vectors to formatted cell arrays of strings
strs.PSD = {'2.5', '3.5', '4.5'};

strs.si = [ cellfun(@(x)num2str(x, '%.2g'),num2cell(bsn.siFco(ocn_n)*100), ...
                   'UniformOutput', false), {'0'}];
strs.car2 = [cellfun(@(x)num2str(x, '%.2g'),num2cell(bsn.carFco(ocn_n)*100), ...
                            'UniformOutput', false), {'0'}];
% legend text variables
lt.si =  strcat(legtext, {'; si = '} , strs.si);
lt.car2 =  strcat(legtext, {'; CaCO3 = '} , strs.car2);
lt.bal =  [{'base'},strcat(legtext2, {'; CaCO3 = '} , strs.car2, {'%; Si = '} ...
                 , strs.si, {char(37)})];
% the regions currently get the wrong ballast percentages plotted
% by them. I'm correcting in inkscape for now.

% appending formatted arrays together
%lt.PSD = strcat( {'PSD = '} , strs.PSD);
lt.PSD = ['base', strcat({'PSD = '} , strs.PSD)];

lt.visc = strcat(['base', ocn_abr(1:2), ocn_abr(1:2)],...
                 {'', '; Q10 = 2', '; Q10 = 2', '; Q10 = 3', ['; Q10 ' ...
                    '= 3']});
% lt.oxy = strcat(['base', ocn_abr_oxy([1,3:4]), ocn_abr_oxy([1,3:4])],...
%                 {' '},...
%                 {'','Km_{Oxy} = 10','Km_{Oxy} = 10','Km_{Oxy} = 10',...
%                  'Km_{Oxy} = 30','Km_{Oxy} = 30','Km_{Oxy} = 30'});
lt.oxy = strcat(['base', ocn_abr_oxy([4:5])])


% font size
fs = 18;



% Viscosity + Density
subplot(2,3,2)
hold all;

set(gca, 'Ydir', 'reverse','FontSize', fs)
ylim([0 1000]); xlim([0 1])
%xlabel('Normalized Flux');
ylabel('Depth')
plot(prof.base.FZ, z, 'Color', [0.8 0.8 0.8], 'LineWidth', 3)
for i = 1:2

    plot(prof.visden.(ocn_abr{i}).FZ, z, 'Color', 'k', 'linestyle', los_linS{i})
end

legend(['base', ocn_abr], 'Location', 'NorthWest')
set(gca, 'xscale', 'log')%, 'TickLength', [0.4 0.1]);
xlim([0.1, 1])
title('Viscosity + Density')

% % Q10 Only (Not currently using)
% col_ls = {'k', 'k', [0, 0, 1], [0, 0, 1]}
% los_linSS = {'-', '--', '-', '--'}

% subplot(2,3, 2)
% hold all;
% set(gca, 'Ydir', 'reverse','FontSize', fs)
% ylim([0 1000]); xlim([0 1])
% xlabel('Normalized Flux'); ylabel('Depth')
% plot(prof.base.FZ, z, 'Color', [0.8 0.8 0.8], 'LineWidth', 3)
% iter = 0;
% for h = 1:2
%     for i = 1:2
%         iter = iter + 1;
%         plot(prof.(Qos{h}).(ocn_abr{i}).FZ, z, 'Color', col_ls{iter}, 'linestyle', los_linSS{iter})
%     end
% end
% legend(lt.visc, 'Location', 'NorthWest')
% title(['Biological Temperature Effect (Q10)'])
% set(gca, 'xscale', 'log'); xlim([0.05, 1]);




% Viscosity + Density + Q10
 col_ls = {'k', 'k', [0, 0, 1], [0, 0, 1]}
 los_linSS = {'-', '--', '-', '--'}
subplot(2,3, 3)

hold all;
set(gca, 'Ydir', 'reverse','FontSize', fs)
ylim([0 1000]); xlim([0 1])
%xlabel('Normalized Flux');
%ylabel('Depth')
plot(prof.base.FZ, z, 'Color', [0.8 0.8 0.8], 'LineWidth', 3)
iter = 0;
for h = 1:2
    for i = 1:2
        iter = iter + 1;
        plot(prof.(Qs{h}).(ocn_abr{i}).FZ, z, 'Color', col_ls{iter}, 'linestyle', los_linSS{iter})
    end
end
legend(lt.visc, 'Location', 'NorthWest')
title(['Viscosity + Density + Q10'])
set(gca, 'xscale', 'log'); xlim([0.1, 1]);

% Oxygen
col_ls2 = { 'k', [0, 0, 1]}

subplot(2,3,4)
hold all;
set(gca, 'Ydir', 'reverse','FontSize', fs)
ylim([0 1000]); xlim([0 1])
xlabel('Normalized Flux');
ylabel('Depth')
plot(prof.base.FZ, z, 'Color', [0.8 0.8 0.8], 'LineWidth', 3)
k = 2
    for i = 4:5

        plot(prof.(kms{k}).(ocn_abr_oxy{i}).FZ, z, 'Color', 'k', 'linestyle', los_linS{i})
    end


legend(lt.oxy, 'Location', 'NorthWest')
set(gca, 'xscale', 'log'); xlim([0.1, 1]);
title('Oxygen')

% PSD
subplot(2,3,5)
hold all;
set(gca, 'Ydir', 'reverse','FontSize', fs)
ylim([0 1000]); xlim([0 1])
xlabel('Normalized Flux');
%ylabel('Depth')
plot(prof.base.FZ, z, 'Color', [0.8 0.8 0.8], 'LineWidth', 3)
for i = 1:3
    plot(prof.PSD.(psd_abr{i}).FZ, z, 'Color', 'k', 'linestyle', los_linS{i})
end
legend(lt.PSD, 'Location', 'NorthWest')
set(gca, 'xscale', 'log'); xlim([0.1, 1]);
title('Particle Size Distribution')

% ballast
 bal_abr = [ ocn_abr, {'Unbal'}];
subplot(2,3,6)
hold all;
set(gca, 'Ydir', 'reverse', 'FontSize', fs)
ylim([0 1000]); xlim([0 1])
xlabel('Normalized Flux');
%ylabel('Depth')
plot(prof.base.FZ, z, 'Color', [0.8 0.8 0.8], 'LineWidth', 3)
for i = 1:3
    plot(prof.bal.(bal_abr{i}).FZ(:,3), z, 'Color', 'k', 'linestyle', los_linS{i})
end
legend(lt.bal, 'Location', 'NorthWest')
set(gca, 'xscale', 'log'); xlim([0.1, 1]);
title('Ballast')

%% Figure 1A (profiles of water temperature, density, oxygen)
figure(1)
%clf
set(gcf,'PaperUnits','inches','PaperPosition',[0 0 12 4])
subplot(2,9,1)
hold all;
set(gca, 'Ydir', 'reverse', 'FontSize', fs)
ylim([0 1000]);
xlabel('Temperature'); ylabel('Depth')
plot(repmat(4, length(z), 1), z, 'Color', [0.9 0.9 0.9], 'LineWidth', 3)
for i = 1:2
    plot(basin_profiles.T(:,ocn_n(i)), z, 'Color', 'k', 'linestyle', los_linS{i})
end
legend(['base', ocn_abr], 'Location', 'NorthWest')

subplot(2,9,2)
hold all;
set(gca, 'Ydir', 'reverse', 'FontSize', fs)
ylim([0 1000]);
xlabel('Potential Density kg/m^3'); ylabel('Depth')
plot(null_dens, z, 'Color', [0.9 0.9 0.9], 'LineWidth', 3)
for i = 1:2
    plot(basin_profiles.pdens(:,ocn_n(i)), z, 'Color', 'k', 'linestyle', los_linS{i})
end
legend(['base', ocn_abr], 'Location', 'NorthWest')

subplot(2,9,3)
hold all;
set(gca, 'Ydir', 'reverse', 'FontSize', fs)
ylim([0 1000]);
xlabel('Oxygen uM'); ylabel('Depth')
plot(null_oxy, z, 'Color', [0.9 0.9 0.9], 'LineWidth', 3)
plot(omz_prof, z, 'Color', 'k', 'linestyle', los_linS{4})
plot(globalOxy_prof, z, 'Color', 'k', 'linestyle', los_linS{5})
legend(['base', ocn_abr_oxy([4,5])], 'Location', 'NorthWest')

%% Actually print that figure


set(gcf,'PaperUnits','inches','PaperPosition',[0 0 14 8])
print('plots/ProtoProfiles' , '-dsvg')
print('plots/ProtoProfiles' , '-depsc')

% %% depreciated since now printing basin profiles with fig 1
% set(gcf,'PaperUnits','inches','PaperPosition',[0 0 12 6])
% print('plots/ProfileAddOns' , '-dsvg')
% print('plots/ProfileAddOns' , '-depsc')

%% Variations in teff in these plots for the paper.
% difference in TEFF between AA and SO with just physical effects
prof.visden.STP.F - prof.visden.AA.F
prof.PSD.x25.F
prof.PSD.x35.F
prof.PSD.x45.F


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Back of the Envelope Calculations
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% These aren't any particlular figure, but I do refer to them in
%% the text.

things.vpsi = things.mpsi./ parJ.component.rho_opal./...
    (things.mpsi ./ parJ.component.rho_opal + ...
        things.mpcar ./ parJ.component.rho_CaCO3 + ...
        (1 - things.mpsi - things.mpcar) ./...
         (parJ.component.rho_POM .* parJ.component.c2pom));
things.vpcar = things.mpcar./ parJ.component.rho_opal./...
    (things.mpsi ./ parJ.component.rho_opal + ...
        things.mpcar ./ parJ.component.rho_CaCO3 + ...
        (1 - things.mpsi - things.mpcar) ./...
         (parJ.component.rho_POM .* parJ.component.c2pom));
things.dens = things.vpsi .* parJ.component.rho_opal + ...
    things.vpcar .* parJ.component.rho_CaCO3 + ...
    (1 - things.vpsi - things.vpcar) .* ...
    parJ.component.rho_POM;

% global averages
mean_things.vpsi = mean_things.mpsi./ parJ.component.rho_opal./...
    (mean_things.mpsi ./ parJ.component.rho_opal + ...
        mean_things.mpcar ./ parJ.component.rho_CaCO3 + ...
        (1 - mean_things.mpsi - mean_things.mpcar) ./...
         (parJ.component.rho_POM .* parJ.component.c2pom));
mean_things.vpcar = mean_things.mpcar./ parJ.component.rho_opal./...
    (mean_things.mpsi ./ parJ.component.rho_opal + ...
        mean_things.mpcar ./ parJ.component.rho_CaCO3 + ...
        (1 - mean_things.mpsi - mean_things.mpcar) ./...
         (parJ.component.rho_POM .* parJ.component.c2pom));
mean_things.dens = mean_things.vpsi .* parJ.component.rho_opal + ...
    mean_things.vpcar .* parJ.component.rho_CaCO3 + ...
   (1 - mean_things.vpsi - mean_things.vpcar) .* ...
    parJ.component.rho_POM;


things.reldens = things.dens - 1.025;
% difference in sinking speed between AA and STP as a result of
% ballast
things.reldens(1)/things.reldens(4)



%% particle sinking speed back of the envelope calculations
%back of the envelope surface sinking speed as a function of size
prof.PSD.x35.W(1)./prof.PSD.x45.W(1)
prof.PSD.x25.W(1)./prof.PSD.x35.W(1)

% sinking speeds converge with depth, when the differences are
% caused by particle size
figure;
clf; subplot(121);
plot(prof.PSD.x30.W, -z)
hold all;
plot(prof.PSD.x40.W, -z) 
set(gca, 'yscale', 'log')
ylabel('Depth (m)');
xlabel('Sinking Speed (m/d)')
legend('Beta = 3', 'Beta = 4', 'Location', 'Best')
subplot(122);
plot(prof.PSD.x30.W./prof.PSD.x40.W, -z)
ylabel('Depth');
ylabel('Sinking Speed Ratio Beta = 0.3/ Beta = 0.4')
set(gca, 'yscale', 'log')
% I don't use this figure in the paper.

%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Figure 2 TEFF Contours%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%


%% Contour Calculations
range.Q10 = 1:0.1:4;
range.siF = [0:.1:0.9, .91:0.02:0.99, 0.999, 0.9999]
range.beta2 = 0:0.2:6;
range.beta3 = 2.5:0.2:4.5;
range.alpha = 1:.1:3;
range.gamma = 0:0.1:2.3;
range.T = -2:1:27;
%range.O = unique([2:1:5,5:5:30,30:10:100,100:25:200]);
range.O = unique([2:1:10,5:5:100,100:25:300]);


Q10 = 2.5

% O vs T
%basinN = 4;
ot_teffs = nan(length(range.O), length(range.T));
tic
for t = 1:length(range.T)
    t
    loc_T = range.T(t);
    tprof_idx = find(tbvec == loc_T); 
    tprof_loc = binned_profiles.T(:,tprof_idx);
    pdprof_loc = binned_profiles.PD(:,tprof_idx);
    for o = 1:length(range.O)
        o
        loc_O = range.O(o);
        oprof_idx = find(obvec == loc_O);
        oprof_loc = binned_profiles.O(:,oprof_idx);
        loc_sol = pvsto_limw3(alpha,stdPSD , alpha-1, [0 Cr4 Cr4], ...
                              repmat(Q10, 1,3), repmat(KmOxy,1,3),...
                              xmm, mxmm, wxmm,...
                              4,2, 4e3, ...
                              rho_std, ...
                              [parAm6.component.rho_CaCO3,parAm6.component.rho_opal,...
                            parAm6.component.rho_POM], ...
                              [parAm6.component.rho_CaCO3,parAm6.component.rho_opal,...
                            (parAm6.component.rho_POM)* parAm6.component.c2pom], ...
                              pdprof_loc, rho_water, ...
                              [mean_things.mpcar, mean_things.mpsi, (1 - mean_things.mpcar - mean_things.mpsi)],...
                              tprof_loc,oprof_loc, z, ...
                              1000);
        loc_teff = loc_sol.F(3);
        ot_teffs(o,t) = loc_teff;
    end
end
toc

% beta vs T (Q10 = 2)
tb_teffs = nan(length(range.beta3), length(range.T)); 
for t = 1:length(range.T)
    t
    loc_T = range.T(t);
    tprof_idx = find(tbvec == loc_T); 
    tprof_loc = binned_profiles.T(:,tprof_idx);
    pdprof_loc = binned_profiles.PD(:,tprof_idx);
    for b = 1:length(range.beta3)
        b
        loc_beta = range.beta3(b);
        loc_sol = pvsto_limw3(alpha, loc_beta, alpha-1, [0 Cr4 Cr4], ...
                              repmat(Q10, 1,3), repmat(KmOxy,1,3),...
                              xmm, mxmm, wxmm,...
                              4,2, 4e3, ...
                              rho_std, ...
                              [parAm6.component.rho_CaCO3,parAm6.component.rho_opal,...
                            parAm6.component.rho_POM], ...
                              [parAm6.component.rho_CaCO3,parAm6.component.rho_opal,...
                            (parAm6.component.rho_POM)* parAm6.component.c2pom], ...
                              pdprof_loc, rho_water, ...
                              [mean_things.mpcar, mean_things.mpsi, (1 - mean_things.mpcar - mean_things.mpsi)],...
                              tprof_loc,null_oxy, z, ...
                              1000);
        loc_teff = loc_sol.F(3);
        tb_teffs(b,t) = loc_teff;
    end
end

        

% beta vs %Si
sib_teffs = nan(length(range.beta3), length(range.siF));
range.rho_part = nan(1,length(range.siF));
for si = 1:length(range.siF)
    si
    loc_si = range.siF(si)
    for b = 1:length(range.beta3)
        b
        loc_beta = range.beta3(b);
        loc_sol = pvsto_limw3(alpha, loc_beta, alpha-1, [0 Cr4 Cr4], ...
                              repmat(Q10, 1,3), repmat(KmOxy,1,3),...
                              xmm, mxmm, wxmm,...
                              4,2, 4e3, ...
                              rho_std, ...
                              [parAm6.component.rho_CaCO3,parAm6.component.rho_opal,...
                            parAm6.component.rho_POM], ...
                              [parAm6.component.rho_CaCO3,parAm6.component.rho_opal,...
                            (parAm6.component.rho_POM)* parAm6.component.c2pom], ...
                              null_dens, rho_water, ...
                              [0, loc_si, 1 - loc_si],...
                             null_temp,null_oxy, z, ...
                              1000);
        loc_teff = loc_sol.F(3);
        sib_teffs(b, si) = loc_teff;
        if b == 1
            % save out particle densities corresponding to given
            % amount of silicate
            range.rho_part(1,si) = loc_sol.all.rvrs1 * loc_sol.all.rs;
        end
    end
end

%% Consideration of particle shape
% beta vs alpha
ab_teffs = nan(length(range.beta3), length(range.alpha)); 
for a = 1:length(range.alpha)
    a
    loc_alpha = range.alpha(a);
    for b = 1:length(range.beta3)
        b
        loc_beta = range.beta3(b);
        loc_sol = pvsto_limw3(loc_alpha, loc_beta, loc_alpha-1, [0 Cr4 Cr4], ...
                              repmat(Q10, 1,3), repmat(KmOxy,1,3),...
                              xmm, mxmm, wxmm,...
                              4,2, 4e3, ...
                              rho_std, ...
                              [parAm6.component.rho_CaCO3,parAm6.component.rho_opal,...
                            parAm6.component.rho_POM], ...
                              [parAm6.component.rho_CaCO3,parAm6.component.rho_opal,...
                            (parAm6.component.rho_POM)* parAm6.component.c2pom], ...
                              null_dens, rho_water, ...
                              [mean_things.mpcar, mean_things.mpsi, (1 - mean_things.mpcar - mean_things.mpsi)],...
                              null_temp,null_oxy, z, ...
                              1000);
        loc_teff = loc_sol.F(3);
        ab_teffs(b,a) = loc_teff;
    end
end

% beta vs gamma
g = 14;
b = 6;
gb_teffs = nan(length(range.beta3), length(range.alpha)); 
for g = 1:length(range.gamma)
    g
    loc_gamma = range.gamma(g);
    for b = 1:length(range.beta3)
        b
        loc_beta = range.beta3(b);
        loc_sol = pvsto_limw3(alpha, loc_beta, loc_gamma, [0 Cr4 Cr4], ...
                              repmat(Q10, 1,3), repmat(KmOxy,1,3),...
                              xmm, mxmm, wxmm,...
                              4,2, 4e3, ...
                              rho_std, ...
                              [parAm6.component.rho_CaCO3,parAm6.component.rho_opal,...
                            parAm6.component.rho_POM], ...
                              [parAm6.component.rho_CaCO3,parAm6.component.rho_opal,...
                            (parAm6.component.rho_POM)* parAm6.component.c2pom], ...
                              null_dens, rho_water, ...
                              [mean_things.mpcar, mean_things.mpsi, (1 - mean_things.mpcar - mean_things.mpsi)],...
                              null_temp,null_oxy, z, ...
                              1000);
        loc_teff = loc_sol.F(3);
        gb_teffs(b,g) = loc_teff;
    end
end

% alpha vs gamma
ag_teffs = nan(length(range.gamma), length(range.alpha)); 
for a = 1:length(range.alpha)
    a
    loc_alpha = range.alpha(a);
    for g = 1:length(range.gamma)
        g
        loc_gamma = range.gamma(g);
        loc_sol = pvsto_limw3(loc_alpha, stdPSD, loc_gamma, [0 Cr4 Cr4], ...
                              repmat(Q10, 1,3), repmat(KmOxy,1,3),...
                              xmm, mxmm, wxmm,...
                              4,2, 4e3, ...
                              rho_std, ...
                              [parAm6.component.rho_CaCO3,parAm6.component.rho_opal,...
                            parAm6.component.rho_POM], ...
                              [parAm6.component.rho_CaCO3,parAm6.component.rho_opal,...
                            (parAm6.component.rho_POM)* parAm6.component.c2pom], ...
                              null_dens, rho_water, ...
                              [mean_things.mpcar, mean_things.mpsi, (1 - mean_things.mpcar - mean_things.mpsi)],...
                              null_temp,null_oxy, z, ...
                              1000);
        loc_teff = loc_sol.F(3);
        ag_teffs(g,a) = loc_teff;
    end
end


%% Alternatively, skip the for loops and just load in the relevant
%% data
% save('./data/contourTBSidata.mat', 'ot_teffs', 'tb_teffs', ...
%       'sib_teffs', 'range', 'ab_teffs', 'gb_teffs', 'ag_teffs')
% load('./data/contourTBSidata.mat')


%% plot the contours
% O vs T
f2 = figure(2);
set(gcf,'PaperUnits','inches','PaperPosition',[0 0 12 12])
set(gcf, 'units','normalized','outerposition',[.4 0 .6 1],...
        'Color', [1 1 1])
set(f2, 'Color', [1 1 1])
jcontour_vec = [0.001, 0.01,.1:.1:1];
jcontour_vec2 = [.1:.025:.5];
jcontour_vec3 = [.37:.001:.38];
jcontour_vec4 = [0.1:0.05:1];


clf
subplot(221)

hold all;

plot(things.TSurf, things.PSD, 'o', 'color',  [.1 0 0], ...
     'MarkerFaceColor', [0.5 0 0], 'linewidth',  2); 
text(things.TSurf + 0.5, things.PSD, regabr, 'Color', [0.5 0 0], 'fontsize', ...
     14); 

[C, h] = contour( range.T,range.beta3', tb_teffs, jcontour_vec4, '-k');
ylabel('\beta'); xlabel('Temperature \circC')
clabel(C,h, 'FontSize', 14); 
% note, for clabel fontsize to work, one needs to apply this patch:
% https://www.mathworks.com/support/bugreports/1114747
%set(gca, 'yscale', 'log')
set(gca, 'FontSize', 14)


subplot(222)

hold all
plot(things.dens, things.PSD, 'o', 'color',  [.1 0 0],...
     'MarkerFaceColor', [0.5 0 0], 'linewidth',  2); 
text(things.dens + .0015, things.PSD, regabr, 'Color', [0.5 0 0], 'fontsize', ...
     14)
% [C, h] = contour( range.siF,range.beta3', sib_teffs, jcontour_vec, '-k');
% ylabel('\beta'); xlabel('Silicate Mass Percent')
[C, h] = contour( range.rho_part,range.beta3', sib_teffs, jcontour_vec4, ...
                  '-k');
xlim([1.1, 1.25])
ylabel('\beta'); xlabel('Particle Density (g/ml)')
clabel(C,h, 'FontSize', 14);
set(gca, 'FontSize', 14)

subplot(223)
hold all
plot(things.TSurf, things.ox200, 'o', 'color',  [.1 0 0],...
     'MarkerFaceColor', [0.5 0 0], 'linewidth',  2); 
plot(range.T, repmat(20,size(range.T)) , '--b', 'linewidth', 2);
plot(range.T, repmat(5,size(range.T)) , ':b', 'linewidth', 2);
plot(range.T, repmat(30,size(range.T)) , ':b', 'linewidth', 2);
text(things.TSurf + .5, things.ox200, regabr, 'color', [0.5 0 0], 'fontsize', ...
     14)
[C, h] = contour( range.T,range.O', ot_teffs, jcontour_vec4, '-k');
ylabel('Oxygen \muM'); xlabel('Temperature \circC')
clabel(C,h, 'FontSize', 14);
set(gca, 'yscale', 'log')
set(gca, 'FontSize', 14)

%%
print('plots/contour' , '-dpng')
export_fig('plots/contour' , '-eps')
print('plots/contour' , '-dsvg')

%% Particle shape plots
f2 = figure(2);
set(gcf,'PaperUnits','inches','PaperPosition',[0 0 12 12])
set(gcf, 'units','normalized','outerposition',[.4 0 .6 1],...
        'Color', [1 1 1])
set(f2, 'Color', [1 1 1])

clf
subplot(221)

hold all;

[C, h] = contour( range.alpha,range.beta3', ab_teffs, jcontour_vec, '-k');
ylabel('\beta'); xlabel('\alpha [\gamma = \alpha -1]')
clabel(C,h, 'FontSize', 14); 
xlim([2,3])
set(gca, 'FontSize', 14)

subplot(222)
hold all;
[C, h] = contour( range.gamma,range.beta3', gb_teffs, jcontour_vec, '-k');
ylabel('\beta'); xlabel('\gamma [\alpha = 2.3]')
clabel(C,h, 'FontSize', 14); 
xlim([1,2])
set(gca, 'FontSize', 14)

subplot(223)
[C, h] = contour( range.alpha,range.gamma', ag_teffs, jcontour_vec, '-k');
ylabel('\gamma'); xlabel('\alpha')
clabel(C,h, 'FontSize', 14); 
xlim([2,3])
ylim([1,2])
set(gca, 'FontSize', 14)

%%
print('plots/contour_ag' , '-dpng')
export_fig('plots/contour_ag' , '-eps')
print('plots/contour_ag' , '-dsvg')

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Figure S1 Input Data%%%%%%%%%%%%%%
%% Region map with surface export%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

f3 = figure(3);
set(f3, 'units','normalized','outerposition',[0 0 1 1],...
        'Color', [1 1 1])

% Contours m_map
nreg = length(regnames);
land = 1- M3d(:,:,1);

clf
lon = grid.xt;
lat = grid.yt;
%map = log10(nanmean(parJ.EX,3));
%map = log10(parJ.EX);
map = (parJ.EX * 365.25/1000);
map(~isfinite(map)) = NaN;

lon2 = reshape(lon, length(lon), 1);
lat2 = reshape(lat, length(lat), 1);

shift_by = 170;
%shift_by = 180;
dx_plot=60;
dy_plot=30;
lon_shift = circshift(lon2,[shift_by 0]);
dlon = diff(lon_shift);
idx = find(dlon<0,1,'first');
lon_shift(idx+1:end) = lon_shift(idx+1:end) + 360.;

map_shift = circshift(map,[0 shift_by]);
R2d_shift = circshift(R2d,[0 shift_by]);
land_shift = circshift(land, [0 shift_by]);

m_proj('moll','lon',[min(lon_shift) max(lon_shift)])
a = m_pcolor(lon_shift,lat2',map_shift);shading('flat');
c = colorbar;
c.Label.String = 'Export (mol C / m^2 / year)';
c.FontSize = 18
v = caxis;
%b = m_contour(lon_shift,lat2',R2d_shift)
hold all
for i = 1:nreg
    RR = double(R2d_shift==i);
    RR(land_shift==1) = NaN;
    [cc,hh] = m_contour(lon_shift,lat2',RR,[.5 .5],'k','linewidth',1);
end
caxis(v)
caxis([0 6])

colormap jet;

m_grid('xtick',[-360:dx_plot:360],'ytick',[-90: ...
                    dy_plot:90],'fontsize',14, 'xaxislocation', 'middle');
m_coast('patch',[.9 .9 .9]);

set(gcf,'PaperUnits','inches','PaperPosition',[0 0 16 8])
print('plots/exportregion' , '-dpng')
export_fig('plots/exportregion' , '-eps')

%% additional input data figures  have to be run after the
%% code calculating transfer efficiency. This is because the
%% transfer efficinecy code also runs the density calculations,
%% which go in these input figures.



%% Calculations for estemating transfer efficiency
%% Give each of these a few days to a week to run on a 12 node
%% computer with 2016 technology.
options = optimset('Display','iter', 'TolX', 5e-2);
options1 = options;
options1.TolX = 0.05;
options.TolFun = 0.0001;
options.TolX = 1;


[CrsJtoptall.TObBa fvalsJ16toptall.TObBa] = fminsearchbnd(@(p)fval_fwteff_type_err_17([p(1)/100, ...
                    p(2), 1, 1, 2.3, 1.3, 1, 0, 1.033, p(3)], parJ, teff_obs, teff_err), ...
                                              [135, 2.5, 10],...
                                              [0 1 0], [500 4 25], ...
                                              options1);
[CrsJtoptall.TOb fvalsJtoptall.TOb] = fminsearchbnd(@(p)fval_fwteff_type_err_17([p(1)/100, ...
                    CrsJtoptall.TObBa(2), 1, 1, 2.3, 1.3, 1, 0, 1.033, CrsJtoptall.TObBa(3)], parJ_flatBal, teff_obs, teff_err), ...
                                              [CrsJtoptall.TObBa(1)],...
                                               [0], [500], ...
                                              options);

[CrsJtoptall.TOBa fvalsJ16toptall.TOBa] = fminsearchbnd(@(p)fval_fwteff_type_err_17([p(1)/100, ...
                    CrsJtoptall.TObBa(2), 1, 1, 2.3, 1.3, 0, 0, 1.033, CrsJtoptall.TObBa(3)], parJ, teff_obs, teff_err), ...
                                              [CrsJtoptall.TObBa(1)],...
                                              [0], [500], ...
                                              options);
[CrsJtoptall.TbBa fvalsJ16toptall.TbBa] = fminsearchbnd(@(p)fval_fwteff_type_err_17([p(1)/100, ...
                    CrsJtoptall.TObBa(2), 1, 1, 2.3, 1.3, 1, 0, 1.033, 0], parJ, teff_obs, teff_err), ...
                                              [CrsJtoptall.TObBa(1)],...
                                              [0], [500], ...
                                              options);
[CrsJtoptall.ObBa fvalsJ16toptall.ObBa] = fminsearchbnd(@(p)fval_fwteff_type_err_17([p(1)/100, ...
                    2, 1, 1, 2.3, 1.3, 1, 0, 1.033, CrsJtoptall.TObBa(3)], parJ_flatTD, teff_obs, teff_err), ...
                                              [CrsJtoptall.TObBa(1)],...
                                              [0], [500], ...
                                              options);

%% Alternatively, same time and electricity by simply re-loading
%% these previously calculated values.

% load('CrsAm616toptall.mat')
% CrsJtoptall = CrsAm616toptall;
% fvalsJtoptall = fvalsAm616toptall;


%% Workup
%% Optimized TEFF workup
% these produce the basin averages, and maps of the input
% data. Each of these should take a little under an hour to run on
% a 12 node processor (assuming you actually use all the nodes).



%% main

% Temperature + Oxygen + Size + Ballast
[bsnsJtoptall.TObBa, mapJtoptall.TObBa, stuffJtoptall.TObBa, ksJ.TObBa] = calc_fwteff_type_17(CrsJtoptall.TObBa(1)/100, ...
                                                  CrsJtoptall.TObBa(2),...
                                                  1,...
                                                  1,...
                                                  2.3, 1.3,...
                                                  1, ...
                                                  0, 1.033,CrsJtoptall.TObBa(3), ...
                                                  parJ);
% Temperature + Oxygen + Size
[bsnsJtoptall.TOb, mapJtoptall.TOb, stuffJtoptall.TOb] = calc_fwteff_type_17(CrsJtoptall.TOb(1)/100, ...
                                                  CrsJtoptall.TObBa(2),...
                                                  1,...
                                                  1,...
                                                  2.3, 1.3,...
                                                  1, ...
                                                  0, 1.033, CrsJtoptall.TObBa(3), ...
                                                  parJ_flatBal);

[bsnsJtoptall.TOBa, mapJtoptall.TOBa, stuffJtoptall.TOBa] = calc_fwteff_type_17(CrsJtoptall.TOBa(1)/100, ...
                                                  CrsJtoptall.TObBa(2),...
                                                  1,...
                                                  1,...
                                                  2.3, 1.3,...
                                                  0, ...
                                                  0, 1.033,CrsJtoptall.TObBa(3), ...
                                                  parJ);
[bsnsJtoptall.TbBa, mapJtoptall.TbBa, stuffJtoptall.TbBa] = calc_fwteff_type_17(CrsJtoptall.TbBa(1)/100, ...
                                                  CrsJtoptall.TObBa(2),...
                                                  1,...
                                                  1,...
                                                  2.3, 1.3,...
                                                  1, ...
                                                  0, 1.033,0, ...
                                                  parJ);

[bsnsJtoptall.ObBa, mapJtoptall.ObBa, stuffJtoptall.ObBa] = calc_fwteff_type_17(CrsJtoptall.ObBa(1)/100, ...
                                                  2,...
                                                  1,...
                                                  1,...
                                                  2.3, 1.3,...
                                                  1, ...
                                                  0, 1.033,10, ...
                                                  parJ_flatTD);

%% For people who would rather not spend half a day re-running these
% save('.data/Jtoptall.mat', 'bsnsJtoptall', 'mapJtoptall', ...
%      'stuffJtoptall')

% load('data/Jtoptall.mat')


%% Plot bars results
%figure
clf; hold all;
a = bar(teff_obs, 'w');
aa = errorbar(teff_obs, teff_err, '.', 'color', [0.6 0.6 0.6]);
set(aa, 'linewidth', 2)

set(gca, 'XTick', 1:length(regabr), 'XTickLabel', regabr,...
         'FontSize',  24);
e = plot([1:8] - 0.1, bsnsJtoptall.TOBa, 's',  'MarkerFaceColor', [0.2 0.2 1],...
         'MarkerEdgeColor', [0, 0, 0],'MarkerSize', 16);
c = plot([1:8], bsnsJtoptall.ObBa, '^',...
         'MarkerFaceColor', [0, 1, 1], ...
         'MarkerEdgeColor', [0, 0, 0], 'MarkerSize', 16);


d = plot([1:8] - 0.05, bsnsJtoptall.TbBa, 'd', 'MarkerFaceColor', [.5 1 0],...
         'MarkerEdgeColor', [0, 0, 0],'MarkerSize', 16);


f = plot([1:8] - 0.1, bsnsJtoptall.TOb, '>',  'MarkerFaceColor', [.96, .87, .7],...
         'MarkerEdgeColor', [0, 0, 0],'MarkerSize', 16);
b = plot([1:8] + 0.05, bsnsJtoptall.TObBa, ':o', 'MarkerFaceColor', [.95, 0.50, 0.50],...
         'MarkerEdgeColor', [0, 0, 0],'MarkerSize', 14);


ylim([0 Inf]);
legend(...
    [b f d c e],...

    sprintf('All Factors; MSE = %1.2f', fvalsJtoptall.TObBa/4),...
    sprintf('Uniform Ballast; MSE = %1.2f',fvalsJtoptall.TOb/4),...
    sprintf('Uniform Oxygen; MSE = %1.2f',fvalsJtoptall.TbBa/4),...
    sprintf('Uniform Temperature; MSE = %1.2f',fvalsJtoptall.ObBa/4),...
    sprintf('Uniform Size; MSE = %1.2f',fvalsJtoptall.TOBa/4),...
    'Location', 'North')

ylabel('T_{EFF}'); xlabel('Region')

%%
set(gcf, 'units','normalized','outerposition',[0 0 1 1],...
        'Color', [1 1 1])
set(gcf,'PaperUnits','inches','PaperPosition',[0 0 16 8])
export_fig('plots/barleaveoneout', '-eps')
print('plots/barleaveoneout', '-dpng')
print('plots/barleaveoneout', '-dsvg')

%% Recalculate MSE
mse.TObBa = sum(((bsnsJtoptall.TObBa - teff_obs)./teff_err).^2)/8
mse.TOBa = sum(((bsnsJtoptall.TOBa - teff_obs)./teff_err).^2)/8
mse.ObBa = sum(((bsnsJtoptall.ObBa - teff_obs)./teff_err).^2)/8

((bsnsJtoptall.TOBa - teff_obs)./teff_err).^2 - ((bsnsJtoptall.ObBa - teff_obs)./teff_err).^2


%% Global Map of TEFF
%%
figure
jm_pcolor(grid.xt, grid.yt, mapJtoptall.TObBa);
colormap(lowjet)
cah = caxis;
hold all
for i = 1:nreg
    RR = double(R2d_shift==i);
    RR(land_shift==1) = NaN;
    [cc,hh] = m_contour(lon_shift,lat2',RR,[.5 .5],'k','linewidth',1);
end
caxis(cah)
c = colorbar;
c.Label.String = 'T_{EFF}';
c.FontSize = 18;
set(gcf, 'units','normalized','outerposition',[0 0 1 1],...
        'Color', [1 1 1])
set(gcf,'PaperUnits','inches','PaperPosition',[0 0 16 8])
export_fig('plots/teffmap', '-eps')

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Reminerilization Length Scales
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%  A few extras for reminerilization length scale
paJset.null = parJ;
mean_PSD = nanmean(parJ.PSD(:));
paJset.null.PSD = repmat(mean_PSD, size(parJ.PSD));
paJset.null.export.CaCO3 = repmat(nanmean(parJ.export.CaCO3(:)), ...
                                          size(parJ.export ...
                                               .CaCO3));
paJset.null.export.opal = repmat(nanmean(parJ.export.opal(:)), ...
                                          size(parJ.export ...
                                               .opal));
paJset.null.phys.temp = repmat(nanmean(parJ.phys.temp(:)), ...
                                          size(parJ.phys.temp));
paJset.null.phys.pdens = repmat(nanmean(parJ.phys.pdens(:)), ...
                                          size(parJ.phys.pdens));
paJset.null.phys.o2 = repmat(nanmean(parJ.phys.o2(:)), ...
                                          size(parJ.phys.o2));



% Biological temperature effect
paJset.bioT = paJset.null;
paJset.bioT.phys.temp = parJ.phys.temp;
paJset.bioT.mode = 3;

% viscosity effect
paJset.visT = paJset.bioT;
paJset.visT.mode = 2;

% density effect
paJset.denT = paJset.null;
paJset.denT.phys.pdens = parJ.phys.pdens;

% oxygen effect
paJset.O = paJset.null;
paJset.O.phys.o2 = parJ.phys.o2;

% ballast effect
paJset.bal = paJset.null;
paJset.bal.export = parJ.export;

% silicate ballast
paJset.si = paJset.null;
paJset.si.export.opal = parJ.export.opal;

% carbonate export
paJset.car = paJset.null;
paJset.car.export.CaCO3 = parJ.export.CaCO3;

% size effect
paJset.size = paJset.null;
paJset.size.PSD = parJ.PSD;
%% Additional calculations
% [bsnsJo.null, mapJo.null, stuffJ.null, ksJ.null] = calc_fwteff_type_17(CrsJtoptall.TObBa(1)/100, ...
%                                                   2,...
%                                                   1,...
%                                                   1,...
%                                                   2.3, 1.3,...
%                                                   0, ...
%                                                   0, 1.033,0, ...
%                                                   paJset.null);

% calculations
% All
[bsnsJo.all, mapJo.all, stuffJo.all, ksJo.all] = calc_fwteff_type_17(CrsJtoptall.TObBa(1)/100, ...
                                                  2,...
                                                  1,...
                                                  1,...
                                                  2.3, 1.3,...
                                                  1, ...
                                                  0, 1.033,10, ...
                                                  parJ);


% Biological temperature effect
[bsnsJo.bioT, mapJo.bioT, stuffJo.bioT, ksJo.bioT] = calc_fwteff_type_17(CrsJtoptall.TObBa(1)/100, ...
                                                  2,...
                                                  1,...
                                                  1,...
                                                  2.3, 1.3,...
                                                  1, ...
                                                  0, 1.033,10, ...
                                                  paJset.bioT);
% viscosity effect
[bsnsJo.visT, mapJo.visT, stuffJo.visT, ksJo.visT] = calc_fwteff_type_17(CrsJtoptall.TObBa(1)/100, ...
                                                  2,...
                                                  1,...
                                                  1,...
                                                  2.3, 1.3,...
                                                  1, ...
                                                  0, 1.033,10, ...
                                                  paJset.visT);
% density effect
[bsnsJo.denT, mapJo.denT, stuffJo.denT, ksJo.denT] = calc_fwteff_type_17(CrsJtoptall.TObBa(1)/100, ...
                                                  2,...
                                                  1,...
                                                  1,...
                                                  2.3, 1.3,...
                                                  1, ...
                                                  0, 1.033,10, ...
                                                  paJset.denT);
% oxygen effect
[bsnsJo.O, mapJo.O, stuffJo.O, ksJo.O] = calc_fwteff_type_17(CrsJtoptall.TObBa(1)/100, ...
                                                  2,...
                                                  1,...
                                                  1,...
                                                  2.3, 1.3,...
                                                  1, ...
                                                  0, 1.033,10, ...
                                                  paJset.O);
% ballast effect
[bsnsJo.bal, mapJo.bal, stuffJo.bal, ksJo.bal] = calc_fwteff_type_17(CrsJtoptall.TObBa(1)/100, ...
                                                  2,...
                                                  1,...
                                                  1,...
                                                  2.3, 1.3,...
                                                  1, ...
                                                  0, 1.033,10, ...
                                                  paJset.bal);

% Carbonate effect
[bsnsJo.car, mapJo.car, stuffJo.car, ksJo.car] = calc_fwteff_type_17(CrsJtoptall.TObBa(1)/100, ...
                                                  2,...
                                                  1,...
                                                  1,...
                                                  2.3, 1.3,...
                                                  1, ...
                                                  0, 1.033,10, ...
                                                  paJset.car);

% Silicate effect
[bsnsJo.si, mapJo.si, stuffJo.si, ksJo.si] = calc_fwteff_type_17(CrsJtoptall.TObBa(1)/100, ...
                                                  2,...
                                                  1,...
                                                  1,...
                                                  2.3, 1.3,...
                                                  1, ...
                                                  0, 1.033,10, ...
                                                  paJset.si);

% size effect
[bsnsJo.size, mapJo.size, stuffJo.size, ksJo.size] = calc_fwteff_type_17(CrsJtoptall.TObBa(1)/100, ...
                                                  2,...
                                                  1,...
                                                  1,...
                                                  2.3, 1.3,...
                                                  1, ...
                                                  0, 1.033,10, ...
                                                  paJset.size);
clear('ksJo') % ksJo is useful for debugging but a total memory hog

%% Load in a long calculation option (if one doesn't want to run
%% the above scripts)
% save('./data/Jo.mat', 'bsnsJo', 'mapJo', 'stuffJo')
% load('./data/Jo.mat')

%% Reminerilizaton Length Plots
fs = 18;
lw = 1.5;
rlsxlim = [5e0 2e3];
figure
clf
subplot(131)
hold all
plot(nanmean(stuffJo.all.W200,2)./...
     nanmean(stuffJo.all.crz200,2),...
     grid.yt, '-r', 'LineWidth', lw)

xlabel(' Remineralization length scale (m)')
ylabel('Latitude')
title('Remineralization Effects')

plot(nanmean(stuffJo.bioT.W200,2)./...
     nanmean(stuffJo.bioT.crz200,2),...
     grid.yt, '--k', 'LineWidth', lw)
plot(nanmean(stuffJo.O.W200,2)./...
     nanmean(stuffJo.O.crz200,2),...
     grid.yt, ':k', 'LineWidth', lw)
set(gca, 'xscale', 'log', 'FontSize', fs, 'XTick', [10^2 10^3])
xlim(rlsxlim)
legend('all', 'Temperature', 'Oxygen', 'Location', 'Best')

subplot(132)
hold all
plot(nanmean(stuffJo.all.W200,2)./...
     nanmean(stuffJo.all.crz200,2),...
     grid.yt, '-r', 'LineWidth', lw)
xlabel(' Remineralization length scale (m)')
title('Sinking Speed Effects')
%ylabel('Latitude')

plot(nanmean(stuffJo.visT.W200,2)./...
     nanmean(stuffJo.visT.crz200,2),...
     grid.yt, '--k', 'LineWidth', lw)
plot(nanmean(stuffJo.denT.W200,2)./...
     nanmean(stuffJo.denT.crz200,2),...
     grid.yt, ':k', 'LineWidth', lw)
plot(nanmean(stuffJo.size.W200,2)./...
     nanmean(stuffJo.size.crz200,2),...
     grid.yt, '-.b', 'LineWidth', lw)
set(gca, 'xscale', 'log', 'FontSize', fs, 'XTick', [10^2 10^3])
xlim(rlsxlim)
legend('all', 'Viscosity', 'Density','Size', 'Location', 'Best')

subplot(133)
hold all
plot(nanmean(stuffJo.all.W200,2)./...
     nanmean(stuffJo.all.crz200,2),...
     grid.yt, '-r', 'LineWidth', lw)
xlabel('Remineralization length scale (m)')
%ylabel('Latitude')
title('Sinking Speed Effects, Ballast')
plot(nanmean(stuffJo.car.W200,2)./...
     nanmean(stuffJo.car.crz200,2),...
     grid.yt, '--b', 'LineWidth', lw)
plot(nanmean(stuffJo.si.W200,2)./...
     nanmean(stuffJo.si.crz200,2),...
     grid.yt, '-.k', 'LineWidth', lw)
set(gca, 'xscale', 'log', 'FontSize', fs, 'XTick', [10^2 10^3])
xlim(rlsxlim)
legend('all','CaCO3', 'Silicate', 'Location', 'Best')

%%
set(gcf, 'units','normalized','outerposition',[0 0 1 1],...
        'Color', [1 1 1])
set(gcf,'PaperUnits','inches','PaperPosition',[0 0 16 8])
print('plots/reminlengthscenarios200m' , '-dpng')
export_fig('plots/reminlengthscenarios200m' , '-eps')

%% As above but this time at 1000m
fs = 18;
lw = 1.5;
figure
clf
subplot(131)
hold all
plot(nanmean(stuffJo.all.W1000,2)./...
     nanmean(stuffJo.all.crz1000,2),...
     grid.yt, '-r', 'LineWidth', lw)

xlabel(' Remineralization length scale (m)')
ylabel('Latitude')
title('Remineralization Effects')

plot(nanmean(stuffJo.bioT.W1000,2)./...
     nanmean(stuffJo.bioT.crz1000,2),...
     grid.yt, '--k', 'LineWidth', lw)
plot(nanmean(stuffJo.O.W1000,2)./...
     nanmean(stuffJo.O.crz1000,2),...
     grid.yt, ':k', 'LineWidth', lw)
set(gca, 'xscale', 'log', 'FontSize', fs, 'XTick', [10^2 10^3])
xlim([3e1 2e3])
legend('all', 'Temperature', 'Oxygen', 'Location', 'Best')

subplot(132)
hold all
plot(nanmean(stuffJo.all.W1000,2)./...
     nanmean(stuffJo.all.crz1000,2),...
     grid.yt, '-r', 'LineWidth', lw)
xlabel(' Remineralization length scale (m)')
title('Sinking Speed Effects')
%ylabel('Latitude')

plot(nanmean(stuffJo.visT.W1000,2)./...
     nanmean(stuffJo.visT.crz1000,2),...
     grid.yt, '--k', 'LineWidth', lw)
plot(nanmean(stuffJo.denT.W1000,2)./...
     nanmean(stuffJo.denT.crz1000,2),...
     grid.yt, ':k', 'LineWidth', lw)
plot(nanmean(stuffJo.size.W1000,2)./...
     nanmean(stuffJo.size.crz1000,2),...
     grid.yt, '-.b', 'LineWidth', lw)
set(gca, 'xscale', 'log', 'FontSize', fs, 'XTick', [10^2 10^3])
xlim(rlsxlim)
legend('all', 'Viscosity', 'Density','Size', 'Location', 'Best')

subplot(133)
hold all
plot(nanmean(stuffJo.all.W1000,2)./...
     nanmean(stuffJo.all.crz1000,2),...
     grid.yt, '-r', 'LineWidth', lw)
xlabel('Remineralization length scale (m)')
%ylabel('Latitude')
title('Sinking Speed Effects, Ballast')
plot(nanmean(stuffJo.car.W1000,2)./...
     nanmean(stuffJo.car.crz1000,2),...
     grid.yt, '--b', 'LineWidth', lw)
plot(nanmean(stuffJo.si.W1000,2)./...
     nanmean(stuffJo.si.crz1000,2),...
     grid.yt, '-.k', 'LineWidth', lw)
set(gca, 'xscale', 'log', 'FontSize', fs, 'XTick', [10^2 10^3])
xlim(rlsxlim)
legend('all','CaCO3', 'Silicate', 'Location', ...
       'Best')

%%
set(gcf, 'units','normalized','outerposition',[0 0 1 1],...
        'Color', [1 1 1])
set(gcf,'PaperUnits','inches','PaperPosition',[0 0 16 8])
print('plots/reminlengthscenarios1000m' , '-dpng')
export_fig('plots/reminlengthscenarios1000m' , '-eps')


%% As above but this time base of photic zone
%% As above but this time at 000m
fs = 18;
lw = 1.5;
figure
clf
subplot(131)
hold all
plot(nanmean(stuffJo.all.W000,2)./...
     nanmean(stuffJo.all.crz000,2),...
     grid.yt, '-r', 'LineWidth', lw)

xlabel(' Remineralization length scale (m)')
ylabel('Latitude')
title('Remineralization Effects')

plot(nanmean(stuffJo.bioT.W000,2)./...
     nanmean(stuffJo.bioT.crz000,2),...
     grid.yt, '--k', 'LineWidth', lw)
plot(nanmean(stuffJo.O.W000,2)./...
     nanmean(stuffJo.O.crz000,2),...
     grid.yt, ':k', 'LineWidth', lw)
set(gca, 'xscale', 'log', 'FontSize', fs, 'XTick', [10^1 10^2 10^3])
xlim(rlsxlim)
legend('all', 'Temperature', 'Oxygen', 'Location', 'Best')

subplot(132)
hold all
plot(nanmean(stuffJo.all.W000,2)./...
     nanmean(stuffJo.all.crz000,2),...
     grid.yt, '-r', 'LineWidth', lw)
xlabel(' Remineralization length scale (m)')
title('Sinking Speed Effects')
%ylabel('Latitude')

plot(nanmean(stuffJo.visT.W000,2)./...
     nanmean(stuffJo.visT.crz000,2),...
     grid.yt, '--k', 'LineWidth', lw)
plot(nanmean(stuffJo.denT.W000,2)./...
     nanmean(stuffJo.denT.crz000,2),...
     grid.yt, ':k', 'LineWidth', lw)
plot(nanmean(stuffJo.size.W000,2)./...
     nanmean(stuffJo.size.crz000,2),...
     grid.yt, '-.b', 'LineWidth', lw)
set(gca, 'xscale', 'log', 'FontSize', fs, 'XTick', [10^1 10^2 10^3])
xlim(rlsxlim)
legend('all', 'Viscosity', 'Density','Size', 'Location', 'Best')

subplot(133)
hold all
plot(nanmean(stuffJo.all.W000,2)./...
     nanmean(stuffJo.all.crz000,2),...
     grid.yt, '-r', 'LineWidth', lw)
xlabel('Remineralization length scale (m)')
%ylabel('Latitude')
title('Sinking Speed Effects, Ballast')
plot(nanmean(stuffJo.car.W000,2)./...
     nanmean(stuffJo.car.crz000,2),...
     grid.yt, '--b', 'LineWidth', lw)
plot(nanmean(stuffJo.si.W000,2)./...
     nanmean(stuffJo.si.crz000,2),...
     grid.yt, '-.k', 'LineWidth', lw)
set(gca, 'xscale', 'log', 'FontSize', fs, 'XTick', [10^1 10^2 10^3])
xlim(rlsxlim)
legend('all','CaCO3 Ballasting', 'Silicate Ballasting', 'Location', 'Best')

%%
set(gcf, 'units','normalized','outerposition',[0 0 1 1],...
        'Color', [1 1 1])
set(gcf,'PaperUnits','inches','PaperPosition',[0 0 16 8])
print('plots/reminlengthscenarios0m' , '-dpng')
export_fig('plots/reminlengthscenarios0m' , '-eps')
%%
%% As above but this time TEFF
fs = 18;
lw = 1.5;
%figure
clf
subplot(131)
hold all
plot(nanmean(mapJo.all,2),...
     grid.yt, '-r', 'LineWidth', lw)

xlabel('Transfer Efficiency')
ylabel('Latitude')
title('Remineralization Effects')

plot(nanmean(mapJo.bioT,2),...
     grid.yt, '--k', 'LineWidth', lw)
plot(nanmean(mapJo.O,2),...
     grid.yt, ':k', 'LineWidth', lw)
%set(gca, 'xscale', 'log', 'FontSize', fs, 'XTick', [10^1 10^2
%10^3])
set(gca, 'xscale', 'log', 'FontSize', fs)
xlim([5e-2 1])
legend('all', 'Temperature', 'Oxygen', 'Location', 'Best')

subplot(132)
hold all
plot(nanmean(mapJo.all,2),...
     grid.yt, '-r', 'LineWidth', lw)
xlabel('Transfer Efficiency')
title('Sinking Speed Effects')
%ylabel('Latitude')

plot(nanmean(mapJo.visT,2),...
     grid.yt, '--k', 'LineWidth', lw)
plot(nanmean(mapJo.denT,2),...
     grid.yt, ':k', 'LineWidth', lw)
plot(nanmean(mapJo.size,2),...
     grid.yt, '-.b', 'LineWidth', lw)
%set(gca, 'xscale', 'log', 'FontSize', fs, 'XTick', [10^1 10^2
%10^3])
set(gca, 'xscale', 'log', 'FontSize', fs)
xlim([5e-2 1])
legend('all', 'Viscosity', 'Density','Size', 'Location', 'Best')

subplot(133)
hold all
plot(nanmean(mapJo.all,2),...
     grid.yt, '-r', 'LineWidth', lw)
xlabel('Transfer Efficiency')
%ylabel('Latitude')
title('Sinking Speed Effects, Ballast')
plot(nanmean(mapJo.car,2),...
     grid.yt, '--b', 'LineWidth', lw)
plot(nanmean(mapJo.si,2),...
     grid.yt, '-.k', 'LineWidth', lw)
set(gca, 'xscale', 'log', 'FontSize', fs)
xlim([5e-2 1])
%xlim([5e0 2e3])
legend('all','CaCO3', 'Silicate', 'Location', 'Best')

%%
set(gcf, 'units','normalized','outerposition',[0 0 1 1],...
        'Color', [1 1 1])
set(gcf,'PaperUnits','inches','PaperPosition',[0 0 16 8])
print('plots/tefflatscenarios' , '-dpng')
export_fig('plots/tefflatscenarios' , '-eps')


%% Reminerilizaton length scale at 200m map
figure; jm_pcolor(grid.xt, grid.yt, log10(stuffJo.all.W200./ ...
                  stuffJo.all.crz200));
%title('log10 reminerilization length', 'FontSize', 20)

hold all
for i = 1:nreg
    RR = double(R2d_shift==i);
    RR(land_shift==1) = NaN;
    [cc,hh] = m_contour(lon_shift,lat2',RR,[.5 .5],'k','linewidth',1);
end

c = colorbar;
c.Label.String = 'RLS at 200m (m)';
c.FontSize = 18;

set(gcf, 'units','normalized','outerposition',[0 0 1 1],...
        'Color', [1 1 1])
set(gcf,'PaperUnits','inches','PaperPosition',[0 0 16 8])
%%
print('plots/reminlengthmap' , '-dpng')
export_fig('plots/reminlengthmap' , '-eps')

%% Additional Reminerilization length scale calculations. What
%% percent of difference do these differences signify?
% 1. Calculate the difference in RLS between high latitudes (above
% 60 degrees) and
% Subtropics (20-30 degrees), for each model.
% 2. Devide by the all factors comparason.

% All parameters difference
rlslat.all = nanmean(stuffJo.all.W200,2)./...
     nanmean(stuffJo.all.crz200,2)
rlshilat.all = nanmean(rlslat.all(find(grid.yt >= 60 | grid.yt <= ...
                                       -60)))
rlsst.all = nanmean(rlslat.all(find((grid.yt >= 20 & grid.yt <= 40)|...
                                    grid.yt >= -40 & grid.yt <= - ...
                                    20)));
rlslolat.all = nanmean(rlslat.all(find( grid.yt <= 40& grid.yt >= ...
                                        -40)))
rlstrop.all = nanmean(rlslat.all(find( grid.yt <= 10 & grid.yt >= ...
                                        -10)))

rlslat.bioT = nanmean(stuffJo.bioT.W200,2)./...
     nanmean(stuffJo.bioT.crz200,2)
rlshilat.bioT = nanmean(rlslat.bioT(find(grid.yt >= 60 | grid.yt <= ...
                                       -60)))
rlsst.bioT = nanmean(rlslat.bioT(find((grid.yt >= 20 & grid.yt <= 40)|...
                                    grid.yt >= -40 & grid.yt <= - ...
                                    20)));
rlslolat.bioT = nanmean(rlslat.bioT(find( grid.yt <= 40& grid.yt >= ...
                                        -40)))
rlstrop.bioT = nanmean(rlslat.bioT(find( grid.yt <= 10 & grid.yt >= ...
                                        -10)))

rlslat.size = nanmean(stuffJo.size.W200,2)./...
     nanmean(stuffJo.size.crz200,2)
rlshilat.size = nanmean(rlslat.size(find(grid.yt >= 60 | grid.yt <= ...
                                       -60)))
rlsst.size = nanmean(rlslat.size(find((grid.yt >= 20 & grid.yt <= 40)|...
                                    grid.yt >= -40 & grid.yt <= - ...
                                    20)));
rlslolat.size = nanmean(rlslat.size(find( grid.yt <= 40& grid.yt >= ...
                                        -40)))
rlstrop.size = nanmean(rlslat.size(find( grid.yt <= 10 & grid.yt >= ...
                                        -10)))

rlshilat.size/rlslolat.size
rlshilat.bioT/rlslolat.bioT

% size effects on differences between high and low latitudes
(rlshilat.size/rlslolat.size)/(rlshilat.all/rlslolat.all)
% size effects on differences between
(rlstrop.size/rlsst.size)/(rlstrop.all/rlsst.all)


% bioT effects on differences between high and low latitudes
(rlshilat.bioT/rlslolat.bioT)/(rlshilat.all/rlslolat.all)
% bioT effects on differences between
(rlstrop.bioT/rlsst.bioT)/(rlstrop.all/rlsst.all)


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Input data Plots
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Ballast and Chl, + T + O
%% For this to run, it will have to go after the initial
%% calculations, because it includes density estemates, which are calculated

latav.si = nanmean(parJ.export.opal,2);
latav.car = nanmean(parJ.export.CaCO3,2);
%latav.chl = nanmean(nanmean(chl_clim, 3), 2);

loc_tprof = squeeze(parJ.phys.temp(62,90,:));

t200 = nan(size(parJ.phys.temp, 1), size(parJ.phys.temp, 2));
for i = 1:size(parJ.phys.temp, 1)
    for j = 1:size(parJ.phys.temp, 2)
        loc_tprof = squeeze(parJ.phys.temp(i,j,:));
        if(isnan(loc_tprof(1:7)) == 0)
            t200_loc = interp1(grid.zt(1:7), loc_tprof(1:7), 200, 'spline');
            t200(i,j) = t200_loc;
        end
    end
end

o200 = nan(size(parJ.phys.o2, 1), size(parJ.phys.o2, 2));
for i = 1:size(parJ.phys.o2, 1)
    for j = 1:size(parJ.phys.o2, 2)
        loc_oprof = squeeze(parJ.phys.o2(i,j,:));
        if(isnan(loc_oprof(1:7)) == 0)
            o200_loc = interp1(grid.zt(1:7), loc_oprof(1:7), 200, 'spline');
            o200(i,j) = o200_loc;
        end
    end
end


latav.t200 = nanmean(t200, 2);
latav.o200 = nanmean(o200, 2);
latav.rho_part = nanmean(stuffJtoptall.TObBa.rho_part, 2); 
latav.rho_part_car = nanmean(stuffJo.car.rho_part, 2); 
latav.rho_part_si = nanmean(stuffJo.si.rho_part, 2); 

%%
figure
% The figure has to be sized before the plot, otherwise the axis
% labels end up in the wrong place for some reason

set(gcf, 'units','normalized','outerposition',[0 0 1 1],...
        'Color', [1 1 1], 'PaperUnits','inches','PaperPosition',[0 0 16 8])
clf;
subplot(122)
 hold all
a = plot(latav.si, grid.yt, '--b', 'LineWidth', lw);
b = plot(latav.car, grid.yt, '-.r', 'LineWidth', lw);
ylim([-80, 65])
xlabel('Ballast:C Molar Ratio')
ylabel('Latitude')
ax1 = gca;
ax2 = axes('Position', ax1.Position,...
           'XAxisLocation','top',...
           'YTickLabelMode', 'manual', 'YTickLabel', [],...
           'YTickMode', 'manual', 'YTick', [], ...
           'Color','none', 'xscale', 'log');
hold all;
f = plot(latav.rho_part, grid.yt, '-k', 'LineWidth', lw);
% g = plot(latav.rho_part_car, grid.yt, '-b', 'LineWidth', lw);
% h = plot(latav.rho_part_si, grid.yt, ':b', 'LineWidth', lw);
% c = plot(latav.chl, grid.yt, '--g', 'LineWidth', lw);
ylim([-80, 65])
% xlim([.05,2.1])
xlabel('Particle Density')

legend([a;b;f],...
       'Si:C', 'CaCO3:C', 'Particle Density', ...
       'Location', 'East')



subplot(121)
hold all
d = plot(latav.t200, grid.yt, '-r', 'LineWidth', lw);
xlabel('Temperature (\circC)')
ylabel('Latitude')
ax3 = gca;
ax4 = axes('Position', ax3.Position,...
           'XAxisLocation','top',...
           'YTickLabelMode', 'manual', 'YTickLabel', [],...
           'YTickMode', 'manual', 'YTick', [], ...
           'Color','none', 'xscale', 'linear');
hold all
e = plot(latav.o200, grid.yt, '-.k', 'LineWidth', lw);
xlabel('Oxygen (\mum)')
legend([d;e], ...
       'Temperature', 'Oxygen',...
       'Location', 'Best')
fs2 = 14
set(ax1, 'FontSize', fs2)
set(ax2, 'FontSize', fs2)
set(ax3, 'FontSize', fs2)
set(ax4, 'FontSize', fs2)

%%

print('plots/ballastchl' , '-dpng')
print('plots/ballastchl' , '-dsvg')
export_fig('plots/ballastchl' , '-eps')

%% Particle Size distribution
% psd vs nearist month chlorphyl climatology
load('/jetwork2/cram/UVP/mcchl21Sep2016', 'mcchlC', 'surf_beta', ...
     'eg', 'pred', 'smodb', 'beta4', 'betaci4', 'xchl')

findx = find(~(isnan(log10(mcchlC)) | isnan(surf_beta')));

[mod4 smod4] = polyfit(log10(mcchlC(findx)), surf_beta(findx)', 1);

[mod4b smod4b] = fit(log10(mcchlC(findx)), surf_beta(findx)', ...
                     'poly1'); 
xchl = [-1.4:0.2:0.4];

[beta4 betaci4] = polyconf(mod4, xchl, smod4, 'predopt', 'curve');
[beta4 betapred4] = polyconf(mod4, xchl, smod4, 'predopt', ...
                             'observation');

%% binned averages
logchl = log10(mcchlC(findx));
sb2 = surf_beta(findx)';
logchlbins = [-1.4:0.2:0.4];
logchledges = [-1.5:0.2:0.5];
[counts, idx] = histc(logchl, logchledges);

table(logchl, idx);
meansb2 = accumarray(idx, sb2, [], @mean);
table(logchlbins', [meansb2]);
mzidx = find(meansb2 == 0);
meansb2(mzidx) = NaN;


%%


f4 = figure;
clf; hold all;
e = plot(logchlbins, -meansb2, 'sk', 'LineWidth', 1.5,...
         'MarkerSize', 12, 'MarkerFaceColor', 'blue');
a = plot(log10(mcchlC), -surf_beta', 'ok', 'LineWidth', 1.5,...
         'MarkerSize', 10);

b = plot(xchl, -beta4, '-r', 'LineWidth', 2);
c = plot(xchl, -beta4-betaci4, '--r', 'LineWidth', 1.5);
plot(xchl, -beta4+betaci4, '--r', 'LineWidth', 1.5)
% plot(xchl, beta4+betapred4, ':r')
% plot(xchl, beta4-betapred4, ':r')
legend([a e b c], ...
       'Observed',...
       'Binned'
       'Best Fit',...
       '95% Confidence Interval')
xlabel('log10 (Chl) mg/m^3'); ylabel('\beta');
set(gca, 'fontsize', 24)
set(gcf, 'units','normalized','outerposition',[0 0 1 1],...
        'Color', [1 1 1])
set(gcf,'PaperUnits','inches','PaperPosition',[0 0 10 10])

%%
print('plots/betareg' , '-dpng', '-r300')
export_fig('plots/betareg' , '-eps')

%% and the psd image
f14 = figure(14);
set(f14, 'units','normalized','outerposition',[0 0 1 1],...
        'Color', [1 1 1])
jm_pcolor(grid.xt, grid.yt, parAm6.PSD);
caxis([2.3 4.5])
h = colorbar;
set(h, 'fontsize', 14);
h.Label.String = '\beta';
h.FontSize = 18;
set(gcf,'PaperUnits','inches','PaperPosition',[0 0 16 8])
hold all
for i = 1:nreg
    RR = double(R2d_shift==i);
    RR(land_shift==1) = NaN;
    [cc,hh] = m_contour(lon_shift,lat2',RR,[.5 .5],'k','linewidth',1);
end
print('plots/betamap' , '-dpng')
export_fig('plots/betamap' , '-eps')


%% Confirm that seasonally weighting transfer efficinecy gives the
%% same values as seasonally weighting the inputs to the transfer
%% efficiency calculation
parJ.component.rho_std = 1.23;
parS6 = parAm6;
parS6 = parJ;
parS6.PSD = mcchl_psd;
parS6.phys.temp = woaRe3.temp_mon;
parS6.phys.pdens = pdens4d;
parS6.phys.o2 = woaRe3.o2_mon;
parS6.EX = frankex_clim;
parS6.maxdeep = z(end);
parS6.teff_depth = 1000;

lcr = CrsJtoptall.TObBa;

% testseas_bak = testseas;
% testseas_bak1 = testseas;
% testseas = testseas_bak;
% testseas = testseas_bak1;

[testseas.bsns, testseas.map, testseas.stuff] = calc_fwteff_type_seas_17(lcr(1)/100, ...
                                                  lcr(2),1, 1, ...
                                                  2.3,1, 0, ...
                                                      1.033, lcr(3), ...
                                                  parS6)

%% Lets explore what happens when we vary gamma, the sinking speed
%% exponent constant

% I am going to change calc_fwteff_type_17 so
% that it takes alpha and gamma as inputs, and doesn't define gamma
% as alpha -1. Then I'm going to modify the one upstream of that to
% account for an extra input. And I'm going to modify all of
% juramia to account for this difference everywhere.

% I'll also explore a flux profile where I just vary gamma.
% And maybe one where I vary alpha.

%  Vary Gamma
gamma_abr = {'g10', 'g13', 'g16'};
gamma_vec = [1.0, 1.3, 1.6];



for i = 1:length(gamma_vec)
    prof.gamma.(gamma_abr{i}) = pvsto_limw3(alpha, stdPSD, gamma_vec(i), Cr4, ...
                                        Q10, KmOxy, xmm, mxmm, wxmm, 4, ds, dl, ...
                                        rho_std, rho_std, rho_std, ...
                                        null_dens/1, rho_water, ...
                                        1, null_temp, null_oxy, z, 1000);
end

%  Vary Alpha
alpha_abr = {'a10', 'a13', 'a16'};
alpha_vec = [2.0, 2.3, 2.6];



for i = 1:length(alpha_vec)
    prof.alpha.(alpha_abr{i}) = pvsto_limw3(alpha_vec(i), stdPSD, alpha_vec(i) -1 , Cr4, ...
                                        Q10, KmOxy, xmm, mxmm, wxmm, 4, ds, dl, ...
                                        rho_std, rho_std, rho_std, ...
                                        null_dens/1, rho_water, ...
                                        1, null_temp, null_oxy, z, 1000);
end


%% Plot

%Plot Gamma
%strs.gamma = num2str(gamma_vec);
strs.gamma = {'1.0', '1.3', '1.6'}
lt.gamma = ['base', strcat({'\gamma = '} , strs.gamma)];

% gamma
figure;
subplot(121)
hold all;
set(gca, 'Ydir', 'reverse','FontSize', fs)
ylim([0 1000]); xlim([0 1])
xlabel('Normalized Flux');
%ylabel('Depth')
plot(prof.base.FZ, z, 'Color', [0.8 0.8 0.8], 'LineWidth', 3)
for i = 1:3
    plot(prof.gamma.(gamma_abr{i}).FZ, z, 'Color', 'k', 'linestyle', los_linS{i})
end
legend(lt.gamma, 'Location', 'NorthWest')
set(gca, 'xscale', 'log'); xlim([0.1, 1]);
title('Gamma')

% Plot Alpha
%strs.gamma = num2str(gamma_vec);
strs.alpha = {'2.0', '2.3', '2.6'}
lt.alpha = ['base', strcat({'\alpha = '} , strs.alpha)];

% gamma
subplot(122)
hold all;
set(gca, 'Ydir', 'reverse','FontSize', fs)
ylim([0 1000]); xlim([0 1])
xlabel('Normalized Flux');
%ylabel('Depth')
plot(prof.base.FZ, z, 'Color', [0.8 0.8 0.8], 'LineWidth', 3)
for i = 1:3
    plot(prof.alpha.(alpha_abr{i}).FZ, z, 'Color', 'k', 'linestyle', los_linS{i})
end
legend(lt.alpha, 'Location', 'NorthWest')
set(gca, 'xscale', 'log'); xlim([0.1, 1]);
title('Alpha')

%% Optimize given alpha and gamma.
alpha_loc = 2.3;
gamma_loc = alpha_loc - 1;
[CrsJtoptall.AGbase fvalsJ16toptall.AGbase] = fminsearchbnd(@(p)fval_fwteff_type_err_17([p(1)/100, ...
                    CrsJtoptall.TObBa(2), 1, 1, alpha_loc, gamma_loc, 1, 0, 1.033, CrsJtoptall.TObBa(3)], parJ, teff_obs, teff_err), ...
                                              [CrsJtoptall.TObBa(1)],...
                                              [0], [500], ...
                                              options1);
alpha_loc = 2.0;
gamma_loc = alpha_loc - 1;
[CrsJtoptall.A.x20 fvalsJ16toptall.A.x20] = fminsearchbnd(@(p)fval_fwteff_type_err_17([p(1)/100, ...
                    CrsJtoptall.TObBa(2), 1, 1, alpha_loc, gamma_loc, 1, 0, 1.033, CrsJtoptall.TObBa(3)], parJ, teff_obs, teff_err), ...
                                              [CrsJtoptall.TObBa(1)],...
                                              [0], [500], ...
                                              options1);

alpha_loc = 2.6;
gamma_loc = alpha_loc - 1;
[CrsJtoptall.A.x26 fvalsJ16toptall.A.x26] = fminsearchbnd(@(p)fval_fwteff_type_err_17([p(1)/100, ...
                    CrsJtoptall.TObBa(2), 1, 1, alpha_loc, gamma_loc, 1, 0, 1.033, CrsJtoptall.TObBa(3)], parJ, teff_obs, teff_err), ...
                                              [CrsJtoptall.TObBa(1)],...
                                              [0], [500], ...
                                              options1);

alpha_loc = 2.3;
gamma_loc = 1.0;
[CrsJtoptall.G.x10 fvalsJ16toptall.G.x10] = fminsearchbnd(@(p)fval_fwteff_type_err_17([p(1)/100, ...
                    CrsJtoptall.TObBa(2), 1, 1, alpha_loc, gamma_loc, 1, 0, 1.033, CrsJtoptall.TObBa(3)], parJ, teff_obs, teff_err), ...
                                              [CrsJtoptall.TObBa(1)],...
                                              [0], [500], ...
                                              options1);

alpha_loc = 2.3;
gamma_loc = 1.6;
[CrsJtoptall.G.x16 fvalsJ16toptall.G.x16] = fminsearchbnd(@(p)fval_fwteff_type_err_17([p(1)/100, ...
                    CrsJtoptall.TObBa(2), 1, 1, alpha_loc, gamma_loc, 1, 0, 1.033, CrsJtoptall.TObBa(3)], parJ, teff_obs, teff_err), ...
                                              [CrsJtoptall.TObBa(1)],...
                                              [0], [500], ...
                                              options1);


[CrsJtoptall.Aopt fvalsJ16toptall.Aopt] = fminsearchbnd(@(p)fval_fwteff_type_err_17([p(1)/100, ...
                    CrsJtoptall.TObBa(2), 1, 1, p(2), p(2) - 1, 1, 0, 1.033, CrsJtoptall.TObBa(3)], parJ, teff_obs, teff_err), ...
                                              [CrsJtoptall.TObBa(1), 2.0],...
                                              [0, 1.0], [500, 3.0], ...
                                              options1);

[CrsJtoptall.Gopt fvalsJ16toptall.Gopt] = fminsearchbnd(@(p)fval_fwteff_type_err_17([p(1)/100, ...
                    CrsJtoptall.TObBa(2), 1, 1, 2.3, p(2), 1, 0, 1.033, CrsJtoptall.TObBa(3)], parJ, teff_obs, teff_err), ...
                                              [CrsJtoptall.TObBa(1), 1.3],...
                                              [0, 0], [500, 2.3], ...
                                              options1);

[CrsJtoptall.AGopt0 fvalsJ16toptall.AGopt0] = fminsearchbnd(@(p)fval_fwteff_type_err_17([p(1)/100, ...
                    CrsJtoptall.TObBa(2), 1, 1, p(2), p(3), 1, 0, 1.033, CrsJtoptall.TObBa(3)], parJ, teff_obs, teff_err), ...
                                              [CrsJtoptall.TObBa(1), 2.0, 1.0],...
                                              [0, 1.0, 0.0], [500, 3.0, 2.0], ...
                                              options1);

[CrsJtoptall.AGopt1 fvalsJ16toptall.AGopt1] = fminsearchbnd(@(p)fval_fwteff_type_err_17([p(1)/100, ...
                    p(2), 1, 1, 2.3, p(3), 1, 0, 1.033, p(4)], parJ, teff_obs, teff_err), ...
                                              [1.9, 2.0, 0.4, 10],...
                                              [0, 1.0, 0.0, 0], [500, 3.0, 2.3, 100], ...
                                              options1);
%%

%% Save over, or reload the size and shape optimized runs
% save( 'data/AGRuns.mat', 'CrsJtoptall', 'fvalsJ16toptall')w
% load( '/data/AGRuns.mat')

%% Calculate RLS for a 1000um particle
% Here, assuming a 2m/d sinking speed.
W1000 = 2 * (1000/20)^1.3
W1000./(CrsJtoptall.TObBa(1)/100)
% 239m

% Devries, gamma = 1.7
W1000_tdv = 2.2e5*(1000*10e-6) ^ 1.7
W1000_tdv/365

% Here, with optimized gamma
W1000_g = 2*(1000/20) ^ CrsJtoptall.TObBaG(3) % 9.4m/d
W1000_g/(CrsJtoptall.TObBaG(1)/100) % 578m
%% Taylor diagrams
% Get R and sigma_m/sigma_r for each scenario
%% Adjusted taylor diagram arguments

namesJTO = fieldnames(bsnsJtoptall);
namesJTO = {'TObBa'; 'TOb'; 'TbBa'; 'ObBa'; 'TOBa'};
nJ = length(namesJTO);

sdev = nan(nJ+1,1);
crmsd = nan(nJ+1,1);
ccoef = nan(nJ+1,1);
sdev_a = nan(nJ+1,1);
crmsd_a = nan(nJ+1,1);
ccoef_a = nan(nJ+1,1);
label = ['observed' ; namesJTO];

obs_sda = adjusted_sd(teff_obs, teff_err);
obs_sd = regular_sd(teff_obs, teff_err);
obs_Ra = adjusted_R(teff_obs, teff_obs, teff_err);
obs_R = regular_R(teff_obs, teff_obs, teff_err);
obs_RMSDa = adjusted_RMSD(teff_obs, teff_obs, teff_err);
obs_RMSD = regular_RMSD(teff_obs, teff_obs, teff_err);

sdev(1) = obs_sd;
crmsd(1) = obs_RMSD;
ccoef(1) = obs_R;

sdev_a(1) = obs_sda;
crmsd_a(1) = obs_RMSDa;
ccoef_a(1) = obs_Ra;

for i = 1:nJ
    mod_tstr = bsnsJtoptall.(namesJTO{i});
    loc_sda = adjusted_sd(mod_tstr, teff_err);
    loc_Ra = adjusted_R(mod_tstr, teff_obs, teff_err);
    loc_RMSDa = adjusted_RMSD(mod_tstr, teff_obs, teff_err);
    loc_RMSDa2 = loc_RMSDa^2;
    loc_checka = loc_sda^2 + obs_sda^2 - 2*(loc_sda * obs_sda * ...
                                           loc_Ra);
    loc_descrepa = loc_RMSDa2 - loc_checka;
    
    loc_sd = regular_sd(mod_tstr, teff_err);
    loc_R = regular_R(mod_tstr, teff_obs, teff_err);
    loc_RMSD = regular_RMSD(mod_tstr, teff_obs, teff_err);
    
    
    loc_RMSD2 = loc_RMSDa^2;
    loc_check = loc_sd^2 + obs_sd^2 - 2*(loc_sd * obs_sd * ...
                                           loc_R);
    loc_descrep = loc_RMSD - loc_check;
    
    sdev(i+1) = loc_sd;
    crmsd(i+1) = loc_RMSD;
    ccoef(i+1) = loc_R;
    
    sdev_a(i+1) = loc_sda;
    crmsd_a(i+1) = loc_RMSDa;
    ccoef_a(i+1) = loc_Ra;
end

%% Make taylor diagram

figure;
set(gcf, 'Position', [ 979    30   779   811], 'color', 'white')
clf;
subplot(211)
labels = {'Observations', 'All Factors' ,'Uniform Ballast', 'Uniform Oxygen',...
          'Uniform Temperature', 'Uniform Size'}
[hp, ht, axl] = taylor_diagram(sdev,crmsd,ccoef, 'markerLabel', ...
                               labels, 'markerLegend', 'on',...
                               'colOBS','r','markerobs','s', ...
                               'styleOBS', '-',...
                               'titleOBS','observation',...
                               'colRMS','m', 'styleRMS', ':', ...
                               'widthRMS', 2.0,'tickRMS',...
                               0:0.025: 0.1);

title('A Standard', 'fontsize', 18)



subplot(212)

hold on
[hp, ht, axl] = taylor_diagram(sdev_a,crmsd_a,ccoef_a, 'markerLabel', ...
                               labels, 'markerLegend', 'on',...
                               'colOBS','r','markerobs','s', ...
                               'titleOBS','observation',...
                               'styleOBS','-', 'colOBS','r',...
                               'colRMS','m', 'styleRMS', ':', ...
                               'widthRMS', 2.0, 'tickRMS', 0:0.5: ...
                               2);
title('B Error Weighted', 'fontsize', 18)

export_fig('plots/taylor' , '-eps')

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%% Model global TEFF, *while optimizing gamma* %%
%%%% Which is the sinking speed exponent constant
%%%% (Effecct of shape on sinking speed)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Calculate free parameters for models that consider particle sshape

CrsJtoptall.TObBaG = CrsJtoptall.AGopt1;
fvalsJ16toptall.TObBaG = fvalsJ16toptall.AGopt1;

[CrsJtoptall.TObG fvalsJ16toptall.TObG] = fminsearchbnd(@(p)fval_fwteff_type_err_17([p(1)/100, ...
                    CrsJtoptall.TObBaG(2), 1, 1, 2.3, CrsJtoptall.TObBaG(3), 1, 0, 1.033, CrsJtoptall.TObBaG(4)], parJ_flatBal, teff_obs, teff_err), ...
                                                  [CrsJtoptall.TObBaG(1)],...
                                                  [0], [500], ...
                                                  options);

[CrsJtoptall.TOBaG fvalsJ16toptall.TOBaG] = fminsearchbnd(@(p)fval_fwteff_type_err_17([p(1)/100, ...
                    CrsJtoptall.TObBaG(2), 1, 1, 2.3, CrsJtoptall.TObBaG(3), 0, 0, 1.033, CrsJtoptall.TObBaG(4)], parJ, teff_obs, teff_err), ...
                                                  [CrsJtoptall.TObBaG(1)],...
                                                  [0], [500], ...
                                                  options);
[CrsJtoptall.TbBaG fvalsJ16toptall.TbBaG] = fminsearchbnd(@(p)fval_fwteff_type_err_17([p(1)/100, ...
                    CrsJtoptall.TObBaG(2), 1, 1, 2.3, CrsJtoptall.TObBaG(3), 1, 0, 1.033, 0], parJ, teff_obs, teff_err), ...
                                                  [CrsJtoptall.TObBaG(1)],...
                                                  [0], [500], ...
                                                  options);
[CrsJtoptall.ObBaG fvalsJ16toptall.ObBaG] = fminsearchbnd(@(p)fval_fwteff_type_err_17([p(1)/100, ...
                    CrsJtoptall.TObBaG(2), 1, 1, 2.3, CrsJtoptall.TObBaG(3), 1, 0, 1.033, CrsJtoptall.TObBaG(4)], parJ_flatTD, teff_obs, teff_err), ...
                                                  [CrsJtoptall.TObBaG(1)],...
                                                  [0], [500], ...
                                                  options);
%% Reload data option
% %%
% save('data/GRuns.mat', 'CrsJtoptall', 'fvalsJ16toptall')
% %%
% load('data/GRuns.mat')

%%

%% Calculate basin averages and maps of models considering
%% Temperature + Oxygen + Size + Ballast

[bsnsJtoptall.TObBaG, mapJtoptall.TObBaG, stuffJtoptall.TObBaG, ksJ.TObBaG] = calc_fwteff_type_17(CrsJtoptall.TObBaG(1)/100, ...
                                                  CrsJtoptall.TObBaG(2),...
                                                  1,...
                                                  1,...
                                                  2.3, CrsJtoptall.TObBaG(3),...
                                                  1, ...
                                                  0, 1.033,CrsJtoptall.TObBaG(4), ...
                                                  parJ);
% Temperature + Oxygen + Size
[bsnsJtoptall.TObG, mapJtoptall.TObG, stuffJtoptall.TObG] = calc_fwteff_type_17(CrsJtoptall.TObG(1)/100, ...
                                                  CrsJtoptall.TObBaG(2),...
                                                  1,...
                                                  1,...
                                                  2.3, CrsJtoptall.TObBaG(3),...
                                                  1, ...
                                                  0, 1.033, CrsJtoptall.TObBaG(4), ...
                                                  parJ_flatBal);

[bsnsJtoptall.TOBaG, mapJtoptall.TOBaG, stuffJtoptall.TOBaG] = calc_fwteff_type_17(CrsJtoptall.TOBaG(1)/100, ...
                                                  CrsJtoptall.TObBaG(2),...
                                                  1,...
                                                  1,...
                                                  2.3, CrsJtoptall.TObBaG(3),...
                                                  0, ...
                                                  0, 1.033,CrsJtoptall.TObBaG(4), ...
                                                  parJ);
[bsnsJtoptall.TbBaG, mapJtoptall.TbBaG, stuffJtoptall.TbBaG] = calc_fwteff_type_17(CrsJtoptall.TbBaG(1)/100, ...
                                                  CrsJtoptall.TObBaG(2),...
                                                  1,...
                                                  1,...
                                                  2.3, CrsJtoptall.TObBaG(3),...
                                                  1, ...
                                                  0, 1.033,0, ...
                                                  parJ);

[bsnsJtoptall.ObBaG, mapJtoptall.ObBaG, stuffJtoptall.ObBaG] = calc_fwteff_type_17(CrsJtoptall.ObBaG(1)/100, ...
                                                  2,...
                                                  1,...
                                                  1,...
                                                  2.3, CrsJtoptall.TObBaG(3),...
                                                  1, ...
                                                  0, 1.033,CrsJtoptall.TObBaG(4), ...
                                                  parJ_flatTD);

%%
save('data/bsnsJtoptall_withGamma.mat', 'bsnsJtoptall', ...
     'mapJtoptall', 'stuffJtoptall')
load('data/bsnsJtoptall_withGamma.mat')


%% Plot basin wide bar chart results
figure
clf; hold all;
a = bar(teff_obs, 'w');
aa = errorbar(teff_obs, teff_err, '.', 'color', [0.6 0.6 0.6]);
set(aa, 'linewidth', 2)

set(gca, 'XTick', 1:length(regabr), 'XTickLabel', regabr,...
         'FontSize',  24);
e = plot([1:8] - 0.1, bsnsJtoptall.TOBaG, 's',  'MarkerFaceColor', [0.2 0.2 1],...
         'MarkerEdgeColor', [0, 0, 0],'MarkerSize', 16);
c = plot([1:8], bsnsJtoptall.ObBaG, '^',...
         'MarkerFaceColor', [0, 1, 1], ...
         'MarkerEdgeColor', [0, 0, 0], 'MarkerSize', 16);


d = plot([1:8] - 0.05, bsnsJtoptall.TbBaG, 'd', 'MarkerFaceColor', [.5 1 0],...
         'MarkerEdgeColor', [0, 0, 0],'MarkerSize', 16);


f = plot([1:8] - 0.1, bsnsJtoptall.TObG, '>',  'MarkerFaceColor', [.96, .87, .7],...
         'MarkerEdgeColor', [0, 0, 0],'MarkerSize', 16);
b = plot([1:8] + 0.05, bsnsJtoptall.TObBaG, ':o', 'MarkerFaceColor', [.95, 0.50, 0.50],...
         'MarkerEdgeColor', [0, 0, 0],'MarkerSize', 14);


ylim([0 Inf]);
legend(...
    [b f d c e],...

    sprintf('All Factors; MSE = %1.2f', fvalsJ16toptall.TObBaG/4),...
    sprintf('Uniform Ballast; MSE = %1.2f',fvalsJ16toptall.TObG/4),...
    sprintf('Uniform Oxygen; MSE = %1.2f',fvalsJ16toptall.TbBaG/4),...
    sprintf('Uniform Temperature; MSE = %1.2f',fvalsJ16toptall.ObBaG/4),...
    sprintf('Uniform Size; MSE = %1.2f',fvalsJ16toptall.TOBaG/4),...
    'Location', 'North')

ylabel('T_{EFF}')

%%
set(gcf, 'units','normalized','outerposition',[0 0 1 1],...
         'Color', [1 1 1])
set(gcf,'PaperUnits','inches','PaperPosition',[0 0 16 8])
export_fig('plots/barleaveoneout_gamma', '-eps')
print('plots/barleaveoneout_gamma', '-dpng')

%% Taylor diagrams (with optimized size exponent gamma
%% Adjusted taylor diagram arguments

namesJTO = fieldnames(bsnsJtoptall);
namesJTO = {'TObBaG'; 'TObG'; 'TbBaG'; 'ObBaG'; 'TOBaG'};
nJ = length(namesJTO);

sdev = nan(nJ+1,1);
crmsd = nan(nJ+1,1);
ccoef = nan(nJ+1,1);
sdev_a = nan(nJ+1,1);
crmsd_a = nan(nJ+1,1);
ccoef_a = nan(nJ+1,1);
label = ['observed' ; namesJTO];

obs_sda = adjusted_sd(teff_obs, teff_err);
obs_sd = regular_sd(teff_obs, teff_err);
obs_Ra = adjusted_R(teff_obs, teff_obs, teff_err);
obs_R = regular_R(teff_obs, teff_obs, teff_err);
obs_RMSDa = adjusted_RMSD(teff_obs, teff_obs, teff_err);
obs_RMSD = regular_RMSD(teff_obs, teff_obs, teff_err);

sdev(1) = obs_sd;
crmsd(1) = obs_RMSD;
ccoef(1) = obs_R;

sdev_a(1) = obs_sda;
crmsd_a(1) = obs_RMSDa;
ccoef_a(1) = obs_Ra;

for i = 1:nJ
    mod_tstr = bsnsJtoptall.(namesJTO{i});
    loc_sda = adjusted_sd(mod_tstr, teff_err);
    loc_Ra = adjusted_R(mod_tstr, teff_obs, teff_err);
    loc_RMSDa = adjusted_RMSD(mod_tstr, teff_obs, teff_err);
    loc_RMSDa2 = loc_RMSDa^2;
    loc_checka = loc_sda^2 + obs_sda^2 - 2*(loc_sda * obs_sda * ...
                                            loc_Ra);
    loc_descrepa = loc_RMSDa2 - loc_checka;
    
    loc_sd = regular_sd(mod_tstr, teff_err);
    loc_R = regular_R(mod_tstr, teff_obs, teff_err);
    loc_RMSD = regular_RMSD(mod_tstr, teff_obs, teff_err);
    
    
    loc_RMSD2 = loc_RMSDa^2;
    loc_check = loc_sd^2 + obs_sd^2 - 2*(loc_sd * obs_sd * ...
                                         loc_R);
    loc_descrep = loc_RMSD - loc_check;
    
    sdev(i+1) = loc_sd;
    crmsd(i+1) = loc_RMSD;
    ccoef(i+1) = loc_R;
    
    sdev_a(i+1) = loc_sda;
    crmsd_a(i+1) = loc_RMSDa;
    ccoef_a(i+1) = loc_Ra;
end

%% Plot Make taylor diagram (with optimized gamma)

figure;
set(gcf, 'Position', [ 979    30   779   811], 'color', 'white')
clf;
subplot(211)
labels = {'Observations', 'All Factors' ,'Uniform Ballast', 'Uniform Oxygen',...
          'Uniform Temperature', 'Uniform Size'};
[hp, ht, axl] = taylor_diagram(sdev,crmsd,ccoef, 'markerLabel', ...
                               labels, 'markerLegend', 'on',...
                               'colOBS','r','markerobs','s', ...
                               'styleOBS', '-',...
                               'titleOBS','observation',...
                               'colRMS','m', 'styleRMS', ':', ...
                               'widthRMS', 2.0,'tickRMS',...
                               0:0.025: 0.1);

title('A Standard \gamma = 0.40', 'fontsize', 18, 'fontweight', 'bold')



subplot(212)

hold on
[hp, ht, axl] = taylor_diagram(sdev_a,crmsd_a,ccoef_a, 'markerLabel', ...
                               labels, 'markerLegend', 'on',...
                               'colOBS','r','markerobs','s', ...
                               'titleOBS','observation',...
                               'styleOBS','-', 'colOBS','r',...
                               'colRMS','m', 'styleRMS', ':', ...
                               'widthRMS', 2.0, 'tickRMS', 0:0.5: ...
                               2);
title('B Error Weighted', 'fontsize', 18)

export_fig('plots/taylor_gamma' , '-eps')

%% What happens to particle speeds with depth given
%% non-remineralizing ballast

seebins = [10; 20; 100; 200; 500; 1000];
% bin sizes in microns
binsurfacesizes = prof.bal.STP.all.D(seebins)/1e-6;

figure(30);
clf;
subplot(221)
hold all;
title({'STP', '35% Calcum Carbonate', '25% Opal by mass'})
for i = 1:length(seebins)
    plot(prof.bal.STP.all.wz(:,seebins(i)), z, 'LineWidth', 2)
end
set(gca, 'Ydir', 'reverse','FontSize', fs)
ylim([0, 1000])
xlabel('Sinking Speed (m/d)')
ylabel('Depth (m)')

subplot(222)
hold all;
title({'AAZ', '9% Calcum Carbonate', '70% Opal by mass'})
for i = 1:length(seebins)
    plot(prof.bal.AA.all.wz(:,seebins(i)), z, 'LineWidth', 2)
end
set(gca, 'Ydir', 'reverse','FontSize', fs)
ylim([0, 1000])
xlabel('Sinking Speed (m/d)')
ylabel('Depth (m)')

subplot(223)
title({'Base', 'Single material'})
hold all;
for i = 1:length(seebins)
    h = plot(prof.base.all.wz(:,seebins(i)), z, 'LineWidth', 2)
end
set(gca, 'Ydir', 'reverse','FontSize', fs)
ylim([0, 1000])
xlabel('Sinking Speed (m/d)')
ylabel('Depth (m)')

legend('20 um',...
'40 um',...
'200 um',...
'400 um',...
'1000 um',...
'2000 um')

subplot(224)
hold all;
plot(prof.base.W, z, 'Color', [0.8 0.8 0.8], 'LineWidth', 3)
for i = 1:3
    plot(prof.bal.(bal_abr{i}).W, z, 'Color', 'k', 'linestyle', los_linS{i})
end
legend(lt.bal, 'Location', 'Best')
set(gca, 'Ydir', 'reverse','FontSize', fs)
title('Ballast')
xlabel('Sinking Speed (m/d)')
ylabel('Depth (m)')
ylim([0, 1000])


set(gcf, 'units','normalized','outerposition',[0 0 1 1],...
         'Color', [1 1 1])
set(gcf,'PaperUnits','inches','PaperPosition',[0 0 16 8])
export_fig('plots/ballasted_particle_velocity', '-eps')
print('plots/ballasted_particle_velocity', '-dpng')

