function r = sumnan(F,dim)
% replace nans with zeros
ii = find(isnan(F(:)));
F(ii) = 0;
if (nargin == 1)
  r = sum(F);
else
  r = sum(F,dim);
end
