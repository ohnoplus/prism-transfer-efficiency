
function Sol = particle_velo_sink_new_170501(beta,r,z,pscheme)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Definitions and example values of Input Variables
%
% z=-4e3:50:0; % depths of grid box interface
% beta = -4.5; % Slope of PSD power law (default 4.46)
% r = 0.1; % first-order respiration (particle mass decay)
%
% Example calls
%
% Snum = particle_velo_sink(beta,r,z,'srnum'); % numerical solution
% Sanl = particle_velo_sink(beta,r,z,'sran'); % analytical solution
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%% Parameters

sperd=86400; % second per day
micron=1e-6; % conversion from micron to meter

% particle sizes
if strcmp(pscheme,'sran')
    DS = 10 * micron; % smallest particle size (m)
    DL = 2e3 * micron; % largest particle size (m)
elseif strcmp(pscheme,'srnum')
    DS = 1 * micron; % smallest particle size (m)
    %DL = 1e4 * micron; % largest particle size (m)
    DL = 2e3 * micron;
end
dD = 0.1 * micron; % particle bin size (m)
D  = DS:dD:DL;
n2=length(D);

% depths
dz=z(2:end)-z(1:end-1);
n1=length(dz);

% sinking speed versus size - modified Stokes (w = Cw*D^gamma)
gamma = 1.17; 
%gamma = 0; 
w20 = 0.7/sperd; % sinking velocity of 20 micron particle (m/d) 
w1=w20*(DS/(20*micron))^gamma; % sinking velocity of smallest particle (diameter DS)     
Cw = w1/(DS^gamma);
wbin=-Cw*D.^gamma; % negative for downward velocity

%wbin=-Cw*1e-6*(D*1e6).^gamma; % negative for downward velocity
wbin=repmat(wbin,n1,1);

% particle mass versus size (m = Cm*D^alpha)
alpha = 2.28; 
m20 = 0.004; % mass of 20 micron particle (nmolN)
m1=m20*(DS/(20*micron))^alpha; % mass of smallest particle (diameter DS)     
Cm = m1/(DS^alpha);

% respiration rates
Cr = r/sperd; % first-order respiration (particle mass decay constant)

%% Particle size distribution versus depth

if strcmp(pscheme,'sran')

    Mo = 1e-12; % Total mass of PSD at z=0 (mmolN/m3) - for w this is arbitrary
    Mtmp=particle_moments([alpha beta],[DS DL]);
    Cn = Mtmp/(Mo*Cm);
    %Cn = 8.2780e+10; % match Cn in srnum

%    keyboard
    
    C1=Cr/(alpha*Cw);
    rr=D;
    for i=1:length(z)
        a1=(beta+1)/gamma;
        a2=(-z(i)*C1*gamma./(rr.^gamma))+1;
        n(i,:)=Cn*(rr.^beta).*(a2.^a1);
        m(i,:)=dD*n(i,:)*Cm.*rr.^alpha;
        f(i,:)=m(i,:)*Cw.*rr.^gamma;
    end
%    display(Cn);
%figure; loglog(rr,n(end,:)); title(['Analytical']);


elseif strcmp(pscheme,'srnum') 

    
drdt=-Cr/alpha*D; % [m/s]
drdt=repmat(drdt,n1,1); 
dr=dD*ones(1,n2);

%
% ---- solve for particle sizes at different depth
%
dt=-dz(1)./wbin(1,:);
sol=zeros(n1,n2);
sol1=zeros(n1,n2);
sol(n1,:)=D; sol1(n1,:)=ones(1,n2);
for i1=n1-1:-1:1
   % sol(i1,:)=zeros(size(sol(i1+1,:)));
    r0=sol(i1+1,:);
    wbin  = -Cw*r0.^gamma;
    dt    = -dz(i1)./wbin;
    mbin  = Cm*r0.^alpha;
    mbin1 = mbin - Cr*dt.*mbin;
    mbin1(mbin1<0)=0;
    sol(i1,:) = (mbin1/Cm).^(1.0/alpha);
    sol1(i1,:)= sol(n1,:)./sol(i1,:);
    dmax(i1)=max(sol(i1,:));
end
r=sol;
%r=particle_solution_3(dz,dr,wbin,drdt); %size(r); pause;

%
% ---- surface number density
%
    Mo = 1e-12; % Total mass of PSD at z=0 (mmolN/m3) - for w this is arbitrary
    Mtmp=particle_moments([alpha beta],[DS DL]);
    Cn = Mtmp/(Mo*Cm);
    %display(Cn);
    csfc = Cn*ones(size(D)).*D.^beta; % the scale is arbitrary?
%csfc = 1e8*ones(size(D)).*D.^beta; % the scale is arbitrary?
%
% ---- construct real size bins
% ---- must be coarser than the grid for Lagrangian tracking
%
ds=10;
dd=2;
n3=1+(2000-ds)/dd; 
rr=zeros(1,n3);
for i1=1:n3;
    rr(1,i1)=(ds+dd*(i1-1))*micron;
end
%
% ---- calculate number density at different depth
%    
cc0=zeros(n1,n3); %nn0=zeros(n1,n3);
for i1=1:n1
    %cc=repmat(csfc',1,n3);
    cc=repmat((csfc.*sol1(i1,:))',1,n3);
    clear r0;
    r0=sol(i1,:); r0=repmat(r0',1,n3); r0(r0<1e-6)=-1e-4;
    rr0=repmat(rr,n2,1);
    %mask=(abs(r0-rr0)<1e-5);
    mask=(abs(r0-rr0)<0.5*dd*micron);
    cc(~mask)=0.0; %nn=sum(mask);
    %cc0(i1,:)=sum(cc)/dd;
    cc0(i1,:)=sum(cc)./sum(mask);%/dd;
end


n=cc0; 

for i=1:n1
        m(i,:)=micron*dd*n(i,:).*Cm.*reshape(rr,1,n3).^alpha; % total mass of particles in size bin
        f(i,:)=m(i,:).*Cw.*reshape(rr,1,n3).^gamma; % sinking flux of particles in size bin
%    end
end

z=(z(2:end)+z(1:end-1))/2;


else

    error('Particle scheme not recognized.')
    
end


%ind=1+length(z)-size(n,1); Sol.z=z(ind:end)';

Sol.f=f; 
Sol.m=m;
Sol.z=z;
Sol.M=sumnan(m,2);
Sol.F=sumnan(f,2);
Sol.w=(Sol.F./Sol.M)*sperd;
Sol.n=n;
Sol.rr=rr;
Sol.Cn=Cn;
Sol.dmax=dmax;
%Sol.nn=nn;

%% Plots


% figure(1);clf;
% subplot(231);plot(log10(M),-z,'r');
% subplot(232);plot(log10(F),-z,'r');
% subplot(233);plot(w,-z,'r');



