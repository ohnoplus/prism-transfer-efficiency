function M = particle_moments(E,D)

e1=1+sum(E);
M=-(D(1)^e1*(1-(D(2)/D(1))^e1))/e1;


