%% This script demonstrates that the Junhong Liang's earlier
%% solution returns similar results to this one (that bins accross depths)

close all
clear all;
%addpath(genpath('/work/cdeutsch/matlab/m_files'));

z=-4e3:2:0; % depths of grid box interface
%beta = -4.5; % Slope of PSD power law (default 4.46)
beta = -4.5; % Slope of PSD power law (default 4.46)
%r = 0.03; % first-order respiration (particle mass decay)
r = 0.0278; % first-order respiration (particle mass decay)
beta =-4.2;
r = 0.03;
tic;
z1=-4e3:1:0;
Sanl = particle_velo_sink_an(beta,r,-z1'); % analytical solution
toc;
tic;
gamma=1.17; alpha=2.28;
Snum = particle_velo_sink_new_170501(beta,r,z,'srnum'); % numerical solution
toc;

%{
tic;
Sanl1 = particle_velo_sink_old(beta,r,z,'sran'); % analytical solution
toc;
tic;
Snum1 = particle_velo_sink_old(beta,r,z,'srnum'); % numerical solution
toc;
%}

%z=-4e3:5:0; % depths of grid box interface

%Sanl1 = particle_velo_sink_new(beta,r,z,'sran'); % analytical solution
%Snum1 = particle_velo_sink_new(beta,r,z,'srnum'); % numerical solution



%Snum = particle_velo_sink_num(beta,r,z); % numerical solution

%%
micron=1e-6;
pcol='rkgbcmy';
%pz=[1 499 999 -1999 3999];
pz=[100 500 1e3 2e3 4e3];
for i=1:length(pz)
    %[junk,pk(i)]=min(abs(-Sanl.z-pz(i)));
    [junk,pk(i)]=min(abs(-z-pz(i)));
end

for i=1:length(pz)
    %[junk,pk(i)]=min(abs(-Sanl.z-pz(i)));
    [junk,pk1(i)]=min(abs(-z1-pz(i)));
end
%{
for i=1:length(pz)
    [junk,pk1(i)]=min(abs(-Sanl1.z-pz(i)));
end
%}

%%
fac=Snum.Cn/Sanl.Cn;
%fac=1;

figure('unit','centimeters','position',[2 2 30 15],'paperpositionmode','auto');
axes('position',[0.15 0.2 0.32 0.7]);
for i=1:length(pk1)
    xx=Sanl.rr/micron; %mask_tmp=xx<20;
    yy=Sanl.n(pk1(i),:)*1e-20*fac; %yy(mask_tmp)=NaN;
    hplot=loglog(xx,yy,[pcol(i) '-'],'linewidth',2); hold on;
    eval(['h' num2str(i) '=hplot;']);
end

for i=1:length(pk)
    xx=Snum.rr/micron; %mask_tmp=xx<20;
    yy=Snum.n(pk(i),:)*1e-20; %yy(mask_tmp)=NaN;
    loglog(xx,yy,'--k','linewidth',3); hold on;
end

set(gca,'xlim',[10 5e3],'ylim',[1e0 1e10],'fontsize',16);
xlabel('Particle size [\mum]');ylabel('Particle size spectrum (normalized)'); %title('New')
%h_legend=legend([h1 h2 h3 h4 h5],'Surface','500m','1000m','2000m','4000m');
%set(h_legend,'fontsize',10);

axes('position',[0.6 0.2 0.32 0.7]);
%plot(Sanl.w,Sanl.z,'k','linewidth',2); hold on;
%plot(Snum.w,Snum.z,'r','linewidth',2); hold on;
plot(Sanl.F/Sanl.F(end),Sanl.z,'r','linewidth',4); hold on;
plot(Snum.F/Snum.F(end),Snum.z,'--k','linewidth',2.5); hold on;
%plot(Snum1.w,Snum1.z,'--r');hold on;
%plot(Sanl1.w,Sanl1.z,'--k')
%legend('Numerical (new)','Analytical (new)','Numerical (old)','Analytical (old)','fontsize',10,'location','southwest')
%legend('Analytic','Numerical','location','southwest','fontsize',12);
set(gca,'fontsize',16)
xlabel('Surface Normalized Flux');
%%
print -djpeg ./particle_validation.jpeg;
print -depsc ./particle_model_validation.eps;