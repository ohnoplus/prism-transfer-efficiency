#!/bin/bash
# copy over all the data I need to run juramia
# this gathers files form arround the Deutsch lab jetsam and gaggle clusters and puts them in one folder for re-distribution. End users won't have reason (or ability) to run this. You may even need to be user "cram" or to have sudo permissions to get this to work.
rsync /jetwork2/cram/transfer_efficiency/WWDens14Oct2015/juramia_data.mat ./data
rsync /jetwork2/cram/UVP/McDonnellPSD.mat ./data
rsync /jetwork2/cram/UVP/McDonnellPSD.mat ./data
rsync /jetwork2/cram/transfer_efficiency/WWDens14Oct2015/juramia_data.mat ./data
#rsync /jetwork2/tweber/Data/NPP/mon_clim_wgrid/vgpm_clim.mat ./data
rsync /jetwork2/tweber/Data/SeaWIFS/chl_wgrid.mat ./data
#rsync /jetwork2/tweber/Data/woa09_mon.mat ./data

rsync /graid1/cdeutsch/Analysis/CMIP5/ferret/gfdl_esm2m_hist_ballast.nc ./data

## also copy over scripts
rsync /jetwork2/cram/transfer_efficiency/basin_avg.m ./tools
rsync /jetwork2/cram/transfer_efficiency/basin_sum.m ./tools
rsync /jetwork2/cram/transfer_efficiency/basin_profile_avg.m ./tools

rsync /jetwork2/cram/transfer_efficiency/TPSD02Oct2015/pvsto_limw3.m ./maincode

rsync /jetwork2/cram/particle-sinking/waterviscosity.m ./tools
rsync -r /jetwork2/cram/matlabtools/m_map ./tools
rsync /jetwork2/cram/matlabtools/sw* ./tools/sw/
rsync /jetwork2/cram/matlabtools/v2struct.m ./tools/
rsync /jetwork2/cram/matlabtools/fminsearchbnd.m ./tools/
rsync /jetwork2/cram/matlabtools/jm_pcolor.m ./tools/

rsync -r /jetwork2/cram/matlabtools/altmany-export_fig-2200e3d ./tools

rsync /jetwork2/cram/transfer_efficiency/WWDens14Oct2015/franken_teff_1k_2k.mat ./data

rsync /jetwork2/cram/transfer_efficiency/WWDens14Oct2015/fval_fwteff_type_err_16.m ./maincode
rsync /jetwork2/cram/transfer_efficiency/WWDens14Oct2015/calc_fwteff_type_16.m ./maincode
rsync /jetwork2/cram/transfer_efficiency/WWDens14Oct2015/calc_teff_type_16.m ./maincode
rsync /jetwork2/cram/transfer_efficiency/WWDens14Oct2015/rescale_map_mxb.m ./tools
rsync /jetwork2/cram/transfer_efficiency/WWDens14Oct2015/CrsAm616toptall.mat ./data
rsync /jetwork2/tweber/sharing/sat_Cexport_all_algorithms.mat ./data
rsync /jetwork2/tweber/sharing/franken_weight_map.mat ./data
rsync -r /jetwork2/cram/matlabtools/comp_struct.m ./tools


