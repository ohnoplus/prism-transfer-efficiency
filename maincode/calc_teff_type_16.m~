function [TeffX, stuff] = calc_teff_type_16(grid,M3d,Cr, Q10, KmOxy, mode, mP_CaCO3, ...
                                            mP_Opal ,alpha,beta_map, gamma, ...
                                            Temp, Dens , oxy ,nplayers, rs, rw_std, ...
                                            rv, rd, ds, dl,teff_depth)
    
    % Calculates transfer efficiency at every coordinate point
    %
    % Input: 
    % grid: lat lon depth and such (structure)
    % M3d: where are land (structue)
    % Cr: Reminerilization constant at 4C (nT)
    % Q10: Temperature sensitivity (nT) 
    % mode: tells about how temperature dependence will be handled
    % mP_CaCO3: mass percentage of calcium carbonate everywhere
    % (nx*ny)
    % mP_Opal: mass percentage opal (nx*ny)
    % alpha: fractal dimension
    % beta_map: PSD slope at each location (nx * ny)
    % gamma: sinking power function
    % Temp: water temperature everywhere (nx*ny*nz)
    % Dens: water potential density everywhere (nz*ny*nz)
    % nplayers: number of productive layers, just use 2
    % rs: standard particle mass (scalar)
    % rw_std: standard water mass (scalar)
    % rv: wet weight of different particle components (nT)
    % rd: dry weight of different particle components (nT)
    % teff_depth: depth to which we are calculating transfer efficiency.
    %
    % Output
    % Teff1000: Unitless, transfer efficiency at 1000m



    % Depth from which we want to know transfer efficiency
    %teff_depth = 1000;
    teff_layer = find(grid.zw >=teff_depth, 1);

    % days per yearr
    dpery = 365.25;

    % sinking rate (d^-1) %JAC: seems like a strange comment
    %r = kpop/dpery; %JAC: not sure why we are deviding by 365, or whats
    %going on at all, but whateves.
    %r = Cr;

    %mode is four to run all the things
    %mode = par.mode;

    % ocean and surface indices
    iocn = find(M3d(:)==1); 
    isurf = find(M3d(:,:,1)==1);
    [j1,idif] = setdiff(iocn,isurf);
    m = length(iocn);
    ns = length(isurf);
    [ny,nx,nz] = size(M3d);

    % depth of grid box interface counting from surface (zw = 0) to bottom
    ZW = cat(3,grid.ZW3d,grid.ZW3d(:,:,end)+grid.DZT3d(:,:,end));

    % set z = 0 at depth of euphotic zone
    ZW = ZW-grid.zw(nplayers+1);
    ZW(:,:,1:nplayers+1) = 0;

    % ignore land points
    MS = 0*ZW;
    MS(:,:,1) = M3d(:,:,1);
    for k = 2:nz+1
        MS(:,:,k) = M3d(:,:,k-1);
    end
    im = find(MS(:)==1);

    % % convert beta to 3-d map
    % B = 0*M3d;
    % B(isurf) = beta;
    % for k = 2:nz+1
    %     B(:,:,k) = B(:,:,1);
    % end
    %beta reads in as a map now

    % reshape temperature data
    % Now feeding in seasonal temp
    % Temp is now input at top
    %Temp = M3d*NaN;
    %Temp(iocn) = par.temp;

    %Extract Q10 value
    %This is now fed in as a dynamic variable at top
    %Q10 = par.Q10;

    % get sinking velocity
    fsink = 0*ZW;
    zvec0t = squeeze(grid.zt); % box midpoint depths
    zvec0w = squeeze(grid.zw);
    % Depth Thickness changed to 100 for fasterness
    depth_thick = 10; % leave at 10
                      %zvec_fine = (0:depth_thick:(grid.zw(end)+grid.dzt(end)))'; %depth layers for
                      %remin model
    maxdeep = 1500;
    zvec_fine = (0:depth_thick:maxdeep)';
    warning('off', 'MATLAB:chckxy:IgnoreNaN')
    TeffX = repmat(NaN, ny, nx);
    inW = repmat(NaN, ny, nx);
    outW = repmat(NaN, ny, nx);
    mP_dry_car = repmat(NaN,ny,nx);
    mP_dry_si = repmat(NaN,ny,nx);
    mP_car =  repmat(NaN,ny,nx);
    mP_si =  repmat(NaN,ny,nx);
    P_car =  repmat(NaN,ny,nx);
    P_si =  repmat(NaN,ny,nx);
    rho_part =  repmat(NaN,ny,nx);
    w20um = repmat(NaN,ny,nx);
    w1mm = repmat(NaN,ny,nx);
    rls = repmat(NaN, ny, nx);
    W200 = repmat(NaN, ny, nx);
    crz200 = repmat(NaN, ny, nx);
    % rls1000 = repmat(NaN, ny, nx);
    W1000 = repmat(NaN, ny, nx);
    crz1000 = repmat(NaN, ny, nx);

    % standard particle size (20um)
    xmm = 20;
    % standard partticle sinking speed (m/d)
    wxmm = 2; %
    mxmm = 0.004;
    %keyboard
    tic
    parfor i=1:ny
        %keyboard
        %for i = 1:ny
        teff_slice = repmat(NaN,1,nx);
        inW_slice = repmat(NaN,1,nx);
        outW_slice  = repmat(NaN,1,nx);
        mP_dry_car_slice = repmat(NaN,1,nx);
        mP_dry_si_slice = repmat(NaN,1,nx);
        mP_car_slice =  repmat(NaN,1,nx);
        mP_si_slice =  repmat(NaN,1,nx);
        P_car_slice =  repmat(NaN,1,nx);
        P_si_slice =  repmat(NaN,1,nx);
        rho_part_slice =  repmat(NaN,1,nx);
        w20um_slice = repmat(NaN,1,nx);
        w1mm_slice = repmat(NaN,1,nx);
        rls_slice = repmat(NaN, 1, nx);
        W200_slice = repmat(NaN, 1, nx);
        crz200_slice = repmat(NaN, 1, nx);
        % rls1000_slice = repmat(NaN, 1, nx);
        W1000_slice = repmat(NaN, 1, nx);
        crz1000_slice = repmat(NaN, 1, nx);
        for j=1:nx
            % i = 50; j = 100;
            % i = 62; j = 90
            % Get temperature and dpeth values that actually go into
            % the sinking model


            if(M3d(i, j, teff_layer) == 1 & all(isfinite(Dens(i, j, 3:teff_layer)))&...
               all(isfinite(Temp(i, j, 3:teff_layer))))
                %deep enough only
                %Vector of temperatures and depths, 24 long
                Tvec0 = squeeze(Temp(i, j, :));
                Dvec0 = squeeze(Dens(i, j, :));
                O2vec0 = squeeze(oxy(i, j, :));
                
                %High resolution, unclipped vector
                warning('off', 'MATLAB:chckxy:IgnoreNaN')
                Tvec_fine = spline(zvec0t, Tvec0, zvec_fine);
                Dvec_fine = spline(zvec0t, Dvec0, zvec_fine);
                O2vec_fine = spline(zvec0t, O2vec0, zvec_fine);
                
                %Find top and bottom of simulated water column
                ztop = zvec0w(nplayers+1);
                
                land_index = find(M3d(i, j, :) == 0);
                last_water = nz;
                if length(land_index) > 0
                    last_water = land_index(1) -1;
                end
                last_interface = last_water + 1;
                zbottom = grid.zw(last_water) + grid.dzt(last_water);
                
                %Trim the water column
                % zvec_ok = zvec_fine>ztop & zvec_fine<zbottom;
                zvec_ok = zvec_fine>ztop;
                
                zvec_ok_idx = find(zvec_ok);
                % zvec1 = 0:depth_thick:(zbottom-ztop); %+depth_thick*10);
                % Tvec1 = Tvec_fine(zvec_ok_idx); 
                zvec1 = 0:depth_thick:(maxdeep - ztop);
                Tvec1 = Tvec_fine(zvec_ok_idx);
                Dvec1 = Dvec_fine(zvec_ok_idx);
                O2vec1 = O2vec_fine(zvec_ok_idx);
                
                zvec00 = [zvec0w zbottom];
                zvec000 = zvec00((nplayers+1):last_interface) - ztop;
                z2 = [0 teff_depth - ztop];
                
                % correct local beta value
                %loc_beta = B(i, j, 1);
                loc_beta = beta_map(i,j);
                
                % local mass percentages
                loc_mP_CaCO3 = mP_CaCO3(i,j);
                loc_mP_Opal = mP_Opal(i,j);
                loc_mP_POC = 1 - loc_mP_CaCO3 - loc_mP_Opal;
                loc_mP = [loc_mP_CaCO3, loc_mP_Opal, loc_mP_POC];
                

                %Run the thing
                Sol = pvsto_limw3(alpha,loc_beta,gamma,Cr, ...
                                 Q10, KmOxy, xmm, mxmm, wxmm, mode, ds, dl,...
                                 rs, rv, rd, Dvec1, rw_std, loc_mP, Tvec1, O2vec1, zvec1, ...
                                            z2);
                
                % Calculate reminerilization length scale for a 1mm
                % particle at 100m
                % index at 100m % 11, hardcoded
                % index of 1mm particle at 100m
                [diff, cidx] = min(abs(Sol.all.DZ(11,:) - 1/1000));
                  % rls_loc = Sol.all.Cww(11)./Sol.all.crz(11,3);
                if(diff < 1/10000)
                    % rls_loc = (Sol.all.crz(11,3) .* Sol.all.Cm )./...
                    %       (Sol.all.Cww(11) * Sol.all.rprsZ(11,cidx)); ...
                    % %m
                    rls_loc = Sol.all.Cww(11) .* ... % sinking speed
                              Sol.all.rprsZ(11,cidx) .* ... % density
                              (1/1000)^(alpha-1)./... % radius m ^ gamma
                              Sol.all.crz(11,3); % remin
                  
                else
                    %don't bother if there are no 1mm particles at 100m
                    rls_loc = NaN;
                end
                % sinking speed and crz at 200m
                [jnk, zidx200] = min(abs(zvec1 - 200));
                [jnk, zidx1000] = min(abs(zvec1 - 1000));
                W200_loc = Sol.W(zidx200);
                crz200_loc = Sol.all.crz(zidx200,3);
                W1000_loc = Sol.W(zidx1000);
                crz1000_loc = Sol.all.crz(zidx1000,3);
                
                                                     
            
            
            TeffX_loc = Sol.F(2,3);
            teff_slice(1,j) = TeffX_loc;
            
            % Speed matrix
            % surface
            inW_loc = Sol.W2(1);
            inW_slice (1, j) = inW_loc;
            % mesopelagic
            outW_loc = Sol.W2(2);
            outW_slice (1,j)  = outW_loc;
            
            %ballast things
            % mass percent
            
            mP_dry_car_loc = Sol.all.mP_dry(1);
            mP_dry_si_loc = Sol.all.mP_dry(2);
            mP_car_loc = Sol.all.mP(1);
            mP_si_loc = Sol.all.mP(2);
            P_car_loc = Sol.all.P(1);
            P_si_loc = Sol.all.P(2);
            rho_part_loc =  Sol.all.rvrs1 * Sol.all.rs;
            w20um_loc = Sol.all.WZ(1,10);
            w1mm_loc = Sol.all.WZ(1,500);
            
            mP_dry_car_slice(1,j) = mP_dry_car_loc;
            mP_dry_si_slice(1,j) = mP_dry_si_loc;
            mP_car_slice(1,j) = mP_car_loc;
            mP_si_slice(1,j) = mP_si_loc;
            P_car_slice(1,j) = P_car_loc;
            P_si_slice(1,j) = P_si_loc;
            rho_part_slice(1,j) = rho_part_loc;
            w20um_slice(1,j) = w20um_loc;
            w1mm_slice(1,j) = w1mm_loc;
            rls_slice(1,j) = rls_loc;
            W200_slice(1,j) = W200_loc;
            crz200_slice(1,j) = crz200_loc;
            W1000_slice(1,j) = W1000_loc;
            crz1000_slice(1,j) = crz1000_loc;
            
        end
    end
    TeffX(i, :) = teff_slice;
    inW(i,:) = inW_slice;
    outW(i,:) = outW_slice;
    mP_dry_car(i,:) = mP_dry_car_slice;
    mP_dry_si(i,:) = mP_dry_si_slice;
    mP_car(i,:) = mP_car_slice;
    mP_si(i,:) = mP_si_slice;
    P_car(i,:) = P_car_slice;
    P_si(i,:) = P_si_slice;
    rho_part(i,:) = rho_part_slice;
    w20um(i,:) = w20um_slice;
    w1mm(i,:) = w1mm_slice;
    rls(i,:) = rls_slice;
    W200(i,:) = W200_slice;
    crz200(i,:) = crz200_slice;
    W1000(i,:) = W1000_slice;
    crz1000(i,:) = crz1000_slice;
    
end
toc
stuff.outW = outW;
stuff.inW = inW;
stuff.mP_dry_car = mP_dry_car;
stuff.mP_dry_si = mP_dry_si;
stuff.mP_car = mP_car;
stuff.mP_si = mP_si;
stuff.P_car = P_car;
stuff.P_si = P_si;
stuff.rho_part = rho_part;
stuff.w20um = w20um;
stuff.w1mm= w1mm;
stuff.rls = rls;
stuff.W200 = W200;
stuff.crz200 = crz200;
stuff.W1000 = W1000;
stuff.crz1000 = crz1000;
%pool = gcp;
%delete(pool)