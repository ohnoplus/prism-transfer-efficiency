function Sol = pvsto_limw3(alpha, beta,gamma, ...
                                                 cr, Q10, KmOxy, xmm, mxmm, wxmm, mode, ds, dl, rs, rv, rd, rw, rw_std, ...
                                             mP_dry, surt, oxy, z, z2)
% Calculate a single sinking profile
% This function takes
% cr: a vector of reminerilization constants, one for each
% ingredient type
% Q10: a vector of Q10 values, one for each ingredient type
% if mode is a scalar
% mode: 1 = temperature affects nothing; 2 = temperature affects sinkng
% speed; 3 = temperature affects reminerilization 4 = temperature
% affects
% if mode is a vector, the first element is the temperature mode
% the second determines whether pore space is being treated
% seperately
% mode: 1 = standard structure
% mode: 2 = special conversion for pourous particles
% rp (rho prime) a vector of density differences from a
% standard particle -- now calculated
% rs: standard particle density g/cm^3
% rv: wet density of ingredients making up the particle; equal to
% rd for ballast, generally.
% gel: indicates whether each component is a gel(1) and is
% dissolved in water or not (0) (discontinued in verion 4)
% rd: dry density of ingredients making up the particleoo
% rw: mass of seawater
%   - 1 element: for entirce water colunn
%   - nZ element: for each depth
% maybe I should pull the dry density bit out of here and run as
% needed outside of this script...
% mP: (proportions) a vector of the fraction of the particle that is
% each proportion of stuff ??? mass or volume???
% 
% z is positive downwards
% beta is positive
% cr is positive
% rp is positive
% gamma is positive
% alpha is positive

% So the theory here is that particles are made of several
% components. Ballast components have higher density. Some
% components (ballast and recalcetrent DOM may have lower
% reminerilization rates). Density affects sinking speed.

if(rw(1) > 900)
    warning(['Water density should be in units of g/ml and should be just above 1, but it ' ...
             'currently has a value of over 900, please double ' ...
             'check'])
end

% oxygen
%KmOxy = 20;


% Parse mode
if length(mode) == 1
    tmode = mode;
    smode = 1;
elseif length(mode) == 2
    tmode = mode(1);
    smode = mode(2);
else
    error('mode must be a scalar or vector of length 2')
end

        

% check to make sure the relevant vecttors have the same length
if ~(length(mP_dry) == length(rd) & length(mP_dry) == length(rv))
    error('mP_dry, rd and rv must all have the same length')
end



% Convert mass persentages to volume percentages
P = (mP_dry ./rd) ./ sum(mP_dry./rd);

% conversion factors
sperd = 86400; % second per day
micron = 1e-6;
nz = length(z);
dz = diff(z);



% particle sizes
% DS = 20*micron; % smallest particle size (um)
% DL = 2e3*micron; % largest particle size (um)
DS = ds*micron; % smallest particle size (um)
DL = dl*micron; % largest particle size (um)
dD = 2*micron; % particle bin size (um)
dD = 2*micron; % particle bin size (um)
D  = DS:dD:DL;
D2 = [DS-dD/2:dD:DL+dD/2];
nD = length(D);
DD = repmat(D,[nz 1]);
DD2 = repmat(D2,[nz 1]);

% fixed parameters
%w20 = 0.7; % sinking velocity of 20 micron particle (m/d) 
%w20 = 2.5; % updated
% m20 = 0.004; % mass of 20 micron particle (arbitrary)
%              % note, Alldrege suggests 22.18ug = 2.22e-5g
m1mm = 3.3 * 10^-6; % mass in grams of a 1mm particle Alldredge 1998
n0 = 1; % arbitrary

% calculate constants Cw and Cm
% w1 = w1mm*(DS/(1000*micron))^gamma; % sinking vel. of smallest particle
% Cw = w1/(DS^gamma);

% m1 = m20*(DS/(20*micron))^alpha; % mass of smallest particle   
% Cm = m1/(DS^alpha);


Cw = wxmm/(xmm*micron)^gamma;
Cm = mxmm/(xmm*micron)^alpha;

% Deal with structure, redefine gamma and Cw if we are doing things
% Curtis' way. Doesn't work. Alternative
% if smode == 2
%     Cw0 = Cw;
%     w1_0 = w1;
%     %Cw = Cw0 * Cm / (rs_dry * pi* (4/3) * (rs_dry + rw_std));
%     gamma0 = gamma;
%     gamma = gamma0 + alpha - 3;
%     w1 = w20*(DS/(20*micron))^gamma;
%     Cw = w1/(DS^gamma);
% elseif smode == 1
% else
%     error('smode must be 1 or 2')
% end

if smode ~=1
    error('smode is depreciated and can only be 1')
end

% size of particles with depth
DZ = zeros(nz,nD);
DZ(1,:) = D;

%deal with rho_water
if length(rw) == 1
    rw_vec = repmat(rw, length(z), 1);
elseif length(rw) == nz
    rw_vec = rw;
else
    error('rw must be a one or length(z) element vector')
end

% calculate wet mass from dry mass
% gels are dry mass + water mass
% non gels are dry mass only
%rv = rd+gel*rw(1);
% rv is an input again
%rs = rs_dry + rw_std; % sure why not.

% and rv varies with water density, at least for the gels
nT = length(mP_dry); % # ingredients in particle
% rvM = repmat(rd, nz, 1) + repmat(gel, nz, 1).*repmat(rw_vec, 1, ...
%                                                   nT);
rvM = repmat(rv, nz, 1);
% so does the standard mass though. We are assuming the whole
% particle volume is a gel here
%rsM = repmat(rs_dry, nz, nT) + repmat(rw_vec, 1, nT);

% mass proportions, wet
mP = P .* rv / sum(P.*rv); % I find this puzzling 06 May 2016

% Matrixes of size (nz, nT)

rwM = repmat(rw_vec, 1, nT);
%rvM = repmat(rv, nz, 1);
rsM = repmat(rs, nz, nT); % there might be a problem with how I am
                          % dealing with these "standard" densities

%Matrixes of size(nD, nT)
rvMD = repmat(rv, nD, 1);

% rho_prime calculations
%rho_water = 1; %Water is 1g/cm^3, at least for now
rpM = rvM-rwM; % Bouyant effective mass, each type, each depth
               
%rswM = rsM-rwM; % Bouyant effective mass of
               %standard particle, each depth
               %rwstd = 1.0250;
rswM = rsM-rw_std; % Bouyant effective mass of standard particle, non
              % variable water density
rvrs = rv./rs; %component mass over standard mass
rprsM = rpM./rswM;  %compnent bouyant mass differential

%surface particle calculations with proportions
rvrsP = rvrs.*P; % each component's relative mass times its
                % proportion of particle volume (or is it
                % proportion of particle  mass?)
rvrs1 = sum(rvrsP); %Whole particle's density at surface
rprsP = rprsM(1,:) .* P; 
rprs1 = sum(rprsP); %Whole particle's apparent density at surface


% the total mass of each particle at each depth
%rpro = sum(rprs.*P); %particle mass over standard particle mass
                     %rpP = rprs.*P; % component relative mass times proportion
% total mass of each size class particles
m_tot_surf = Cm * rvrs1 * D .^ alpha;
% total mass of each kind of matter in each size class particle
m_type_surf = Cm * rvrsP' * D .^ alpha;
% number of types of matter

% for i = 2:nz
%     dD = -cr*DZ(i-1,:).^(1-gamma)*dz(i-1)./(alpha*Cw);
%     DZ(i,:) = DZ(i-1,:)+dD;
% end

timtemp = 4; %temp at which Cw = Cw2/mu
% Calculate temperature profile form surt (surface temp)
if(length(surt) == 1)
    if(surt >=0)
        TR = temp_approx_3(surt, z, 1);
    else
        TR = repmat(timtemp, 1, nz+1);
    end
else
    TR = surt;
end

% make sure TR runs vertically
TR = reshape(TR, length(TR), 1);


% Temperature Dependent Sinking Constant (JAC)
mu = waterviscosity(TR)';
%muM = repmat(mu', [1, nD]);
Cw2 = Cw.*waterviscosity(timtemp);
if(tmode == 2 | tmode == 4)
    Cww = Cw2./mu;
elseif (tmode == 1 | tmode ==3)
    Cww = repmat(Cw, [1 nz]);
else
    error('the tmode variable is not legal')
end
%CwwM = Cw2./muM; % not used currently

% Temperature Dependent Reminerilization Constant (JAC)
crM = repmat(cr, nz, 1);
Q10M = repmat(Q10, nz, 1);
KmOxyM = repmat(KmOxy, nz, 1);
TRM = repmat(TR, 1, nT);
oxyM = repmat(oxy, 1, nT);
if(tmode == 3 | tmode == 4)
    crz = crM.*Q10M.^(((TRM-4)/10)).* oxyM./(KmOxyM + oxyM);
elseif (tmode == 1 | tmode == 2)
    crz = crM;
else
    error('the tmode variable is not legal, failure in remin')
end
crzp = permute(crz, [1 3 2]);
crzM = repmat(crzp, [1 nD 1]);


% DZ is the particle size vector
MZ = zeros(nz, nD, nT); % wet mass of each ingredient at each size class
MZtot = zeros(nz, nD); % mass of each particle size class
WZ = zeros(nz, nD); % sinking speed of each particle size class
dtZ = zeros(nz, nD); % time to sink to next depth
PZ = zeros(nz, nD, nT); % ingredient proportion by volume
mPZ = zeros(nz, nD, nT); % ingredient proportion by mass
rvrsZ = zeros(nz, nD); % mass over standard mass at each depth
rprsZ = zeros(nz, nD); % apparent mass over apparent standard mass

MZ(1, :, :) = m_type_surf';
%MZtot(1,:) = m_tot_surf';
WZ(1,:) = Cww(1)*rprs1 * D .^gamma;
dtZ(1,:) = dz(1)./WZ(1,:);
PZ(1,:,:) = repmat(P, nD,1);
mPZ(1,:,:) = repmat(mP, nD, 1);
rprsZ(1,:) = repmat(rprs1, 1, nD);
rvrsZ(1,:) = repmat(rvrs1,1, nD);

for i = 2:nz
    %why is everything exploding around iteration 100?
    %change in mass wrt time and temp
    %    dmdt = squeeze(crzM(i-1, :, :)).*squeeze(MZ(i-1,:,:));  %
    %    TEMPERATURE CHECK
        dmdt = reshape(crzM(i-1, :, :),[nD nT]).*reshape(MZ(i-1,:,:),[nD ...
                   nT]);
    dmdz = dmdt .* repmat(squeeze(dtZ(i-1,:)'), 1, nT);
    %update masses of particles
    MZ_loc = reshape(MZ(i-1,:,:), [nD nT]) - dmdz;
    MZ_loc(MZ_loc<0|isnan(MZ_loc)) = 0;
    MZ(i,:,:) = MZ_loc;
    %update proportions
    %Problem:  these are mass proportions, but were defined as
    %volume proportions. :(
    mPZ(i,:,:) = reshape(MZ(i,:,:),[nD nT])./repmat(nansum( reshape(MZ(i,:,:),[nD nT]), ...
                                                    2),1,nT);
    loc_mPZ = reshape(mPZ(i,:,:), [nD nT]);
    sum_mPZ = repmat(sum(loc_mPZ./rvMD,2), 1, nT);
    PZ(i,:,:) = (loc_mPZ./rvMD)./sum_mPZ;
    %particle densities and bouyancies at local depth
    rprsZ(i,:) = sum(repmat(rprsM(i,:),nD,1).*reshape(PZ(i,:,:), [nD nT]),2)';
    % chaos traced here
    rvrsZ(i,:) = sum(repmat(rvrs,nD,1).*reshape(PZ(i,:,:), [nD nT]),2)';
    %update particle diameters
    DZ_loc = (sum(reshape(MZ(i,:,:), [nD nT]),2)'./(Cm.*rvrsZ(i,:))).^(1/ ...
                                                      alpha);
    DZ_loc(DZ_loc<0|isnan(DZ_loc)) = 0;
    DZ(i,:) = DZ_loc;
    %update sinking speeds
    WZ(i,:) = Cww(i) * rprsZ(i,:).*DZ(i,:) .^ gamma; % TEMPERATURE CHECK
    % I don't remember what this does
    if(i<nz)
    dtZ(i,:) = dz(i)./WZ(i,:);
    end
end

%don't actually use this, but
MZtot = sum(MZ, 3);

%Total particle wet density
rv3(1,1,:) = rv;
rvblock = repmat(rv3, nz, nD, 1);
densZ = sum(rvblock.*PZ,3); %density of particles at each size-depth

WZ = real(WZ);
WZ(WZ<0| isnan(WZ)) = 0;

% convert wet to dry mass

%rdrv = permute(rd./rv, [1 3 2]); %permute so it is oriented in the
                                 %third dimension
%rdrvm = repmat(rdrv, nz, nD); %convert wet to dry mass matrix

%MZ_dry = MZ.*rdrvm;

% mass and sinking velocity with depth
% mz = Cm*DZ.^alpha; % mass of particles
% wz = Cw*DZ.^gamma; % sinking velocity of particles

mz = sum(MZ, 3);
%mz_dry = sum(MZ_dry, 3);
wz = WZ;


% number of particles with depthn
c0 = repmat(n0*D.^-beta.*wz(1,:),[nz 1]);
N = c0./wz;



% particle flux
F = nansum(N.*wz.*mz,2);
%F_dry = nansum(N.*wz.*mz_dry, 2);


% mass-weighted sinking velocity
W = nansum(N.*wz.*mz,2)./nansum(N.*mz,2);

NZ = repmat(N, 1, 1, nT);
WZ2 = repmat(WZ, 1,1,nT);

FZ2 = NZ.*WZ2.*MZ;

FZ = squeeze(nansum(FZ2,2));

F2 = interp1(z, F, z2);

% Normalize FZ and F
FZ_topM = repmat(FZ(1,:), size(FZ, 1), 1);
FZ_norm = FZ./FZ_topM;

FZ_out = interp1(z, FZ_norm, z2);

F_norm = F/F(1);

W2 = interp1(z, W, z2);

% Sol.N = N;
%Sol.F = F2;
Sol.W = W; 
Sol.FZ = FZ_norm;
Sol.F = FZ_out;
Sol.W2 = W2;
% Sol.R = DZ;
% Sol.MZ = MZ;
% Sol.MZ_dry = MZ_dry;
% Sol.D = densZ;
Sol.all = v2struct;
