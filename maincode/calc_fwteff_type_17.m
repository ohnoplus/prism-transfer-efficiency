function [bsns_fteff1000, teff1000, stuff, ks] = calc_fwteff_type_17(Cr4, ...
                                                  Q10, CaI, OI, ...
                                                  alpha, gamma ,mf, bf, ...
                                                      rhoPOM, KmOxy, par1)
% calculate flux weighted transfer efficiency
% Inputs:
% Cr4: base remineralization rate
% Q10: temperature dependence of the remineralization rate
% CaI: calcium incorperation percentage (generally set to 1)
% OI: opal incorperation percentage (generally set to 1)
% alpha: particle fractal dimension
% mf: rescaling slope parameter for particle size distribution
% bf: rescaling intercept parameter for particle size distribution
% rhoPOM: density of organic matter
% kmOxy: Oxygen dependence Km
% par1: An object with a bunch of other parameters. Some of these
% are used in this script, others are passed forward to other scripts.
%
% Note that I usually only set Cr4, Q10, KmOxy as free
% parameters. I experemented with varying others of thesee at one
% point in the development process
%
% Outputs:
% bsns_fteff1000: Basin wide spatially flux-weighted transfer efficiency
% values
% teff1000: transfer efficiency values at each grid cell
% stuff: An object with a bunch of parameters, passed forward from
% calc_teff_type_17
% ks: every parameter found within this file. Used for
% debugging. Generally do not save to memory as it is big.
%
% Called by: fval_fwteff_tcype_err_17
% Calls: calc_teff_type_17



% unpacking
grid = par1.grid;
M3d = par1.M3d;
mode = par1.mode;
%alpha = par1.alpha;
%gamma = par1.gamma;
%gamma = alpha - 1; % Changed 18 Apr 2016
% gamma reads in as an arument as of 29 Aug 2016
nplayers = par1.nplayers;
maxdeep = par1.maxdeep;

export = par1.export;
mass = par1.mass;
phys = par1.phys;
component = par1.component;
component.rho_POM = rhoPOM;

teff_depth = par1.teff_depth;
% need to define
%keyboard
PSD = rescale_map_mxb(par1.PSD, mf, bf);
%keyboard
R2d = par1.R2d;
regnames = par1.regnames;
EX = par1.EX;

% size limits (um)
ds = par1.ds;
dl = par1.dl;


AREA = grid.DXT3d.*grid.DYT3d;
area = AREA(:,:,1);


% Export 2 had all of the zeros removed, and the carbon estemates
% increased by 1mol/m^2/yr or whatever the units are
export2 = export;
export2.CaCO3(find(export2.CaCO3 < 0)) = 0;
export2.opal(find(export2.opal < 0)) = 0;
export2.C(find(export2.C < 0)) = 0;
%export2.C = export2.C + 1; % commented out 03 aug 2016

export3.CaCO3 = export2.CaCO3 * CaI;
export3.opal = export2.opal * OI;
export3.C = export2.C;

% Calculate export percentages
f.CaCO3 = (export3.CaCO3*mass.CaCO3)./((export3.CaCO3*mass.CaCO3) + ...
                                       (export3.opal*mass.opal) + ...
                                       (export3.C*mass.C));

f.opal = (export3.opal*mass.opal)./((export3.CaCO3*mass.CaCO3) + ...
                                    (export3.opal*mass.opal) + ...
                                    (export3.C*mass.C));
f.C = (export3.C*mass.C)./((export3.CaCO3*mass.CaCO3) + ...
                           (export3.opal*mass.opal) + ...
                           (export3.C*mass.C));


rv = [component.rho_CaCO3, component.rho_opal, component.rho_POM];
rd = [component.rho_CaCO3, component.rho_opal, component.rho_POM *component.c2pom];
% flag! 16 Nov 2015, shoudl be rho_POM as well
% corrected upstream! Just make sure these component.rho_POM = component.dm_POM
%rd = [component.rho_CaCO3, component.rho_opal, component.rho_POM]; %
                                                                  % flag!
% replacing dens with pdens

[teff1000 stuff] = calc_teff_type_17(grid,M3d,[0 Cr4 Cr4], [Q10 Q10 Q10], [KmOxy, KmOxy, KmOxy], mode, f.CaCO3, ...
                            f.opal ,alpha,PSD, ...
                            gamma , phys.temp, phys.pdens, phys.o2 ,nplayers, ...
                            component.rho_std, component.rho_water_std, ...
                            rv, rd, ds, dl, maxdeep, teff_depth);

    

EX1000 = teff1000 .* EX; %% Left off here 19 Oct 2015

%teff_mask = ~isnan(teff1000(:,:,1));

% for seas = 1:12
%     teff_mask = ~isnan(teff1000(:,:,seas));
%     teff_mask_seas(:,:,seas) = teff_mask;
%     %total basin surface export for each season
%     basin_EX(seas,:) = basin_sum(teff_mask.*EX(:,:,seas).*area, ...
%                                  R2d);
%     basin_EX1000(seas,:) = basin_sum(teff1000(:,:,seas) .* EX(:,:,seas) ...
%                                      .*area, R2d);
% end
teff_mask = ~isnan(teff1000);
basin_EX = basin_sum(teff_mask.*EX.*area, R2d);
basin_EX1000 = basin_sum(EX1000.*area, R2d);


%Divide total basin 1000m export by basins surface export
bsns_fteff1000 = basin_EX1000./basin_EX;
% % Get annual averages
% bsns_fteff1000_an = squeeze(mean(bsns_fteff1000, 1));
% % Package for export

% kitchen sink, for debugging
ks = v2struct(); 