function [bsns_fteff1000_an, teff1000_clim, stuff_clim, ks] = calc_fwteff_type_seas_17(Cr4, ...
                                                  Q10, CaI, OI, ...
                                                  alpha,mf, bf, ...
                                                      rhoPOM, KmOxy, par1)
% calculate flux weighted transfer efficiency, using seasonal inputs
% Inputs:
% Cr4: base remineralization rate
% Q10: temperature dependence of the remineralization rate
% CaI: calcium incorperation percentage (generally set to 1)
% OI: opal incorperation percentage (generally set to 1)
% alpha: particle fractal dimension
% mf: rescaling slope parameter for particle size distribution
% bf: rescaling intercept parameter for particle size distribution
% rhoPOM: density of organic matter
% kmOxy: Oxygen dependence Km
% par1: An object with a bunch of other parameters. Some of these
% are used in this script, others are passed forward to other scripts.
%
% Note that I usually only set Cr4, Q10, KmOxy as free
% parameters. I experemented with varying others of thesee at one
% point in the development process
%
% Outputs:
% bsns_fteff1000_an: Basin wide seasonally and spatially flux-weighted transfer efficiency
% values
% teff1000_clim: transfer efficiency values at each grid cell, for
% each of the 12 months of the year (climatology)
%
% stuff_clim: An object with a several parameters
% stuff_clim.outW(:,:,t) = loc_stuff.outW;
% sinking speed at teff_depth, at each
% latitude, longitude and climatology month
% an nx * ny * nt matrix,
%
% stuff_clim.inW(:,:,t) = loc_stuff.inW;
% sinking speed at base of euphotic, at each
% latitude, longitude and climatology month
% an nx * ny * nt matrix,

% stuff_clim.fpoc(:,:,:,t) = loc_stuff.fpoc;
% flux of particulate organic carbon at each
% depth, latitude, longitide and climatology month
% an nx * ny * nz *nt matrix,
%
% ks: every parameter found within this file. Used for
% debugging. Generally do not save to memory as it is big.
%
% Called by: juramia_teff_script
% Calls: calc_teff_type_17

% unpacking
grid = par1.grid;
M3d = par1.M3d;
mode = par1.mode;
%alpha = par1.alpha;
%gamma = par1.gamma;
gamma = alpha - 1; % Changed 18 Apr 2016
nplayers = par1.nplayers;
maxdeep = par1.maxdeep;

export = par1.export;
mass = par1.mass;
phys = par1.phys;
component = par1.component;
component.rho_POM = rhoPOM;

teff_depth = par1.teff_depth;
% need to define
%keyboard
PSD = rescale_map_mxb(par1.PSD, mf, bf);
%keyboard
R2d = par1.R2d;
regnames = par1.regnames;
EX = par1.EX;

% size limits (um)
ds = par1.ds;
dl = par1.dl;


AREA = grid.DXT3d.*grid.DYT3d;
area = AREA(:,:,1);


% Export 2 had all of the zeros removed, and the carbon estemates
% increased by 1mol/m^2/yr or whatever the units are
export2 = export;
export2.CaCO3(find(export2.CaCO3 < 0)) = 0;
export2.opal(find(export2.opal < 0)) = 0;
export2.C(find(export2.C < 0)) = 0;
%export2.C = export2.C + 1; % commented out 03 aug 2016

export3.CaCO3 = export2.CaCO3 * CaI;
export3.opal = export2.opal * OI;
export3.C = export2.C;

% Calculate export percentages
f.CaCO3 = (export3.CaCO3*mass.CaCO3)./((export3.CaCO3*mass.CaCO3) + ...
                                       (export3.opal*mass.opal) + ...
                                       (export3.C*mass.C));

f.opal = (export3.opal*mass.opal)./((export3.CaCO3*mass.CaCO3) + ...
                                    (export3.opal*mass.opal) + ...
                                    (export3.C*mass.C));
f.C = (export3.C*mass.C)./((export3.CaCO3*mass.CaCO3) + ...
                           (export3.opal*mass.opal) + ...
                           (export3.C*mass.C));


rv = [component.rho_CaCO3, component.rho_opal, component.rho_POM];
rd = [component.rho_CaCO3, component.rho_opal, component.rho_POM *component.c2pom];
% flag! 16 Nov 2015, shoudl be rho_POM as well
% corrected upstream! Just make sure these component.rho_POM = component.dm_POM
%rd = [component.rho_CaCO3, component.rho_opal, component.rho_POM]; %
                                                                  % flag!

% how many layers in the seasonal "stuff" output
depth_thick = 10;
%maxdeep = 1500;
ztop = grid.zw(nplayers+1);
zvec_wc = 0:depth_thick:(maxdeep - ztop);
nz2 = length(zvec_wc);

[ny, nx, nt] = size(PSD);
teff1000_clim = nan(size(PSD));
stuff_clim.outW = nan(size(PSD));
stuff_clim.inW = nan(size(PSD));
stuff_clim.fpoc = nan(ny, nx, nz2, nt);

for t = 1:12
    [loc_teff1000 loc_stuff] =...
        calc_teff_type_17(grid,M3d,[0 Cr4 Cr4], [Q10 Q10 Q10], [KmOxy, KmOxy, KmOxy], mode, f.CaCO3, ...
                          f.opal ,alpha,PSD(:,:,t), ...
                          gamma , phys.temp(:,:,:,t),...
                          phys.pdens(:,:,:,t), phys.o2(:,:,:,t) ,nplayers, ...
                          component.rho_std, component.rho_water_std, ...
                          rv, rd, ds, dl, maxdeep, teff_depth);
    teff1000_clim(:,:,t) = loc_teff1000;
    stuff_clim.outW(:,:,t) = loc_stuff.outW;
    stuff_clim.inW(:,:,t) = loc_stuff.inW;
    stuff_clim.fpoc(:,:,:,t) = loc_stuff.fpoc;

end

    

%EX1000 = teff1000 .* EX; %% Left off here 19 Oct 2015
EX1000_clim = teff1000_clim.*EX;

teff_mask = ~isnan(teff1000_clim(:,:,1));

for seas = 1:12
    teff_mask = ~isnan(teff1000_clim(:,:,seas));
    teff_mask_seas(:,:,seas) = teff_mask;
    %total basin surface export for each season
    basin_EX_clim(seas,:) = basin_sum(teff_mask.*EX(:,:,seas).*area, ...
                                 R2d);
    basin_EX1000_clim(seas,:) = basin_sum(teff1000_clim(:,:,seas) .* EX(:,:,seas) ...
                                     .*area, R2d);
end

% teff_mask = ~isnan(teff1000);
% basin_EX = basin_sum(teff_mask.*EX.*area, R2d);
% basin_EX1000 = basin_sum(EX1000.*area, R2d);


%Divide total basin 1000m export by basins surface export
%bsns_fteff1000 = basin_EX1000./basin_EX;
bsns_fteff1000_clim = basin_EX1000_clim./basin_EX_clim;
% % Get annual averages
% bsns_fteff1000_an = squeeze(mean(bsns_fteff1000, 1));

basin_EX_an = sum(basin_EX_clim, 1);
basin_EX1000_an = sum(basin_EX1000_clim, 1);
bsns_fteff1000_an = basin_EX1000_an./basin_EX_an;

% % Package for export

% kitchen sink, for debugging
ks = v2struct(); 
