function fx = fval_fwteff_type_err_17(p, par2, obs_teffs, err_teffs)
% p: A vector of values for optimizing, see calc_fwteff_type17 for
% description of each
%
% par2: stuff I feed into calc_fwteff as par1
%
% obs_teffs: 
% observed transfer efficiencies, one for each basen;
% size nR
%
% err_teffs: error values from the diagnosed transfer efficiencies
% for each basin
% 
% Output fx: an adjusted f value, that is related to mean squared
% error (MSE)
%
% Calls: calc_fwteff_type17
% Called by juramia_teff_script
p
Cr4 = p(1);
Q = p(2);
CaI = p(3);
OI = p(4);
alpha = p(5);
gamma = p(6);
mf= p(7);
bf = p(8);
rhoPOM = p(9);
KmOxy = p(10);
mean_err = nanmean(err_teffs);
%keyboard
mod_teffs = calc_fwteff_type_17(Cr4, Q, CaI, OI, alpha, gamma, mf, bf, ...
                                rhoPOM, KmOxy, par2);
teff_tilde = (mod_teffs - obs_teffs)./err_teffs;
f = teff_tilde * teff_tilde';
fx = 0.5 * f;

% plot the output each iteration
clf
hold all
bar(obs_teffs, 'w')
plot(mod_teffs, 'o')

set(gca, 'XTick', 1:length(par2.regnames), 'XTickLabel', par2.regnames)
legend('Diagnosed', 'Ballasted', 'Location', ...
       'Best') 

