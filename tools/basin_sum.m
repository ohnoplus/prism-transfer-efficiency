function EXbsn = basin_sum(EXmap, R2d)
nbasins = max(R2d(:));
for loc_ocn = 1:nbasins
    loc_mask = (R2d == loc_ocn);
    loc_ibsn = find(R2d(:) == loc_ocn);
    bsn_all = EXmap(loc_ibsn);
    EXbsn(1,loc_ocn) = nansum(bsn_all);
end
