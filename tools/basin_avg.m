% Get average value accross entire basin, not flux weighted
% Output: Exbsn, average of whatever is in exmap for each region in
% R2d (nx*ny)
% Input: Exmap a 2d nx * ny map of some variable
% R2d: A map of the regions nx*ny
% EXbsn = basin_avg(EXmap, R2d)
function EXbsn = basin_avg(EXmap, R2d)
nbasins = max(R2d(:));
%keyboard
for loc_ocn = 1:nbasins
    loc_mask = (R2d == loc_ocn);
    loc_ibsn = find(R2d(:) == loc_ocn);
    bsn_all = EXmap(loc_ibsn);
    EXbsn(1,loc_ocn) = nanmean(bsn_all);
end
