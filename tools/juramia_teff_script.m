% A Juramia is the prehistoric ancestor to all mammals. Earlier
% versions of this script were named after dinosaurs.
% This script recreates the figures in Cram et al. (in prep). They
% are not necessarely in the same order here as one would find them
% in the paper.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% SETUP, load in paths and data%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
addpath('data')
addpath(genpath('tools'))
addpath('maincode')
% Surface PSD data from p16 transect
load('McDonnellPSD.mat') % p16psd
% chlorophyl climatology data
chl = load('chl_wgrid.mat');
load('juramia_data.mat')

nx = length(grid.xt);
ny = length(grid.yt);
regabr = {'AA', 'SAA', 'STA', 'STP', 'TA', 'TP', 'NA', 'NP'};

                                            
% weber et al. 2016 transfer efficiency estemates
frankteff = load('franken_teff_1k_2k.mat'); % method*region*depth
teff_obs = frankteff.output.TE(1, :, 1);
teff_err =  frankteff.output.TEerr(1,:,1);

%%
%% frankenexport (surface export according to Weber et al. 2016)

% frankex_clim = nan([size(grid.XT), 12]);

exportmaps = load('sat_Cexport_all_algorithms.mat');
franken_weight = load('franken_weight_map.mat');

%W * Cex is anual map

pre_franken_clim = permute(exportmaps.Cex_mon, [1 2 4 5 3]) .* repmat(franken_weight.W, 1, 1, ...
                                            1, 1, 12);

frankex_clim_woa = squeeze(sum(sum(pre_franken_clim, 3), 4));

for t = 1:12
    frankex_clim(:,:,t) = repaint_interp2(woachl.grid.XT, woachl.grid.YT, ...
                              frankex_clim_woa(:,:,t), grid.XT, ...
                                          grid.YT, M3d(:,:,1));
end

%% frankenexport but this time, missing values are treated as zeros
frankex_clim_woa_0 = frankex_clim_woa;
frankex_clim_woa_0(isnan(frankex_clim_woa_0)) = 0;

for t = 1:12
    frankex_clim_0(:,:,t) = repaint_interp2(woachl.grid.XT, woachl.grid.YT, ...
                              frankex_clim_woa_0(:,:,t), grid.XT, ...
                                          grid.YT, M3d(:,:,1));
end

%% Calculate particle size distribution map
% from chlorphyl concentraitons and the P16 UVP data.
% mcchlC is the chlophyl climatology measurment corresponding to each PSD measurment observed by
% McDonnell's group and their UVP
% The following block makes that list.
nL = length(p16psd.lat);
Ih = find(chl.grid.xt > 180);
Il = find(chl.grid.xt <= 180);
re_chl_clim = [chl.chl_clim(:,Ih,:),chl.chl_clim(:,Il,:)];
re_chl_lat = [chl.grid.xt(Ih)-360, chl.grid.xt(Il)];

for i = 1:nL
    [jnk, ccidx.lat(i,1)] = min(abs(chl.grid.yt - p16psd.lat(i)));
    [jnk, ccidx.lon(i,1)] = min(abs(re_chl_lat - ...
                                    p16psd.lon(i)));
    try
        mcchlC(i,1) = re_chl_clim(ccidx.lat(i,1), ccidx.lon(i,1), ...
                                  p16psd.mon(i));
    catch
        mcchlC(i,1) = NaN;
    end
end


% Linear model to estemate PSD from vgpm productivity.
findx = find(~(isnan(log10(mcchlC)) | isnan(p16psd.surf_beta)));

[mod4 smod4] = polyfit(log10(mcchlC(findx)), p16psd.surf_beta(findx), ...
                       1);
[mod4b smod4b] = fit(log10(mcchlC(findx)), p16psd.surf_beta(findx), ...
                     'poly1'); 


% extrapolate PSD from chlorophyl climatology
iocn = find(M3d(:)==1);
isurf = find(M3d(:,:,1)==1);
isurft = find(repmat(M3d(:,:,1), 1, 1, 12) == 1);

chl_clim_vec = pgrid_chl_clim(isurft);
chl_psd_vec = polyval(mod4, log10(chl_clim_vec));

mcchl_psd = nan(size(pgrid_chl_clim));
mcchl_psd(isurft) = chl_psd_vec;

PSD_map = -sum(mcchl_psd .* frankex_clim, 3)./...
    sum(frankex_clim, 3);

%% calculate and regrid sea water density and temperature
iocn = find(M3d(:)==1);
M4d = repmat(M3d, 1, 1, 1, 12);
iocn4 = find(M4d(:)==1);

grid4 = grid;
grid4.XT4d = repmat(grid.XT3d, 1, 1, 1, 12);
grid4.YT4d = repmat(grid.YT3d, 1, 1, 1, 12);
grid4.ZT4d = repmat(grid.ZT3d, 1, 1, 1, 12);


vecs4.lat = grid4.YT4d(iocn4);
vecs4.depth= grid4.ZT4d(iocn4);
vecs4.pres = sw_pres(vecs4.depth, vecs4.lat);
vecs4.salt = woaRe3.salt_mon(iocn4);
vecs4.temp = woaRe3.temp_mon(iocn4);
vecs4.pdens = sw_pden(vecs4.salt, vecs4.temp, vecs4.pres, 10.13);
pdens4d = nan(size(woaRe3.temp_mon));
pdens4d(iocn4) = vecs4.pdens/1000;

% and more bookkeeping
flux4D_0 = repmat(permute(frankex_clim,[ 1 2 4 3]), 1, 1, 24, 1);
% nan below sea floor
flux4D = nan(size(flux4D_0)); 
flux4D(iocn4) = flux4D_0(iocn4);

%% Ballast export
% these are austensibly molar ratios, according to CD
gfdl_file = 'gfdl_esm2m_hist_ballast.nc';
gfdl.info = ncinfo(gfdl_file);
gfdl.lon = ncread(gfdl_file, 'LON');
gfdl.lat = ncread(gfdl_file, 'LAT');

gfdl.C = ncread(gfdl_file, 'EXPC');
gfdl.si = ncread(gfdl_file, 'EXPSI');
gfdl.calc = ncread(gfdl_file, 'EXPCALC');
gfdl.arag = ncread(gfdl_file, 'EXPARAG');
gfdl.car = gfdl.calc + gfdl.arag;

gfdl.F.car = gfdl.car./gfdl.C;
gfdl.F.si = gfdl.si./gfdl.C;

[gfdl.grid.X2d, gfdl.grid.Y2d] = meshgrid(gfdl.lon, gfdl.lat);

gfdl_PF.car = repaint_interp2(gfdl.grid.X2d, gfdl.grid.Y2d, ...
                              gfdl.F.car', grid.XT, grid.YT, M3d(:,: ...
                                                  ,1));
gfdl_PF.si = repaint_interp2(gfdl.grid.X2d, gfdl.grid.Y2d, ...
                              gfdl.F.si', grid.XT, grid.YT, M3d(:,: ...
                                                  ,1));


%% Make the parJ object
% which is a multi field structure that is an input to fucntions
% and contains all of the data those functions need

% grid, same as in Devries
parJ.grid = grid;
% which grid cells are ocean and which are land
parJ.M3d = M3d;
% consider temperature effects, also don't break
parJ.mode = 4;
% the top three layers are not modeled because we focus on the mesopelegic
parJ.nplayers = 3;

%densities and similar
parJ.component.rho_opal = 2.09;
parJ.component.rho_CaCO3 = 2.83;
parJ.component.rho_POM = 1.033;
parJ.component.c2pom = 0.1; % fraction of POM that is actually
                           % carbon
parJ.component.rho_std = 1.23;
parJ.component.rho_water_std = 1.027;
% Since the top three grid cells are not used, we actually only go
% to 900m, which gets us close to 1000m actual
parJ.teff_depth = 900;
parJ.R2d = R2d;
parJ.regnames = regnames;
parJ.ds = 2;
parJ.dl = 4000;
parJ.EX = mean(frankex_clim_0, 3);

parJ.phys.temp = nansum(woaRe3.temp_mon .* flux4D, 4)./nansum(flux4D, ...
                                                  4);
parJ.phys.pdens = nansum(pdens4d .* flux4D, 4)./nansum(flux4D, ...
                                                  4);
parJ.phys.o2 = nansum(woaRe3.o2_mon .* flux4D, 4)./nansum(flux4D, ...
                                                  4);
% Note: Jacob, check that this doesn't require repainting
parJ.PSD = PSD_map;
parJ.export.CaCO3 = gfdl_PF.car;
parJ.export.opal= gfdl_PF.si;
parJ.export.C= ones(ny, nx);
parJ.mass.CaCO3 = 100.1;
parJ.mass.opal = 96.1;
parJ.mass.C = 12;

%% parJ objects in which some parameters do not vary
% for isolating the effects of particular variables
% temperature, density, and oxygen are constant, size still varies
parJ_flatBTDO = parJ;
parJ_flatBTDO.PSD = repmat(nanmean(parJ.PSD(:)), ...
                             size(parJ.PSD));
parJ_flatBTDO.phys.temp = repmat(nanmean(parJ.phys.temp(:)), ...
                             size(parJ.phys.temp));
parJ_flatBTDO.phys.pdens = repmat(nanmean(parJ.phys.pdens(:)), ...
                             size(parJ.phys.pdens));
parJ_flatBTDO.phys.o2 = repmat(nanmean(parJ.phys.o2(:)), ...
                             size(parJ.phys.o2));
% as above, but now density varies
parJ_flatBTO = parJ_flatBTDO;
parJ_flatBTO.phys.pdens = parJ.phys.pdens;
parJ_flatPSD = parJ;

% particle size distribution is the same everywhere
mean_PSD = nanmean(parJ.PSD(:));
parJ_flatPSD.PSD = repmat(mean_PSD, size(parJ.PSD));
% temperature and density are the same everywhere
parJ_flatTD = parJ;
parJ_flatTD.phys.temp = repmat(nanmean(parJ.phys.temp(:)), ...
                             size(parJ.phys.temp));
parJ_flatTD.phys.pdens = repmat(nanmean(parJ.phys.pdens(:)), ...
                             size(parJ.phys.pdens));
% ballast is the same everywhere
parJ_flatBal = parJ;
parJ_flatBal.export.CaCO3 = repmat(nanmean(parJ.export.CaCO3(:)), size(parJ.export.CaCO3));
parJ_flatBal.export.opal = repmat(nanmean(parJ.export.opal(:)), size(parJ.export.opal));


%% Whole basin flux profile PreCalculations
% for calculating basin average flux proiles
% a vector of depths
z = 1:10:5100;
% basin wide particle size distribtutions

basin_profiles.PSD = basin_avg(nanmean(parJ.PSD,3), R2d);
% basin wide silicate and carbonate exports 
things.si = basin_sum(parJ.export.opal .* parJ.EX, R2d)./ ...
    basin_sum(parJ.EX, R2d);
things.car =  basin_sum(parJ.export.CaCO3 .* parJ.EX, R2d)./ ...
    basin_sum(parJ.EX, R2d);
% basin wide mass percentages
things.mpsi = (things.si .* parJ.mass.opal) ./...
    ((things.si .*parJ.mass.opal) + ...
    (things.car .*parJ.mass.CaCO3) + ...
    (ones(size(things.si)) .*parJ.mass.C));
things.mpcar = (things.car .* parJ.mass.CaCO3) ./...
    ((things.si .*parJ.mass.opal) + ...
    (things.car .*parJ.mass.CaCO3) + ...
    (ones(size(things.si)) .*parJ.mass.C));

% global averages of the above
mean_things.si = nansum(parJ.export.opal(:) .* parJ.EX(:)) ./ ...
    nansum(parJ.EX(:));
mean_things.car = nansum(parJ.export.CaCO3(:) .* parJ.EX(:)) ./ ...
    nansum(parJ.EX(:));
mean_things.mpsi = (mean_things.si .* parJ.mass.opal) ./...
    ((mean_things.si .*parJ.mass.opal) + ...
    (mean_things.car .*parJ.mass.CaCO3) + ...
    (ones(size(mean_things.si)) .*parJ.mass.C));
mean_things.mpcar = (mean_things.car .* parJ.mass.CaCO3) ./...
    ((mean_things.si .*parJ.mass.opal) + ...
    (mean_things.car .*parJ.mass.CaCO3) + ...
    (ones(size(mean_things.si)) .*parJ.mass.C));


%%%%%%%%%%%%%%%%%%%%%%%%%%
%% setup for figure 1 %%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Shows variability in flux proviles when individual parameters
%% are varied
basin_profiles.T = basin_profile_avg(nanmean(parJ.phys.temp,4), grid, R3d, ...
                                     z);

basin_profiles.pdens = basin_profile_avg(nanmean(parJ.phys.pdens, 4), grid, R3d, ...
                                         z);
basin_profiles.O2 =  basin_profile_avg(nanmean(parJ.phys.o2, 4), grid, R3d, ...
                                         z);
Cr4 = 0.5;
Q10 = 2;
alpha = 2.3;
KmOxy = 10;
null_dens = repmat(mean(basin_profiles.pdens(:)), length(z), 1);
null_temp = repmat(mean(basin_profiles.T(:)), length(z), 1);
null_oxy =  repmat(mean(basin_profiles.O2(:)), length(z), 1);
null_si = mean(things.mpsi);
null_car = mean(things.mpcar); 

% standard particle size (um)
xmm = 20;
% standard partticle sinking speed (m/d)
wxmm = 2; 
% standar particle mass (arbitrary
mxmm = 0.004;
ds = 2;
dl = 4e3;
rho_std = parJ.component.rho_std;
rho_std1 = 1.092; %average particle mass
rho_water = 1.0250;
rho_POM = parJ.component.rho_POM;

stdPSD = 3.5;
%%
% a base flux profile, to which other profiles are compared
prof.base = pvsto_limw3(alpha, stdPSD, alpha-1, Cr4, ...
                       Q10, KmOxy, xmm, mxmm, wxmm, 4, ds, dl, ...
                       rho_std1,  rho_std1, rho_std1, ...
                       null_dens/1, rho_water, ...
                       1, null_temp, null_oxy, z, z);


 
ocn_abr = {'AA', 'STP'};
ocn_n = [1 4];

% flux profiles where I vary different parameters
% Viscosity + Density
Q10_null = 1;
KmOxy_null = 0;
for i = 1:2
    basinN = ocn_n(i)
    prof.visden.(ocn_abr{i}) = pvsto_limw3(alpha, stdPSD, alpha-1, Cr4, ...
                                          Q10_null, KmOxy_null, xmm, mxmm, wxmm, 4,ds, dl, ...
                                          rho_std1,  rho_std1, rho_std1, ...
                                          basin_profiles.pdens(:,basinN)/1, rho_water, ...
                                          1, basin_profiles.T(:,basinN), basin_profiles.O2(:,basinN), ...
                                          z, z); % 1.033
end

%  Q10 Only
Qos = {'Qo2', 'Qo3'};
Qvals = {2, 3};
for h = 1:2
    for i = 1:2
        basinN = ocn_n(i)
        prof.(Qos{h}).(ocn_abr{i}) = pvsto_limw3(alpha, stdPSD, alpha-1, Cr4, ...
                                                          Qvals{h}, KmOxy_null,xmm, mxmm, wxmm, 3,ds, dl, ...
                                                          rho_std1,  rho_std1, rho_std1, ...
                                                          null_dens/1, rho_water, ...
                                                          1, basin_profiles.T(:,basinN), basin_profiles.O2(:,basinN),...
                                                          z, z); % 1.033
    end
end

% Viscosity + Density + Q10
Qs = {'Q2', 'Q3'};
Qvals = {2, 3};
for h = 1:2
    for i = 1:2
        basinN = ocn_n(i)
        prof.(Qs{h}).(ocn_abr{i}) = pvsto_limw3(alpha, stdPSD, alpha-1, Cr4, ...
                                                          Qvals{h}, KmOxy_null,xmm, mxmm, wxmm, 4,ds, dl, ...
                                                          rho_std1,  rho_std1, rho_std1, ...
                                                          basin_profiles.pdens(:,basinN)/1, rho_water, ...
                                                          1, basin_profiles.T(:,basinN), basin_profiles.O2(:,basinN),...
                                                          z, z); % 1.033
    end
end

% PSD
bsn.PSD = basin_sum(parJ.PSD .* parJ.EX, R2d)./...
          basin_sum(parJ.EX, R2d);
psd_vec = [2.5 3.5 4.5 3 4]
psd_abr = {'x25', 'x35', 'x45', 'x30', 'x40'}

for i = 1:length(psd_vec)
    prof.PSD.(psd_abr{i}) = pvsto_limw3(alpha, psd_vec(i), alpha-1, Cr4, ...
                                       Q10, KmOxy, xmm, mxmm, wxmm, 4, ds, dl, ...
                                       rho_std1, rho_std1, rho_std1, ...
                                       null_dens/1, rho_water, ...
                                       1, null_temp, null_oxy, z, z);
end

%  Opal + Car
bsn.carFco = things.mpcar;
bsn.siFco =  things.mpsi;
bsn.CFco = 1-bsn.carFco - bsn.siFco;
for i = 1:2
    basinN = ocn_n(i);
    prof.bal.(ocn_abr{i}) = pvsto_limw3(alpha, stdPSD, alpha-1, [0 Cr4 Cr4], ...
                                       [Q10 Q10 Q10], repmat(KmOxy_null, 1, 3), xmm, mxmm, wxmm, 4, ds, dl, ...
                                       rho_std1, ...
                                       [parJ.component.rho_CaCO3,parJ.component.rho_opal,...
                        parJ.component.rho_POM],...
                                       [parJ.component.rho_CaCO3,parJ.component.rho_opal,...
                        parJ.component.rho_POM * parJ.component.c2pom],...
                                       null_dens/1, rho_water, ...
                                       [bsn.carFco(basinN), bsn.siFco(basinN),...
                        1-bsn.carFco(basinN)-bsn.siFco(basinN)],...
                                       null_temp, null_oxy, z, z);
end

prof.bal.Unbal = pvsto_limw3(alpha, stdPSD, alpha-1, [0 Cr4 Cr4], ...
                                       [Q10 Q10 Q10],repmat(KmOxy_null, 1, 3), xmm, mxmm, wxmm, 4, ds, dl, ...
                                       rho_std1, ...
                                       [parJ.component.rho_CaCO3,parJ.component.rho_opal,...
                        parJ.component.rho_POM],...
                                       [parJ.component.rho_CaCO3,parJ.component.rho_opal,...
                        parJ.component.rho_POM * parJ.component.c2pom],...
                                       null_dens/1, rho_water, ...
                                       [0 0 1],...
                                       null_temp, null_oxy, z, z);

% Oxygen
ocn_n_oxy = [1 4 6];
ocn_abr_oxy = regabr(ocn_n_oxy);
for i = 1:3
    basinN = ocn_n_oxy(i)
    prof.oxy.(ocn_abr_oxy{i}) = pvsto_limw3(alpha, stdPSD, alpha-1, Cr4, ...
                                          Q10_null, KmOxy, xmm, mxmm, wxmm, 4,ds, dl, ...
                                          rho_std1,  rho_std1, rho_std1, ...
                                          null_dens/1, rho_water, ...
                                          1, null_temp, basin_profiles.O2(:,basinN), ...
                                          z, z); % 1.033
end



%%%%%%%%%%%%%%%%%%%%
%% Plot Figure 1%%
%% Flux profiles%%
%%%%%%%%%%%%%%%%%%%%%%%
f1 = figure(1);
default_colors = get(groot,'DefaultAxesColorOrder');
los_linS = {'-', '--', ':', '-.'};
legtext = ['base', ocn_abr(1:2)];
legtext2 = [ocn_abr(1:2), 'Unbal'];

% converting numeric vectors to formatted cell arrays of strings
strs.PSD = {'2.5', '3.5', '4.5'};
strs.bal = {'Unbal', '50% Carbonate', '50% Silicate'};

strs.si = [{'0'}, cellfun(@(x)num2str(x, '%.2g'),num2cell(bsn.siFco(ocn_n)*100), ...
                   'UniformOutput', false)];
strs.car2 = [{'0'}, cellfun(@(x)num2str(x, '%.2g'),num2cell(bsn.carFco(ocn_n)*100), ...
                            'UniformOutput', false)];
% legend text variables
lt.si =  strcat(legtext, {'; si = '} , strs.si);
lt.car2 =  strcat(legtext, {'; CaCO3 = '} , strs.car2);
lt.bal =  [{'base'},strcat(legtext2, {'; CaCO3 = '} , strs.car2, {'%; Si = '} ...
                 , strs.si, {char(37)})];


% appending formatted arrays together
%lt.PSD = strcat( {'PSD = '} , strs.PSD);
lt.PSD = ['base', strcat({'PSD = '} , strs.PSD)];

lt.visc = strcat(['base', ocn_abr(1:2), ocn_abr(1:2)],...
                 {'', '; Q10 = 2', '; Q10 = 2', '; Q10 = 3', ['; Q10 ' ...
                    '= 3']});
%lt.bal = ['base', strs.bal];




% Viscosity + Density
subplot(2,3,1)
hold all;

set(gca, 'Ydir', 'reverse')
ylim([0 1000]); xlim([0 1])
xlabel('Normalized Flux'); ylabel('Depth')
plot(prof.base.FZ, z, 'Color', [0.8 0.8 0.8], 'LineWidth', 3)
for i = 1:2

    plot(prof.visden.(ocn_abr{i}).FZ, z, 'Color', 'k', 'linestyle', los_linS{i})
end

legend(['base', ocn_abr], 'Location', 'NorthWest')
set(gca, 'xscale', 'log'); xlim([0.05, 1]);
title('Viscosity + Density')

% Q10 Only
col_ls = {'k', 'k', [0, 0, 1], [0, 0, 1]};
los_linSS = {'-', '--', '-', '--'};

subplot(2,3, 2)
hold all;
set(gca, 'Ydir', 'reverse')
ylim([0 1000]); xlim([0 1])
xlabel('Normalized Flux'); ylabel('Depth')
plot(prof.base.FZ, z, 'Color', [0.8 0.8 0.8], 'LineWidth', 3)
iter = 0;
for h = 1:2
    for i = 1:2
        iter = iter + 1;
        plot(prof.(Qos{h}).(ocn_abr{i}).FZ, z, 'Color', col_ls{iter}, 'linestyle', los_linSS{iter})
    end
end
legend(lt.visc, 'Location', 'NorthWest')
title(['Biological Temperature Effect (Q10)'])
set(gca, 'xscale', 'log'); xlim([0.05, 1]);




% Viscosity + Density + Q10
subplot(2,3, 3)
hold all;
set(gca, 'Ydir', 'reverse')
ylim([0 1000]); xlim([0 1])
xlabel('Normalized Flux'); ylabel('Depth')
plot(prof.base.FZ, z, 'Color', [0.8 0.8 0.8], 'LineWidth', 3)
iter = 0;
for h = 1:2
    for i = 1:2
        iter = iter + 1;
        plot(prof.(Qs{h}).(ocn_abr{i}).FZ, z, 'Color', col_ls{iter}, 'linestyle', los_linSS{iter})
    end
end
legend(lt.visc, 'Location', 'NorthWest')
title(['Viscosity + Density + Q10'])
set(gca, 'xscale', 'log'); xlim([0.05, 1]);

% Oxygen
subplot(2,3,4)
hold all;
set(gca, 'Ydir', 'reverse')
ylim([0 1000]); xlim([0 1])
xlabel('Normalized Flux'); ylabel('Depth')
plot(prof.base.FZ, z, 'Color', [0.8 0.8 0.8], 'LineWidth', 3)
for i = 1:3

    plot(prof.oxy.(ocn_abr_oxy{i}).FZ, z, 'Color', 'k', 'linestyle', los_linS{i})
end

legend(['base', ocn_abr_oxy], 'Location', 'NorthWest')
set(gca, 'xscale', 'log'); xlim([0.05, 1]);
title('Oxygen')

% PSD
subplot(2,3,5)
hold all;
set(gca, 'Ydir', 'reverse')
ylim([0 1000]); xlim([0 1])
xlabel('Normalized Flux'); ylabel('Depth')
plot(prof.base.FZ, z, 'Color', [0.8 0.8 0.8], 'LineWidth', 3)
for i = 1:3
    plot(prof.PSD.(psd_abr{i}).FZ, z, 'Color', 'k', 'linestyle', los_linS{i})
end
legend(lt.PSD, 'Location', 'NorthWest')
set(gca, 'xscale', 'log'); xlim([0.05, 1]);
title('Particle Size Distribution')

% ballast
 bal_abr = [ ocn_abr, {'Unbal'}];
subplot(2,3,6)
hold all;
set(gca, 'Ydir', 'reverse')
ylim([0 1000]); xlim([0 1])
xlabel('Normalized Flux'); ylabel('Depth')
plot(prof.base.FZ, z, 'Color', [0.8 0.8 0.8], 'LineWidth', 3)
for i = 1:3
    plot(prof.bal.(bal_abr{i}).FZ(:,3), z, 'Color', 'k', 'linestyle', los_linS{i})
end
legend(lt.bal, 'Location', 'NorthWest')
set(gca, 'xscale', 'log'); xlim([0.05, 1]);
title('Ballast')



set(gcf,'PaperUnits','inches','PaperPosition',[0 0 14 8])
print('plots/ProtoProfiles' , '-dsvg')
print('plots/ProtoProfiles' , '-depsc')

%% Figure 1 insets
figure(11)
clf
set(gcf,'PaperUnits','inches','PaperPosition',[0 0 12 4])
subplot(1,3,1)
hold all;
set(gca, 'Ydir', 'reverse')
ylim([0 1000]);
xlabel('Temperature'); ylabel('Depth')
plot(repmat(4, length(z), 1), z, 'Color', [0.9 0.9 0.9], 'LineWidth', 3)
for i = 1:2
    plot(basin_profiles.T(:,ocn_n(i)), z, 'Color', 'k', 'linestyle', los_linS{i})
end
legend(['base', ocn_abr], 'Location', 'NorthWest')

subplot(1,3,2)
hold all;
set(gca, 'Ydir', 'reverse')
ylim([0 1000]);
xlabel('Potential Density kg/m^3'); ylabel('Depth')
plot(null_dens, z, 'Color', [0.9 0.9 0.9], 'LineWidth', 3)
for i = 1:2
    plot(basin_profiles.pdens(:,ocn_n(i)), z, 'Color', 'k', 'linestyle', los_linS{i})
end
legend(['base', ocn_abr], 'Location', 'NorthWest')

subplot(1,3,3)
hold all;
set(gca, 'Ydir', 'reverse')
ylim([0 1000]);
xlabel('Oxygen uM'); ylabel('Depth')
plot(null_oxy, z, 'Color', [0.9 0.9 0.9], 'LineWidth', 3)
for i = 1:3
    plot(basin_profiles.O2(:,ocn_n_oxy(i)), z, 'Color', 'k', 'linestyle', los_linS{i})
end
legend(['base', ocn_abr_oxy], 'Location', 'NorthWest')


set(gcf,'PaperUnits','inches','PaperPosition',[0 0 4 4])
print('plots/ProfileAddOns' , '-dsvg')
print('plots/ProfileAddOns' , '-depsc')

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Back of the Envelope Calculations
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% These aren't any particlular figure, but I do refer to them in
%% the text.
% for particle density
things.vpsi = things.mpsi./ parJ.component.rho_opal./...
    sum(things.mpsi ./ parJ.component.rho_opal + ...
        things.mpcar ./ parJ.component.rho_CaCO3 + ...
        ((1 - things.mpsi - things.mpcar) ./...
         parJ.component.rho_POM .* parJ.component.c2pom));
things.vpcar = things.mpcar ./ parJ.component.rho_CaCO3./...
    sum(things.mpsi ./ parJ.component.rho_opal + ...
        things.mpcar ./ parJ.component.rho_CaCO3 + ...
        ((1 - things.mpsi - things.mpcar) ./...
         parJ.component.rho_POM .* parJ.component.c2pom));
things.dens = things.vpsi .* parJ.component.rho_opal + ...
    things.vpcar .* parJ.component.rho_CaCO3 + ...
    (1 - things.vpsi - things. vpcar) .*  parJ.component.rho_POM;
things.reldens = things.dens - 1.025;
% difference in sinking speed between AA and STP as a result of
% ballast
things.reldens(1)/things.reldens(4)

.33 * parAm6.mass.opal

avPSD = mean(basin_profiles.PSD);

%% particle sinking speed back of the envelope calculations
%back of the envelope surface sinking speed as a function of size
prof.PSD.x35.W(1)./prof.PSD.x45.W(1)
prof.PSD.x25.W(1)./prof.PSD.x35.W(1)

% sinking speeds converge with depth, when the differences are
% caused by particle size
figure;
clf; subplot(121);
plot(prof.PSD.x30.W, -z)
hold all;
plot(prof.PSD.x40.W, -z) 
set(gca, 'yscale', 'log')
ylabel('Depth (m)');
xlabel('Sinking Speed (m/d)')
legend('Beta = 3', 'Beta = 4', 'Location', 'Best')
subplot(122);
plot(prof.PSD.x30.W./prof.PSD.x40.W, -z)
ylabel('Depth');
ylabel('Sinking Speed Ratio Beta = 0.3/ Beta = 0.4')
set(gca, 'yscale', 'log')

%%%%%%%%%%%%%%%%%%%%%%
%% Figure 2 Contours%%
%%%%%%%%%%%%%%%%%%%%%%


%% Pre work
%% make flux profiles accross a range of binned temperature and
%% oxygen values
% temperature profiles are the average of all grid cells in which
% surface temperature falls within a certain range.

% surface temperatures
surt_mat = parAm6.phys.temp(:,:,1);
surt_vec = surt_mat(isurf); 
[surt_b, surt_i] = sort(surt_vec);

% surface oxygens, unused
suro_mat = parAm6.phys.o2(:,:,1);
suro_vec = suro_mat(isurf);
[suro_b, suro_i] = sort(suro_vec);

% oxygen minima
lowo_mat = min(parAm6.phys.o2, [], 3);
lowo_vec = lowo_mat(isurf);
[lowo_b, lowo_i] = sort(lowo_vec);

[nx ny nz] = size(parAm6.phys.temp); 

% sort temperature and density by surface temperature, and oxygen
% by minimum oxygen
t_unsorted = nan(length(isurf), nz);
t_sorted =  nan(length(isurf), nz);
pd_unsorted = nan(length(isurf), nz);
pd_sorted =  nan(length(isurf), nz);
o_unsorted = nan(length(isurf), nz);
o_sorted =  nan(length(isurf), nz);

for zidx = 1:nz
    t_mat_loc = parAm6.phys.temp(:,:,zidx);
    t_unsorted(:,zidx) = t_mat_loc(isurf);
    t_sorted(:,zidx) = t_unsorted(surt_i,zidx);
    % note that pd indexes on the order of temperature
    pd_mat_loc = parAm6.phys.pdens(:,:,zidx);
    pd_unsorted(:,zidx) = pd_mat_loc(isurf);
    pd_sorted(:,zidx) = pd_unsorted(surt_i, zidx);
    % but oxygen does not
    o_mat_loc = parAm6.phys.o2(:,:,zidx);
    o_unsorted(:,zidx) = o_mat_loc(isurf);
    o_sorted(:,zidx) = o_unsorted(lowo_i, zidx);
    
end

% bin by degree or oxygen ppm (I recall this being easier last time, but can't
% find it)

lt =  floor(t_sorted(1,1));
ht = ceil(t_sorted(end,1));
lo = floor(min(o_sorted(1,:)));
ho = ceil(min(o_sorted(end,:)));

% temperature and density
tbvec = lt:1:ht-1;
tbin = nan(nz, length(tbvec));
pdbin = nan(nz, length(tbvec));
nT = length(tbvec);
for tidx = 1:nT;
    t = tbvec(tidx);
    loc_sidx = find(surt_b >= t & surt_b < t+1);
    tbin(:,tidx) = nanmean(t_sorted(loc_sidx,:),1);
    pdbin(:,tidx) = nanmean(pd_sorted(loc_sidx,:),1);
end

% oxygen
obvec = lo:1:ho;
nO = length(obvec);
obin = nan(nz, nO);
for oidx = 1:nO;
    o = obvec(oidx);
    loc_osidx = find(lowo_b >= o & lowo_b < o+1);
    obin(:,oidx) = nanmean(o_sorted(loc_osidx, :), 1);
end


% interpolate all profiles
% note: basin_profile_avg calls grid.zw, rather than grid.zt, I
% think the latter is more appropriate
binned_profiles.T = nan(length(z), nT);
binned_profiles.PD = nan(length(z), nT);
for tidx = 1:nT
    binned_profiles.T(:,tidx) = spline(grid.zt, tbin(:,tidx), z);
    binned_profiles.PD(:,tidx) = spline(grid.zt, pdbin(:,tidx), z);
end
binned_profiles.O = nan(length(z), nO);
for oidx = 1:nO-1
    try
    binned_profiles.O(:,oidx) = spline(grid.zt, obin(:,oidx), z);
    end
end

range.Q10 = 1:0.1:4;
range.siF = [0:.1:0.9, .91:0.02:0.99, 0.999, 0.9999]
range.beta2 = 0:0.2:6;
range.alpha = 1:.1:3;
range.T = -2:1:25;
%range.O = unique([2:1:5,5:5:30,30:10:100,100:25:200]);
range.O = unique([2:1:10,5:5:100,100:25:200]);


% O vs T
basinN = 4;
ot_teffs = nan(length(range.O), length(range.T));
tic
for t = 1:length(range.T)
    t
    loc_T = range.T(t);
    tprof_idx = find(tbvec == loc_T); 
    tprof_loc = binned_profiles.T(:,tprof_idx);
    pdprof_loc = binned_profiles.PD(:,tprof_idx);
    for o = 1:length(range.O)
        o
        loc_O = range.O(o);
        oprof_idx = find(obvec == loc_O);
        oprof_loc = binned_profiles.O(:,oprof_idx);
        loc_sol = pvsto_limw3(alpha,stdPSD , alpha-1, [0 Cr4 Cr4], ...
                              repmat(Q10, 1,3), repmat(KmOxy,1,3),...
                              xmm, mxmm, wxmm,...
                              4,2, 4e3, ...
                              rho_std, ...
                              [parAm6.component.rho_CaCO3,parAm6.component.rho_opal,...
                            parAm6.component.rho_POM], ...
                              [parAm6.component.rho_CaCO3,parAm6.component.rho_opal,...
                            (parAm6.component.rho_POM)* parAm6.component.c2pom], ...
                              pdprof_loc, rho_water, ...
                              [bsn.carFco(basinN), bsn.siFco(basinN), bsn.CFco(basinN)],...
                              tprof_loc,oprof_loc, z, ...
                              1000);
        loc_teff = loc_sol.F(3);
        ot_teffs(o,t) = loc_teff;
    end
end
toc

% beta vs T (Q10 = 2)
tb_teffs = nan(length(range.beta3), length(range.T)); 
for t = 1:length(range.T)
    t
    loc_T = range.T(t);
    tprof_idx = find(tbvec == loc_T); 
    tprof_loc = binned_profiles.T(:,tprof_idx);
    pdprof_loc = binned_profiles.PD(:,tprof_idx);
    for b = 1:length(range.beta3)
        b
        loc_beta = range.beta3(b);
        loc_sol = pvsto_limw3(alpha, loc_beta, alpha-1, [0 Cr4 Cr4], ...
                              repmat(Q10, 1,3), repmat(KmOxy,1,3),...
                              xmm, mxmm, wxmm,...
                              4,2, 4e3, ...
                              rho_std, ...
                              [parAm6.component.rho_CaCO3,parAm6.component.rho_opal,...
                            parAm6.component.rho_POM], ...
                              [parAm6.component.rho_CaCO3,parAm6.component.rho_opal,...
                            (parAm6.component.rho_POM)* parAm6.component.c2pom], ...
                              pdprof_loc, rho_water, ...
                              [bsn.carFco(basinN), bsn.siFco(basinN), bsn.CFco(basinN)],...
                              tprof_loc,null_oxy, z, ...
                              1000);
        loc_teff = loc_sol.F(3);
        tb_teffs(b,t) = loc_teff;
    end
end

        

% beta vs %Si
sib_teffs = nan(length(range.beta3), length(range.siF));
for si = 1:length(range.siF)
    si
    loc_si = range.siF(si)
    for b = 1:length(range.beta3)
        b
        loc_beta = range.beta3(b);
        loc_sol = pvsto_limw3(alpha, loc_beta, alpha-1, [0 Cr4 Cr4], ...
                              repmat(Q10, 1,3), repmat(KmOxy,1,3),...
                              xmm, mxmm, wxmm,...
                              4,2, 4e3, ...
                              rho_std, ...
                              [parAm6.component.rho_CaCO3,parAm6.component.rho_opal,...
                            parAm6.component.rho_POM], ...
                              [parAm6.component.rho_CaCO3,parAm6.component.rho_opal,...
                            (parAm6.component.rho_POM)* parAm6.component.c2pom], ...
                              null_dens, rho_water, ...
                              [0, loc_si, 1 - loc_si],...
                             null_temp,null_oxy, z, ...
                              1000);
        loc_teff = loc_sol.F(3);
        sib_teffs(b, si) = loc_teff;
    end
end



%% plot
% O vs T
f2 = figure(2);
set(gcf,'PaperUnits','inches','PaperPosition',[0 0 4 8])
set(f2, 'Color', [1 1 1])
jcontour_vec = [0.001, 0.01,.1:.1:1];
jcontour_vec2 = [.1:.025:.5];
jcontour_vec3 = [.37:.001:.38];
jcontour_vec4 = [0.1:0.05:0.5];


subplot(223)
[C, h] = contour( range.T,range.O', ot_teffs, jcontour_vec4, '-k');
ylabel('Oxygen'); xlabel('Temperature (Q10 = 2)')
clabel(C,h);
set(gca, 'yscale', 'log')

subplot(221)
[C, h] = contour( range.T,range.beta3', tb_teffs, jcontour_vec4, '-k');
ylabel('\beta'); xlabel('Temperature (Q10 = 2)')
clabel(C,h);
%set(gca, 'yscale', 'log')

subplot(222)
[C, h] = contour( range.siF,range.beta3', sib_teffs, jcontour_vec, '-k');
ylabel('\beta'); xlabel('Silicate Mass Percent')
clabel(C,h);

print('Plots6/contour' , '-dpng')
export_fig('Plots6/contour' , '-eps')

%% Figure S1 Input Data
%% Region map with surface export
f3 = figure(3)
set(f3, 'units','normalized','outerposition',[0 0 1 1],...
        'Color', [1 1 1])

% Contours m_map
nreg = length(regnames);
land = 1- M3d(:,:,1);

clf
lon = grid.xt;
lat = grid.yt;
%map = log10(nanmean(parJ.EX,3));
map = log10(parJ.EX);
map(~isfinite(map)) = NaN;

lon2 = reshape(lon, length(lon), 1);
lat2 = reshape(lat, length(lat), 1);

shift_by = 170;
%shift_by = 180;
dx_plot=60;
dy_plot=30;
lon_shift = circshift(lon2,[shift_by 0]);
dlon = diff(lon_shift);
idx = find(dlon<0,1,'first');
lon_shift(idx+1:end) = lon_shift(idx+1:end) + 360.;

map_shift = circshift(map,[0 shift_by]);
R2d_shift = circshift(R2d,[0 shift_by]);
land_shift = circshift(land, [0 shift_by]);

m_proj('moll','lon',[min(lon_shift) max(lon_shift)])
a = m_pcolor(lon_shift,lat2',map_shift);shading('flat');
c = colorbar;
v = caxis;
%b = m_contour(lon_shift,lat2',R2d_shift)
hold all
for i = 1:nreg
    RR = double(R2d_shift==i);
    RR(land_shift==1) = NaN;
    [cc,hh] = m_contour(lon_shift,lat2',RR,[.5 .5],'k','linewidth',1);
end
caxis(v)
caxis([log10(2), log10(50)])
logTick = c.Ticks;
newTick = cellfun(@(x)num2str(10.^x, '%.1f'), num2cell(logTick), ...
                  'UniformOutput', false)
set(c, 'YTickLabel', newTick, 'FontSize', 12)

colormap jet;
% m_grid('box','fancy','xtick',[-360:dx_plot:360],'ytick',[-90: ...
%                     dy_plot:90],'fontsize',8, 'xaxislocation',
%                     'middle');
m_grid('xtick',[-360:dx_plot:360],'ytick',[-90: ...
                    dy_plot:90],'fontsize',8, 'xaxislocation', 'middle');
m_coast('patch',[.9 .9 .9]);

set(gcf,'PaperUnits','inches','PaperPosition',[0 0 16 8])
print('plots/exportregion' , '-dpng')
export_fig('plots/exportregion' , '-eps')

%% additional input data figures actually have to be run after the
%% code calculating transfer efficiency. This is because the
%% transfer efficinecy code also runs the density calculations,
%% which go in these input figures.



%% Calculations for estemating transfer efficiency
%% Give each of these a few days to a week to run on a 12 node
%% computer with 2016 technology.
options = optimset('Display','iter', 'TolX', 5e-2);
options1 = options;
options1.TolX = 0.05;
options.TolFun = 0.0001;
options.TolX = 1;


[CrsJtoptall.TObBa fvalsJ16toptall.TObBa] = fminsearchbnd(@(p)fval_fwteff_type_err_16([p(1)/100, ...
                    p(2), 1, 1, 2.3, 1, 0, 1.033, p(3)], parJ, teff_obs, teff_err), ...
                                              [135, 2.5, 10],...
                                              [0 1 0], [500 4 25], ...
                                              options1);
[CrsJtoptall.TOb fvalsJtoptall.TOb] = fminsearchbnd(@(p)fval_fwteff_type_err_16([p(1)/100, ...
                    CrsJtoptall.TObBa(2), 1, 1, 2.3, 1, 0, 1.033, CrsJtoptall.TObBa(3)], parAm6_flatBal, teff_obs, teff_err), ...
                                              [CrsJtoptall.TObBa(1)],...
                                               [0], [500], ...
                                              options);

[CrsJtoptall.TOBa fvalsJ16toptall.TOBa] = fminsearchbnd(@(p)fval_fwteff_type_err_16([p(1)/100, ...
                    CrsJtoptall.TObBa(2), 1, 1, 2.3, 0, 0, 1.033, CrsJtoptall.TObBa(3)], parJ, teff_obs, teff_err), ...
                                              [CrsJtoptall.TObBa(1)],...
                                              [0], [500], ...
                                              options);
[CrsJtoptall.TbBa fvalsJ16toptall.TbBa] = fminsearchbnd(@(p)fval_fwteff_type_err_16([p(1)/100, ...
                    CrsJtoptall.TObBa(2), 1, 1, 2.3, 1, 0, 1.033, 0], parJ, teff_obs, teff_err), ...
                                              [CrsJtoptall.TObBa(1)],...
                                              [0], [500], ...
                                              options);
[CrsJtoptall.ObBa fvalsJ16toptall.ObBa] = fminsearchbnd(@(p)fval_fwteff_type_err_16([p(1)/100, ...
                    2, 1, 1, 2.3, 1, 0, 1.033, CrsJtoptall.TObBa(3)], parJ_flatTD, teff_obs, teff_err), ...
                                              [CrsJtoptall.TObBa(1)],...
                                              [0], [500], ...
                                              options);

%% Alternatively, same time and electricity by simply re-loading
%% these previously calculated values.
load('CrsAm616toptall.mat')
CrsJtoptall = CrsAm616toptall;
fvalsJtoptall = fvalsAm616toptall;


%% Workup
%% Optimized TEFF workup
% these produce the basin averages, and maps of the input
% data. Each of these should take a little under an hour to run on
% a 12 node processor (assuming you actually use all the nodes).


%% these two iterations were for debugging, you don't need to run
%% them to make the figures in the paper
% [bsnsJAtoptall.TObBa, mapJAtoptall.TObBa, stuffJAtoptall.TObBa, ksJ.TObBa] = calc_fwteff_type_16(CrsJtoptall.TObBa(1)/100, ...
%                                                   CrsJtoptall.TObBa(2),...
%                                                   1,...
%                                                   1,...
%                                                   2.3,...
%                                                   1, ...
%                                                   0, 1.033,CrsJtoptall.TObBa(3), ...
%                                                   parAm6);
% parJ6 = parJ;
% parJ6.EX = parAm6.EX;
% [bsnsJ6toptall.TObBa, mapJ6toptall.TObBa, stuffJ6toptall.TObBa, ksJ.TObBa] = calc_fwteff_type_16(CrsJtoptall.TObBa(1)/100, ...
%                                                   CrsJtoptall.TObBa(2),...
%                                                   1,...
%                                                   1,...
%                                                   2.3,...
%                                                   1, ...
%                                                   0, 1.033,CrsJtoptall.TObBa(3), ...
%                                                   parJ6);

%% main

% Temperature + Oxygen + Size + Ballast
[bsnsJtoptall.TObBa, mapJtoptall.TObBa, stuffJtoptall.TObBa, ksJ.TObBa] = calc_fwteff_type_16(CrsJtoptall.TObBa(1)/100, ...
                                                  CrsJtoptall.TObBa(2),...
                                                  1,...
                                                  1,...
                                                  2.3,...
                                                  1, ...
                                                  0, 1.033,CrsJtoptall.TObBa(3), ...
                                                  parJ);
% Temperature + Oxygen + Size
[bsnsJtoptall.TOb, mapJtoptall.TOb, stuffJtoptall.TOb] = calc_fwteff_type_16(CrsJtoptall.TOb(1)/100, ...
                                                  CrsJtoptall.TObBa(2),...
                                                  1,...
                                                  1,...
                                                  2.3,...
                                                  1, ...
                                                  0, 1.033, CrsJtoptall.TObBa(3), ...
                                                  parJ_flatBal);

[bsnsJtoptall.TOBa, mapJtoptall.TOBa, stuffJtoptall.TOBa] = calc_fwteff_type_16(CrsJtoptall.TOBa(1)/100, ...
                                                  CrsJtoptall.TObBa(2),...
                                                  1,...
                                                  1,...
                                                  2.3,...
                                                  0, ...
                                                  0, 1.033,CrsJtoptall.TObBa(3), ...
                                                  parJ);
[bsnsJtoptall.TbBa, mapJtoptall.TbBa, stuffJtoptall.TbBa] = calc_fwteff_type_16(CrsJtoptall.TbBa(1)/100, ...
                                                  CrsJtoptall.TObBa(2),...
                                                  1,...
                                                  1,...
                                                  2.3,...
                                                  1, ...
                                                  0, 1.033,0, ...
                                                  parJ);

[bsnsJtoptall.ObBa, mapJtoptall.ObBa, stuffJtoptall.ObBa] = calc_fwteff_type_16(CrsJtoptall.ObBa(1)/100, ...
                                                  2,...
                                                  1,...
                                                  1,...
                                                  2.3,...
                                                  1, ...
                                                  0, 1.033,10, ...
                                                  parJ_flatTD);

figure
clf; hold all;
a = bar(teff_obs, 'w');
errorbar(teff_obs, teff_err, '.', 'color', [0.7 0.7 0.7]);
b = plot([1:8] + 0.05, bsnsJtoptall.TObBa, ':o', 'MarkerFaceColor', [.94, 0.50, 0.50],...
         'MarkerEdgeColor', [0, 0, 0],'MarkerSize', 10);
set(gca, 'XTick', 1:length(regabr), 'XTickLabel', regabr);
c = plot([1:8], bsnsJtoptall.ObBa, '^',...
         'MarkerFaceColor', [0, 1, 1], ...
         'MarkerEdgeColor', [0, 0, 0], 'MarkerSize', 10);
d = plot([1:8] - 0.05, bsnsJtoptall.TbBa, 'd', 'MarkerFaceColor', [.5 1 0],...
         'MarkerEdgeColor', [0, 0, 0],'MarkerSize', 10);
e = plot([1:8] - 0.1, bsnsJtoptall.TOBa, 's',  'MarkerFaceColor', [0.2 0.2 1],...
         'MarkerEdgeColor', [0, 0, 0],'MarkerSize', 10);
f = plot([1:8] - 0.1, bsnsJtoptall.TOb, '>',  'MarkerFaceColor', [1 1 1],...
         'MarkerEdgeColor', [0, 0, 0],'MarkerSize', 10);
% g = plot([1:8] - 0.1, bsnsJtoptall.TO, '+',  'MarkerFaceColor', [1 1 1],...
%          'MarkerEdgeColor', [0, 0, 0],'MarkerSize', 10);



ylim([0 Inf]);
legend(...
    [b c d e f],...

    sprintf('T + O + \\beta + Bal; aMSE = %1.2f', fvalsJtoptall.TObBa/4),...
    sprintf('O +\\beta + Bal; aMSE = %1.2f',fvalsJtoptall.ObBa/4),...
    sprintf('T +\\beta + Bal; aMSE = %1.2f',fvalsJtoptall.TbBa/4),...
    sprintf('T + O + Bal; aMSE = %1.2f',fvalsJtoptall.TOBa/4),...
    sprintf('T + O +\\beta; aMSE = %1.2f',fvalsJtoptall.TOb/4),...
% sprintf('T + O; aMSE = %1.2f',fvalsJtoptall.TO),...
    'Location', 'North')
title(sprintf('Q_{10} = %1.1f, K_{mOxy} = %1.1f',...
              CrsJtoptall.TObBa(2),...
              CrsJtoptall.TObBa(3)))

set(gcf, 'units','normalized','outerposition',[0 0 1 1],...
        'Color', [1 1 1])
set(gcf,'PaperUnits','inches','PaperPosition',[0 0 16 8])
export_fig('plots/barleaveoneout', '-eps')

%% Global Map of TEFF
%%
figure
jm_pcolor(grid.xt, grid.yt, mapJtoptall.TObBa);
colormap(lowjet)
set(gcf, 'units','normalized','outerposition',[0 0 1 1],...
        'Color', [1 1 1])
set(gcf,'PaperUnits','inches','PaperPosition',[0 0 16 8])
export_fig('plots/teffmap', '-eps')
%caxis([0 0.5])

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Reminerilization Length Scales
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%  A few extras for reminerilization length scale
paJset.null = parJ;
mean_PSD = nanmean(parJ.PSD(:));
paJset.null.PSD = repmat(mean_PSD, size(parJ.PSD));
paJset.null.export.CaCO3 = repmat(nanmean(parJ.export.CaCO3(:)), ...
                                          size(parJ.export ...
                                               .CaCO3));
paJset.null.export.opal = repmat(nanmean(parJ.export.opal(:)), ...
                                          size(parJ.export ...
                                               .opal));
paJset.null.phys.temp = repmat(nanmean(parJ.phys.temp(:)), ...
                                          size(parJ.phys.temp));
paJset.null.phys.pdens = repmat(nanmean(parJ.phys.pdens(:)), ...
                                          size(parJ.phys.pdens));
paJset.null.phys.o2 = repmat(nanmean(parJ.phys.o2(:)), ...
                                          size(parJ.phys.o2));





% Biological temperature effect
paJset.bioT = paJset.null;
paJset.bioT.phys.temp = parJ.phys.temp;
paJset.bioT.mode = 3;

% viscosity effect
paJset.visT = paJset.bioT;
paJset.visT.mode = 2;

% density effect
paJset.denT = paJset.null;
paJset.denT.phys.pdens = parJ.phys.pdens;

% oxygen effect
paJset.O = paJset.null;
paJset.O.phys.o2 = parJ.phys.o2;

% ballast effect
paJset.bal = paJset.null;
paJset.bal.export = parJ.export;

% silicate ballast
paJset.si = paJset.null;
paJset.si.export.opal = parJ.export.opal;

% carbonate export
paJset.car = paJset.null;
paJset.car.export.CaCO3 = parJ.export.CaCO3;

% size effect
paJset.size = paJset.null;
paJset.size.PSD = parJ.PSD;
%% Additional calculations
[bsnsJo.null, mapJo.null, stuffJ.null, ksJ.null] = calc_fwteff_type_16(CrsJoptall.TObBa(1)/100, ...
                                                  2,...
                                                  1,...
                                                  1,...
                                                  2.3,...
                                                  0, ...
                                                  0, 1.033,0, ...
                                                  paJset.null);

% calculations
% All
[bsnsJo.all, mapJo.all, stuffJo.all, ksJo.all] = calc_fwteff_type_16(CrsJoptall.TObBa(1)/100, ...
                                                  2,...
                                                  1,...
                                                  1,...
                                                  2.3,...
                                                  1, ...
                                                  0, 1.033,10, ...
                                                  parJ);


% Biological temperature effect
[bsnsJo.bioT, mapJo.bioT, stuffJo.bioT, ksJo.bioT] = calc_fwteff_type_16(CrsJoptall.TObBa(1)/100, ...
                                                  2,...
                                                  1,...
                                                  1,...
                                                  2.3,...
                                                  1, ...
                                                  0, 1.033,10, ...
                                                  paJset.bioT);
% viscosity effect
[bsnsJo.visT, mapJo.visT, stuffJo.visT, ksJo.visT] = calc_fwteff_type_16(CrsJoptall.TObBa(1)/100, ...
                                                  2,...
                                                  1,...
                                                  1,...
                                                  2.3,...
                                                  1, ...
                                                  0, 1.033,10, ...
                                                  paJset.visT);
% density effect
[bsnsJo.denT, mapJo.denT, stuffJo.denT, ksJo.denT] = calc_fwteff_type_16(CrsJoptall.TObBa(1)/100, ...
                                                  2,...
                                                  1,...
                                                  1,...
                                                  2.3,...
                                                  1, ...
                                                  0, 1.033,10, ...
                                                  paJset.denT);
% oxygen effect
[bsnsJo.O, mapJo.O, stuffJo.O, ksJo.O] = calc_fwteff_type_16(CrsJoptall.TObBa(1)/100, ...
                                                  2,...
                                                  1,...
                                                  1,...
                                                  2.3,...
                                                  1, ...
                                                  0, 1.033,10, ...
                                                  paJset.O);
% ballast effect
[bsnsJo.bal, mapJo.bal, stuffJo.bal, ksJo.bal] = calc_fwteff_type_16(CrsJoptall.TObBa(1)/100, ...
                                                  2,...
                                                  1,...
                                                  1,...
                                                  2.3,...
                                                  1, ...
                                                  0, 1.033,10, ...
                                                  paJset.bal);

% Carbonate effect
[bsnsJo.car, mapJo.car, stuffJo.car, ksJo.car] = calc_fwteff_type_16(CrsJoptall.TObBa(1)/100, ...
                                                  2,...
                                                  1,...
                                                  1,...
                                                  2.3,...
                                                  1, ...
                                                  0, 1.033,10, ...
                                                  paJset.car);

% Silicate effect
[bsnsJo.si, mapJo.si, stuffJo.si, ksJo.si] = calc_fwteff_type_16(CrsJoptall.TObBa(1)/100, ...
                                                  2,...
                                                  1,...
                                                  1,...
                                                  2.3,...
                                                  1, ...
                                                  0, 1.033,10, ...
                                                  paJset.si);

% size effect
[bsnsJo.size, mapJo.size, stuffJo.size, ksJo.size] = calc_fwteff_type_16(CrsJoptall.TObBa(1)/100, ...
                                                  2,...
                                                  1,...
                                                  1,...
                                                  2.3,...
                                                  1, ...
                                                  0, 1.033,10, ...
                                                  paJset.size);

%% Reminerilizaton Length Plots
figure
clf
subplot(131)
hold all
plot(nanmean(stuffJo.all.W200,2)./...
     nanmean(stuffJo.all.crz200,2),...
     grid.yt, '-r')

xlabel(' Remineralization length scale (m)')
ylabel('Latitude')
title('Remineralization Effects')

plot(nanmean(stuffJo.bioT.W200,2)./...
     nanmean(stuffJo.bioT.crz200,2),...
     grid.yt, '--k')
plot(nanmean(stuffJo.O.W200,2)./...
     nanmean(stuffJo.O.crz200,2),...
     grid.yt, ':k')
set(gca, 'xscale', 'log')
xlim([3e1 2e3])
legend('all', 'bioT', 'bioO', 'Location', 'Best')

subplot(132)
hold all
plot(nanmean(stuffJo.all.W200,2)./...
     nanmean(stuffJo.all.crz200,2),...
     grid.yt, '-r')
xlabel(' Remineralization length scale (m)')
title('Sinking Speed Effects')
ylabel('Latitude')

plot(nanmean(stuffJo.visT.W200,2)./...
     nanmean(stuffJo.visT.crz200,2),...
     grid.yt, '--k')
plot(nanmean(stuffJo.denT.W200,2)./...
     nanmean(stuffJo.denT.crz200,2),...
     grid.yt, ':k')
plot(nanmean(stuffJo.size.W200,2)./...
     nanmean(stuffJo.size.crz200,2),...
     grid.yt, '-.b')
set(gca, 'xscale', 'log')
xlim([3e1 2e3])
legend('all', 'visT', 'denT','size', 'Location', 'Best')

subplot(133)
hold all
plot(nanmean(stuffJo.all.W200,2)./...
     nanmean(stuffJo.all.crz200,2),...
     grid.yt, '-r')
xlabel('Remineralization length scale (m)')
ylabel('Latitude')
title('Sinking Speed Effects, Ballast')
plot(nanmean(stuffJo.car.W200,2)./...
     nanmean(stuffJo.car.crz200,2),...
     grid.yt, '--b')
plot(nanmean(stuffJo.si.W200,2)./...
     nanmean(stuffJo.si.crz200,2),...
     grid.yt, '-.k')
set(gca, 'xscale', 'log')
xlim([3e1 2e3])
legend('all','CaCO3 Ballasting', 'Silicate Ballasting', 'Location', 'Best')

set(gcf, 'units','normalized','outerposition',[0 0 1 1],...
        'Color', [1 1 1])
set(gcf,'PaperUnits','inches','PaperPosition',[0 0 16 8])
print('plots/reminlengthscenarios' , '-dpng')
export_fig('plots/reminlengthscenarios' , '-eps')


figure; jm_pcolor(grid.xt, grid.yt, log10(stuffJo.all.W200./ ...
                  stuffJo.all.crz200));
%title('log10 reminerilization length', 'FontSize', 20)
set(gcf, 'units','normalized','outerposition',[0 0 1 1],...
        'Color', [1 1 1])
set(gcf,'PaperUnits','inches','PaperPosition',[0 0 16 8])
print('plots/reminlengthmap' , '-dpng')
export_fig('plots/reminlengthmap' , '-eps')

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Input data Plots
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Ballast and Chl, + T + O
%% For this to run, it will have to go after the initial
%% calculations, because it includes density estemates, which are calculated
figure(13)
latav.si = nanmean(parJ.export.opal,2);
latav.car = nanmean(parJ.export.CaCO3,2);
%latav.chl = nanmean(nanmean(chl_clim, 3), 2);

loc_tprof = squeeze(parJ.phys.temp(62,90,:));

t200 = nan(size(parJ.phys.temp, 1), size(parJ.phys.temp, 2));
for i = 1:size(parJ.phys.temp, 1)
    for j = 1:size(parJ.phys.temp, 2)
        loc_tprof = squeeze(parJ.phys.temp(i,j,:));
        if(isnan(loc_tprof(1:7)) == 0)
            t200_loc = interp1(grid.zt(1:7), loc_tprof(1:7), 200, 'spline');
            t200(i,j) = t200_loc;
        end
    end
end

o200 = nan(size(parJ.phys.o2, 1), size(parJ.phys.o2, 2));
for i = 1:size(parJ.phys.o2, 1)
    for j = 1:size(parJ.phys.o2, 2)
        loc_oprof = squeeze(parJ.phys.o2(i,j,:));
        if(isnan(loc_oprof(1:7)) == 0)
            o200_loc = interp1(grid.zt(1:7), loc_oprof(1:7), 200, 'spline');
            o200(i,j) = o200_loc;
        end
    end
end


latav.t200 = nanmean(t200, 2);
latav.o200 = nanmean(o200, 2);
latav.rho_part = nanmean(stuffJtoptall.TObBa.rho_part, 2); 
latav.rho_part_car = nanmean(stuffJo.car.rho_part, 2); 
latav.rho_part_si = nanmean(stuffJo.si.rho_part, 2); 

%
figure
clf;
subplot(122)
 hold all
a = plot(latav.si, grid.yt, '--b');
b = plot(latav.car, grid.yt, '-.r');
ylim([-80, 65])
xlabel('Ballast:C Molar Ratio')
ylabel('Latitude')
ax1 = gca;
ax2 = axes('Position', ax1.Position,...
           'XAxisLocation','top',...
           'YTickLabelMode', 'manual', 'YTickLabel', [],...
           'YTickMode', 'manual', 'YTick', [], ...
           'Color','none', 'xscale', 'log');
hold all;
f = plot(latav.rho_part, grid.yt, '-k');
% g = plot(latav.rho_part_car, grid.yt, '-b');
% h = plot(latav.rho_part_si, grid.yt, ':b');
% c = plot(latav.chl, grid.yt, '--g');
ylim([-80, 65])
% xlim([.05,2.1])
xlabel('Particle Density')

legend([a;b;f],...
       'Si:C', 'CaCO3:C', 'Particle Density', ...
       'Location', 'East')

subplot(121)
hold all
d = plot(latav.t200, grid.yt, '-r');
xlabel('Temperature (\circC)')
ylabel('Latitude')
ax3 = gca;
ax4 = axes('Position', ax3.Position,...
           'XAxisLocation','top',...
           'YTickLabelMode', 'manual', 'YTickLabel', [],...
           'YTickMode', 'manual', 'YTick', [], ...
           'Color','none', 'xscale', 'linear');
hold all
e = plot(latav.o200, grid.yt, '-.k');
xlabel('Oxygen (\mum)')
legend([d;e], ...
       'Temperature', 'Oxygen',...
       'Location', 'Best')

set(gcf, 'units','normalized','outerposition',[0 0 1 1],...
        'Color', [1 1 1], 'PaperUnits','inches','PaperPosition',[0 0 16 8])


print('plots/ballastchl' , '-dpng')
print('plots/ballastchl' , '-dsvg')
export_fig('plots/ballastchl' , '-eps')