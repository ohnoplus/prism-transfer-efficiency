function [out_map] = rescale_map_mxb(in_map, m_factor, b_factor)
map_mean = nanmean(in_map(:)); % maybe this should be median
centered = in_map - map_mean;
scaled = centered * m_factor;
out_map = scaled + map_mean + b_factor;

