function sd = adjusted_sd(mod, err)
N = length(mod);
sd = sqrt((1/N) .* sum(((mod - mean(mod))./err).^2));