function sd = regular_sd(mod, err)
N = length(mod);
sd = sqrt((1/N) .* sum(((mod - mean(mod))).^2));