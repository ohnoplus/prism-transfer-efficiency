function out_map = repaint_interp2(in_XT, in_YT, in_map, out_XT, ...
                                   out_YT, ocean_map)

% check dimensions
if(~isequal(size(in_XT), size(in_YT), size(in_map)))
    error('in_XT, in_YT and in_map must be of the same size')
end
if(~isequal(size(out_XT), size(out_YT), size(ocean_map)))
    error('out_XT, out_YT and ocean_map) must be of the same size')
end


paint_map = inpaint_nans(in_map, 4);
regrid_map = interp2(in_XT, in_YT, paint_map, out_XT, ...
                                   out_YT);
loc_iocn = find(ocean_map == 1);
out_map = nan(size(regrid_map));
out_map(loc_iocn) = regrid_map(loc_iocn);