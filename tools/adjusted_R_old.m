function R = adjusted_R_old(mod, obs, err)
% calculate an adjusted R value by correlating modeled against
% observed data adjusting for standard errors of the observed data
sdm = adjusted_sd(mod, err);
sdo = adjusted_sd(obs, err);
N = length(mod);
R = (1/N) .* sum((obs - mean(obs)).*(mod - mean(mod)) ./err .^2 )./...
    (sdm .* sdo);
% Why am I squaring anything here?