function R = regular_R(mod, obs, err)
% calculate an adjusted R value by correlating modeled against
% observed data adjusting for standard errors of the observed data
sdm = regular_sd(mod);
sdo = regular_sd(obs);
N = length(mod);
R = (1/N) .* sum((obs - mean(obs)).*(mod - mean(mod)))./...
    (sdm .* sdo);

