
function profiles = basin_profile_avg(map3d, grid, R3d, z)
% basin_profile_avg Get profile averages for each basin
% profiles = basin_profile_avg(map3d, grid, R3d, z)
% profiles (nbasin * nz)
% map3d (nx * ny * *nz0): 3d map of the data in question
% grid defining map3d
% R3d: three dimensional region map
% z: depths for which you want profile outputs

nbasins = max(R3d(:));
profiles = repmat(NaN, length(z), nbasins);
for loc_ocn = 1:nbasins
     loc_mask = (R3d == loc_ocn);
     loc_ibsn = find(loc_mask);
     loc_na_mask = NaN * R3d;
     loc_na_mask(loc_ibsn) = 1;
     loc_data = loc_na_mask .* map3d;
     loc_short_profile = squeeze(nanmean(nanmean(loc_data, 1), 2));
     warning('off', 'MATLAB:chckxy:IgnoreNaN')
     loc_long_profile =  spline(grid.zw, loc_short_profile, z);
     profiles(:,loc_ocn) = loc_long_profile;
end
 
     
