function [a c] = jm_pcolor(lon, lat, map)
%default pretty map
%function jm_pcolor(lon, lat, map)


% vectors need to be vertical
lon2 = reshape(lon, length(lon), 1);
lat2 = reshape(lat, length(lat), 1);

shift_by = 170;
dx_plot=60;
dy_plot=30;
lon_shift = circshift(lon2,[shift_by 0]);
dlon = diff(lon_shift);
idx = find(dlon<0,1,'first');
lon_shift(idx+1:end) = lon_shift(idx+1:end) + 360.;

map_shift = circshift(map,[0 shift_by]);

m_proj('moll','lon',[min(lon_shift) max(lon_shift)])
a = m_pcolor(lon_shift,lat2',map_shift);shading('flat');
c = colorbar('fontsize', 18);
colormap jet;
m_grid('xtick',[-360:dx_plot:360],'ytick',[-90: ...
                    dy_plot:90],'fontsize',18, 'xaxislocation', 'middle');
m_coast('patch',[.9 .9 .9]);