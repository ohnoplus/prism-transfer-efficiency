function RMSD = adjusted_RMSD(mod, obs, err)
N = length(mod);
RMSD = sqrt((1/N) .* sum((((mod - mean(mod)) - (obs - mean(obs))) ./ ...
                          err).^2));
%keyboard