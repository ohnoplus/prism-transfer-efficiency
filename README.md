# README #

### What is this repository for? ###

This corresponds to the Cram et al. 2017 in press manuscript "The role of particle size, ballast, temperature, and oxygen in the sinking flux to the deep sea".
It contains the most recent version of the particle remineralization and sinking model. In this version, particles are from a full spectrum of sizes and can be made of any number of components, each with its own defined base remineralization rate and density (currently we are using three component models, POM, Silicate and Carbonate). It also contains code for calculating transfer efficiency across a 90x180 ocean grid. A script for generating all figures in the text, and which can be used to test the functions is also presented.

The following files and directories are of note:

juramia_teff_script.m 
This script generates the figures found in the manuscript. It is also useful for testing code. I have never tried running the whole script at once, I usually run cell by cell or line by line.

bringteffdata.sh
I used this bash script to gather data and files that I needed to run the rest of the code. It is useful for figuring out where data came from iff you work in Deutsch lab or otherwise have access to that group's computing system.

maincode
Is a directory containing the key pieces of code run by juramia_teff_script. These are listed below. Other code are found in tools. See the comments for the relevant inputs and outputs for each of these files.

maincode/pvsto_limw3.m
The size resolved, ballasted, temperature and oxygen dependent particle sinking model. 

maincode/calc_teff_type_17.m
Calculate transfer efficiency at each grid cell in a 4 degree x 4 degree ocean model. Also returns some other parameters about each grid cell.

maincode/calc_fwteff_type_17.m
Calculate spatially flux-weighted transfer efficiency for each major ocean region.

maincode/calc_fwteff_type_seas_17.m
Calculate spatially and climatologically flux-weighted transfer efficiency for each major ocean region.

maincode/fval_fwteff_type_err_17.m
Calculate an error-weighted f value, related to mean square error, associated with a set of input parameters. Used for model optimization.
The f, value, btw is 1/2 of MSE * nR where, nR is the number of regions.

plots
Plots that correspond to the figures in the manuscript go here. The ProtoProfiles figure is modified in inkscape to get the version that actually goes into the figures. I generally generate eps and png, and sometimes svg files for each of these plots. Items in the PDF and PNG folders were generated for the manuscript and are not created from files in this directory.

tools
Additional matlab functions that are called by juramia_teff_script.m or maincode files.

data
Input data files, that are loaded by juramia_teff_script.m. These include gridded data about global parameters and also outputs of some of the longer calculations.

sideprojects
Projects that are not part of the main project go here for safekeeping.



### How do I get set up? ###


* Configuration
You should just be able to run the juramia_teff_script.m out of the box. I generally run it by cell, or line by line.
* Dependencies
Requires MATLAB 2017 and the parallel toolbox, perhaps curve fitting toolbox, and perhaps some other packages as well. Individual, unusual functions should all be in the tools folder. Please contact the author if you experience unmet dependencies.


* Repo owner or admin
Jacob A Cram
cram at uw dot edu
cram at umces dot edu
* Other community or team contact
Curits Deutsch and Hartmut Frenzel at university of washington can both be located through the UW directory.