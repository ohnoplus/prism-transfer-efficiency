close all; clear all;

z=-4e3:2:0; % depths of grid box interface
%beta = -4.5; % Slope of PSD power law (default 4.46)
beta = -4.5; % Slope of PSD power law (default 4.46)
%r = 0.03; % first-order respiration (particle mass decay)
r = 0.0278; % first-order respiration (particle mass decay)
beta =-4.2;
r = 0.03;
tic;
z1=-4e3:1:0;
Sanl = particle_velo_sink_an(beta,r,-z1'); % analytical solution
toc;
tic;
gamma=1.17; alpha=2.28;
Snum = particle_velo_sink_new_170501(beta,r,z,'srnum'); % numerical solution
toc;

%close all
micron=1e-6;
pcol='rkgbcmy';
pcol2 = {[1,0,0], [0.5 0.5 0.5], [0 1 0], [0 0 1], [0 1 1]}

%pz=[1 499 999 -1999 3999];
pz=[100 500 1e3 2e3 4e3];
for i=1:length(pz)
    %[junk,pk(i)]=min(abs(-Sanl.z-pz(i)));
    [junk,pk(i)]=min(abs(-z-pz(i)));
end

for i=1:length(pz)
    %[junk,pk(i)]=min(abs(-Sanl.z-pz(i)));
    [junk,pk1(i)]=min(abs(-z1-pz(i)));
end

fac=Snum.Cn/Sanl.Cn;
%%
figure('unit','centimeters','position',[2 2 30 15],'paperpositionmode','auto');
axes('position',[0.15 0.2 0.32 0.7]);
for i=1:length(pk1)
    xx=Sanl.rr/micron; 
    yy=Sanl.n(pk1(i),:)*1e-20 * fac;
    hplot=loglog(xx,yy, '-', 'col', pcol2{i},'linewidth',4); hold on;
    eval(['h' num2str(i) '=hplot;']);
end

for i=1:length(pk)
    xx=Snum.rr/micron;
    yy=Snum.n(pk(i),:)*1e-20; 
    gplot = loglog(xx,yy,['k' '--'],'linewidth',2.5); hold on;
    eval(['g' num2str(i) '=gplot;']);
end

set(gca,'xlim',[10 5e3],'ylim',[1e0 1e10],'fontsize',16);
xlabel('Particle size [\mum]');ylabel('Particle size spectrum (normalized)'); %title('New')

h_legend=legend([h1 h2 h3 h4 h5 g1],'100m','500m','1000m','2000m','4000m', ...
                'Lagrangian', 'Location', 'NorthEast');

set(h_legend,'fontsize',16);

axes('position',[0.6 0.2 0.32 0.7]);
j1 = plot(Sanl.F/Sanl.F(end),Sanl.z,'-r','linewidth',6); hold on;
j2 = plot(Snum.F/Snum.F(end),Snum.z,'--k','linewidth',4); hold on;
j_legend = legend([j1 j2], 'Eularian', 'Lagrangian', 'Location', 'East');
set(j_legend,'fontsize',16);

set(gca,'fontsize',16, 'XTick', 0:0.2:1)
xlabel('Surface Normalized Flux');ylabel(['Depth below ' ...
                    'eutrophic zone [m]'])
%%
print -djpeg ../plots/particle_model_validation.jpeg;
print -depsc ../plots/particle_model_validation.eps;

%% JAC Edit -- check flux; more complicated than this
figure; 
plot(Sanl.F/Sanl.F(end), Sanl.z)
hold all
plot(Snum.F/Snum.F(end), Snum.z)
legend('Analytic', 'Numeric')

figure; 
plot(Sanl.w, Sanl.z)
hold all
plot(Snum.w, Snum.z)
legend('Analytic', 'Numeric')

%% pvsto_limw3 version
addpath(genpath('..'))

alpha = 2.28;
gamma = 1.17;
Q10 = 2; % mostly arbitrary I hope
KmOxy = 0; % mostly arbitrary I hope
xmm = 20;
mxmm =0.004;
wxmm = 0.7;
ds = 20;
dl = 2000;
rho_std = 1.23; 
rho_water = 1.0250;
zp = 0:2:4e3;
%zp = 0:0.1:4e3;
null_dens = repmat(rho_water, length(zp), 1); % negate temperature
                                           % effect, everything
                                           % assumes 4C water
null_temp = repmat(4, length(zp), 1);
null_oxy = repmat(200, length(zp), 1);


Spvsto = pvsto_limw3(alpha, -beta, gamma, r, ...
                       Q10, KmOxy, xmm, mxmm, wxmm, 4, ds, dl, ...
                       rho_std,  rho_std, rho_std, ...
                       null_dens/1, rho_water, ...
                       1, null_temp, null_oxy, zp, 1000);

%%
figure
clf
plot(Spvsto.FZ/Spvsto.FZ(1), -zp)

hold all
plot(Sanl.F/Sanl.F(end), Sanl.z)
plot(Snum.F/Snum.F(end), Snum.z)

figure; 
plot(Spvsto.FZ/Spvsto.FZ(1), -zp, 'o')
legend('Analytic', 'Numeric')

figure
plot(Spvsto.W, -zp)
plot(Sanl.w, Sanl.z)
hold all
plot(Snum.w, Snum.z)

%% Particle profiles
micron = 1e-6;
figure;
clf; hold all;
loglog(Spvsto.all.D/micron, Spvsto.all.N(1,:))
loglog(Spvsto.all.D/micron, Spvsto.all.N(pk1(2),:))