function Sol = particle_velo_sink_an(beta,Cr,z)

% Definitions and example values of Input Variables
% z=[0:4e3]; % depths of grid box interface
% beta = 4.46; % Slope of PSD power law (default 4.46)
% r = 0.0302; % first-order respiration (particle mass decay)

% conversion factors
sperd = 86400; % second per day
micron = 1e-6;

% particle sizes
DS = 10*micron; % smallest particle size (um)
DL = 2e3*micron; % largest particle size (um)
dD = 2*micron; % particle bin size (um)
D  = DS:dD:DL;

% sinking speed versus size - modified Stokes (w = Cw*D^gamma)
gamma = 1.17; 
w20 = 0.7; % sinking velocity of 20 micron particle (m/d) 
w1 = w20*(DS/(20*micron))^gamma; % sinking vel. of smallest particle (diameter DS)
Cw = w1/(DS^gamma);

% particle mass versus size (m = Cm*D^alpha)
alpha = 2.28;
m20 = 0.004; % mass of 20 micron particle (nmolN)
m1 = m20*(DS/(20*micron))^alpha; % mass of smallest particle (diameter DS)     
Cm = m1/(DS^alpha); % redundant

Mo = 1e10; % Total mass of PSD at z=0 (mmolN/m3) - for w this is arbitrary

% particle moments
Mtmp = particle_moments([0*beta+alpha beta],[DS DL]);
Cn = Mtmp/(Mo*Cm); % redundant
C1 = Cr/(alpha*Cw);

% matrices for calculations
zz = repmat(z,[1 length(D)]);
DD = repmat(D,[length(z) 1]);
%beta = repmat(beta,[1 length(D)]);
%Cn = repmat(Cn,[1 length(D)]);

% particle distribution characteristics
a1 = (beta+1)./gamma;

a2 = (C1*zz*gamma./(DD.^gamma))+1;
n = Cn.*(DD.^beta).*(a2.^a1);
m = dD*Cm*n.*DD.^alpha;
f = Cw*m.*DD.^gamma;

% solution
for iz=1:length(z)
    tm1=m(iz,:); tm1(D>max((DL^gamma-z(iz)*C1*gamma)^(1./gamma),0))=0;
    Sol.M(iz) = sum(tm1);
    tm1=f(iz,:); tm1(D>max((DL^gamma-z(iz)*C1*gamma)^(1./gamma),0))=0;
    Sol.F(iz) = sum(tm1);
    tm1=n(iz,:); tm1(D>max((DL^gamma-z(iz)*C1*gamma)^(1./gamma),0))=NaN;
    n(iz,:)=tm1;
end

Sol.n = n;

% solution
%Sol.M = sum(m,2);
%Sol.F = sum(f,2);
Sol.w = Sol.F./Sol.M;
Sol.rr=D;
Sol.z=-z;
Sol.Cn=Cn;
Sol.Cw=Cw;
% clear memory
clear zz DD beta Cn a1 a2 n m f

function M = particle_moments(E,D)
M = zeros(size(E,1),1);
for i = 1:size(E,1)
    e1 = 1+sum(E(i,:));
    M(i) = -(D(1)^e1*(1-(D(2)/D(1))^e1))/e1;
end


