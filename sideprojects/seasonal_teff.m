%% seasonal transfer efficiency example
% Not related to manuscript

parJ.component.rho_std = 1.23;
parS6 = parAm6;
parS6 = parJ;
parS6.PSD = mcchl_psd;
parS6.phys.temp = woaRe3.temp_mon;
parS6.phys.pdens = pdens4d;
parS6.phys.o2 = woaRe3.o2_mon;
parS6.EX = frankex_clim;
parS6.maxdeep = z(end);
parS6.teff_depth = 1000;

lcr = CrsJtoptall.TObBa;

% testseas_bak = testseas;
% testseas_bak1 = testseas;
% testseas = testseas_bak;
% testseas = testseas_bak1;

[testseas.bsns, testseas.map, testseas.stuff] = calc_fwteff_type_seas_17(lcr(1)/100, ...
                                                  lcr(2),1, 1, ...
                                                  2.3,1, 0, ...
                                                      1.033, lcr(3), ...
                                                  parS6)
size(testseas.stuff.fpoc)

% rerun this overnight with maxdeep corrected

nz2 = size(testseas.stuff.fpoc, 3);

plot(squeeze(testseas.stuff.fpoc(50, 100, :, 1)), -z(1:nz2))
hold all;
plot(squeeze(testseas.stuff.fpoc(25, 50, :, 1)), -z(1:nz2))

% basin average flux
% To Do: Swith out parJ.EX for parS6.EX(:,:,s)
bsn_sflux = nan(nreg, nz2, 12);
for s = 1:12
    for lz = 1:nz2
        loc_fbsn = basin_sum(testseas.stuff.fpoc(:,:,lz, s).*...
            parJ.EX, R2d)./...
            basin_sum(parJ.EX, R2d);
        bsn_sflux(:, lz, s) = loc_fbsn;
    end
end

% figure; pcolor(1:12, -z(1:nz2), squeeze(bsn_sflux(1, :, :)))


% interpolate pdens
% apparently, linear interpolation beats spline interpolation for
% some reason
% look out, I am extrapolating, even into regions where I don't
% have density. I hope this will be ok, since some places there
% just won't be flux estemates there.
% Lets make sure flux estemates account for the sea-floor already though.
zvec_fine = z(1:nz2);
pdens_fine = nan(ny, nx, nz2, 12);
tic
for s = 1:12
    s
    for i = 1:ny
        for j = 1:nx
            
                pdens_loc = interp1(grid.zt, squeeze(pdens4d(i, j, :, s)), ...
                                    zvec_fine, 'linear', 'extrap');
                % pdens_loc = spline(grid.zt, squeeze(pdens4d(j, i, :, s)), ...
                %                     zvec_fine); 
                loc_fm3d = find(M3d(i, j, :) == 0);
                if(length(loc_fm3d) > 0)
                    loc_floor0 = grid.zt(loc_fm3d(1));
                    loc_floor1 = min(loc_floor0, z(nz2));
                    pdens_ocn = pdens_loc(find(zvec_fine <= ...
                                               loc_floor1));
                    pdens_ocn2 = nan(length(zvec_fine),1);
                    pdens_ocn2(1:length(pdens_ocn)) = pdens_ocn;
                    pdens_fine(i, j, :, s) = pdens_ocn2;
                end
            
        end
    end
end
toc

figure; pcolor(zvec_fine, grid.yt, squeeze(pdens_fine(:, 100, :, ...
                                                  12))); shading flat; ...
    colorbar;
figure; pcolor(squeeze(pdens_fine(:, 100, :, ...
                                                  12))); shading flat; colorbar;
figure; pcolor(grid.zt, grid.yt, squeeze(M3d(:, 100, :))); shading flat; colorbar;

% basin-season average pdens
bsn_spdens = nan(nreg, nz2, 12);
for s = 1:12
    for lz = 1:nz2
        loc_spdens = basin_sum(pdens_fine(:,:,lz, s).*...
            parJ.EX, R2d)./...
            basin_sum((pdens_fine(:,:,lz, s)*0+1).*parJ.EX, R2d);
        bsn_spdens(:, lz, s) = loc_spdens;
    end
end

% what if I don't interpolate
nz1 = length(grid.zt);
bsn_spdensA = nan(nreg, nz1, 12);
for s = 1:12
    for lz = 1:nz1
        loc_spdensA = basin_sum(pdens4d(:,:,lz, s).*...
            parJ.EX, R2d)./...
            basin_sum((pdens4d(:,:,lz, s)*0+1).*parJ.EX, R2d);
        bsn_spdensA(:, lz, s) = loc_spdensA;
    end
end

figure; plot(bsn_spdens(1, :, 1), -z(1:nz2))
figure; plot(bsn_spdensA(1, :, 1), -grid.zt)

% so I actually want to somehow interpolate flux with respect to
% pdens, rather than z
pdvec = 1.022:0.00005:1.028
npd = length(pdvec);

zvec_fine = z(1:nz2);
fluxVden = nan(ny, nx, npd, 12);
tic
for s = 1:12
    for i = 1:ny
        for j = 1:nx
            
            loc_denvec = squeeze(pdens_fine(i,j,:,s));
            loc_fvec = squeeze(testseas.stuff.fpoc(i, j, :, ...
                                                   s));
            usethese = find(isfinite(loc_denvec));
            loc_denvec1 = loc_denvec(usethese);
            loc_fvec1 = loc_fvec(usethese);

            if(length(usethese)>0)
                fluxVden_loc = interp1(loc_denvec1,...
                                       loc_fvec1, ...
                                       pdvec);
                
                
                % pdens_loc = spline(grid.zt, squeeze(pdens4d(j, i, :, s)), ...
                %                     zvec_fine); 
                fluxVden(i, j, :, s) = fluxVden_loc;
            end 
        end
    end
end
toc

%% basin interpolate with respect to pden
bsn_fluxVden = nan(nreg, npd, 12);
for s = 1:12
    for lp = 1:npd
        loc_fvd = basin_sum(fluxVden(:,:,lp, s).*...
            parJ.EX, R2d)./...
            basin_sum((fluxVden(:,:,lp, s)*0+1).*parJ.EX, R2d);
        bsn_fluxVden(:, lp, s) = loc_fvd;
    end
end

% These are where I made the two figures for Curtis March 2017
figure; pcolor(1:12, -pdvec, squeeze(bsn_fluxVden(1,:,:))); 
shading flat; colorbar; colormap jet;
ylim([-1.028, -1.022])
caxis([0 1])
title('AA')

figure; pcolor(1:12, -pdvec, squeeze(bsn_fluxVden(2,:,:)));
shading  flat; colorbar; colormap jet;
ylim([-1.028, -1.022])
caxis([0 1])
title('SAA')

%% The antarctic version has this band at the very bottom of high
%% flux. Presumably there is one (or very few) places with such
%% high density that have lots of flux (eg, in the downwelling
%% region)
%% I could ask the mean depth at each cell. I could also ask the
%% number of observations at each cell. I'll start with the later.

for s = 1:12
    for lp = 1:npd
        loc_nvd =  basin_sum((fluxVden(:,:,lp, s)*0+1), R2d);
        bsn_NVden(:, lp, s) = loc_nvd;
    end
end

figure; pcolor(1:12, -pdvec, squeeze(log10(bsn_NVden(1,:,:)))); 
shading flat; colorbar; colormap jet;
ylim([-1.028, -1.022])

%% Interpolate flux into 24 depth bins
seasflux = nan(ny, nx, nz1, 12);
for s = 1:12
    for i = 1:ny
        for j = 1:nx
            loc_fvec = squeeze(testseas.stuff.fpoc(i,j,:, s));
            loc_fvec_course = interp1(z(1:nz2), loc_fvec, ...
                                      grid.zt);
            seasflux(i, j, :, s) = loc_fvec_course;
        end
    end
end

sM3d = repmat(M3d, 1, 1, 1, 12);
seasiocn = find(sM3d == 1);

seasflux1 = nan(size(seasflux));
seasflux1(seasiocn) = seasflux(seasiocn);

            
save('seasonalflux.mat', 'seasflux1', 'pdens4d', 'grid', 'parJ', 'parS6')

figure; pcolor(squeeze(seasflux1(:, 100, :,  12)));
shading flat; colorbar;