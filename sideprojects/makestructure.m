% saving variabiles that users will load in if they want to work
% with the transfer efficeincy scripts. this file requires access
% to the deutch lab workspace
p16psd.mon = mcd2.mon;
p16psd.lat = mcd2.lat;
p16psd.lon = mcd2.lon;
p16psd.surf_beta = surf_beta';


save('juramia_data.mat', 'p16psd', 'M3d', 'pgrid', 'pgrid_chl_clim', ...
     'frankex_clim', 'grid', 'R2d', 'R3d', 'regnames', 'woaRe3', ...
     'woachl', 'parAm6', 'lowjet')